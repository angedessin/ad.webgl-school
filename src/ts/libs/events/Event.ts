const MOUSE_WHEEL_EVENT: string = "onwheel" in document ? "wheel" : 'onmousewheel' in document ? "mousewheel" : "DOMMouseScroll";
export default class Event {
    static RESIZE: string = "resize";
    static ORIENTATION: string = "orientationchange";
    static LOAD: string = "load";
    static SCROLL: string = "scroll";
    static SELECT: string = "select";
    static SUBMIT: string = "submit";
    static HASH_CHANGE: string = "hashchange";
    static FOUCUS: string = "focus";
    static BLUR: string = "blur";
    static CHANGE: string = "change";
    static ABORT: string = "unload";
    static BEFORE_UNLOAD: string = "beforeunload";
    static ERROR: string = "error";
    static CONTEXT_MENU: "contextmenu";
    static COPY: string = "copy";
    static PASTE: string = "paste";
    static READY_STATE_CHANGE: string = "readystatechange";
    static RESET: string = "reset";
    static LOAD_END: string = "loadend";
    static POP_STATE: string = "popstate";
    static MOUSE_WHEEL: string = MOUSE_WHEEL_EVENT;

    type: string;
    data: any;

    constructor(type: string, data?: any) {
        this.type = type;
        this.data = data;
    }

    getData(): any {
        return this.data;
    }

    getType(): string {
        return this.type;
    }
}