export default class AnimationEvent {
    static ANIMATION_START: string[] = [
        "webkitAnimationStart", "mozAnimationStart", "msAnimationStart", "oAnimationStart", "animationStart"
    ];
    static ANIMATION_END: string[] = [
        "webkitAnimationEnd", "mozAnimationEnd", "msAnimationEnd", "oAnimationEnd", "animationEnd"
    ];
    static TRANSITION_END: string[] = [
        "webkitTransitionEnd", "mozTransitionEnd", "msTransitionEnd", "oTransitionEnd", "transitionend"
    ];
}


