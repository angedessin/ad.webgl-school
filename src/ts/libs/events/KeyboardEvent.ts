export default class KeyboardEvent {
    static KEY_DOWN: string = "keydown";
    static KEY_UP: string = "keyup";
    static KEY_PRESS: string = "keypress";
}