export default class TouchEvent {
    static TOUCH_START: string = "touchstart";
    static TOUCH_MOVE: string = "touchmove";
    static TOUCH_END: string = "touchend";
    static TOUCH_CANCEL: string = "touchcancel";
}