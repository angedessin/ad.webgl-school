export default class VideoEvent {
    static LOAD_START: string = "loadstart";
    static LOADED_META_DATA: string = "loadedmetadata";
    static LOADED_DATA: string = "loadeddata";
    static CAN_PLAY: string = "canplay";
    static CAN_PLAY_THROUGH: string = "canplaythrough";
    static WAITING: string = "waiting";
    static ERROR: string = "error";
    static PLAYING: string = "playing";
    static TIME_UPDATE: string = "timeupdate";
    static PROGRESS: string = "progress";
    static PAUSE: string = "pause";
    static ENDED: string = "ended";
}