import Event from "./Event";
import EventDispatcher from "./EventDispatcher";

export default class EventThrottle extends EventDispatcher {
    static EVENT_THROTTLE: string = "EVENT_THROTTLE";
    static THROTTLE_EVENT = new Event(EventThrottle.EVENT_THROTTLE);
    protected _duration: number;
    protected _stock: number = 0;
    protected _timer;

    /**
     * constructor
     * @constructor
     * @param duration
     */
    constructor(duration?: number) {
        super();
        this._duration = duration ? duration : 1000 / 60;
    }

    dispatchEvent(e: Event, data?: any) {
        const current: number = new Date().getTime();
        if (this._timer) clearTimeout(this._timer);
        if (current - this._stock >= this._duration) {
            (data) ? super.dispatchEvent(e, data) : super.dispatchEvent(e);
            this._stock = current;
        } else {
            this._timer = setTimeout(() => {
                (data) ? super.dispatchEvent(e, data) : super.dispatchEvent(e);
            }, this._duration);
        }
    }
}