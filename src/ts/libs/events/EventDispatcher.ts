import Event from "./Event";

export interface IEventListeners {
    type: string;
    listener: Function;
}

export interface IEventDispatcher {
    _listeners: IEventListeners[];
    hasEventListener(type: string, listener: Function): boolean;
    addEventListener(type: string, listener: Function): void;
    removeEventListener(type: string, listener: Function): void;
    dispatchEvent(e: Event, data?: any): void;
}

export interface IEventListener {
    type: string;
    listener: Function;
    data?: any;
}

export default class EventDispatcher implements IEventDispatcher {

    _listeners: IEventListener[];

    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        this._listeners = [];
    }

    /**
     * EventListenerをあるかどうかを確認する
     * @public
     * @param type
     * @param listener
     * @returns {boolean}
     */
    hasEventListener(type: string, listener?: Function): boolean {
        if (typeof(type) !== "string" && typeof(listener) !== "function") {
            return;
        }
        let exists: boolean = false;
        for (let i: number = 0; i < this._listeners.length; i++) {
            if (this._listeners[i].type === type || this._listeners[i].listener === listener) {
                exists = true;
                return exists;
            }
        }
        return exists;
    }

    /**
     * EventListenerを登録する
     * @public
     * @param type
     * @param listener
     */
    addEventListener(type: string, listener: Function): void {
        if (typeof(type) !== "string" && typeof(listener) !== "function") {
            return;
        }

        if (this.hasEventListener(type, listener)) {
            return;
        }

        this._listeners.push({type: type, listener: listener});
    }

    /**
     * EventListenerを削除
     * @public
     * @param type
     * @param listener
     */
    removeEventListener(type: string, listener: Function): void {
        if (typeof(type) !== "string" && typeof(listener) !== "function") {
            return;
        }

        for (let i: number = 0; i < this._listeners.length; i++) {
            if (this._listeners[i].type === type && this._listeners[i].listener === listener) {
                this._listeners.splice(i, 1);
            }
        }
    }

    allRemoveEventListener(): void {
        for (let i: number = 0; i < this._listeners.length; i++) {
            this._listeners.splice(i, 1);
        }
        
        this._listeners = [];
    }

    /**
     * Eventを発火する
     * @public
     * @param e
     * @param data
     */
    dispatchEvent(e: Event, data?: any): void {
        for (let i: number = 0; i < this._listeners.length; i++) {
            if (this._listeners[i].type === e.getType()) {
                this._listeners[i].listener.call(this, e);
            }
        }
    }

    /**
     * EventListenerをリセットする
     * @public
     */
    clearEvent(): void {
        this._listeners = [];
    }
}
