export default class Dom {

    /**
     * HTMLElementを取得する
     * @param id
     * @returns {HTMLElement}
     */
    public static getElement(id: string) {
        return document.getElementById(id);
    }

    /**
     * セレクタをマッチしたノードを取得する
     * @param el
     * @param selector
     * @returns {NodeListOf<Element>}
     */
    public static getSelectorAll(el: HTMLElement, selector: string) {
        return el.querySelectorAll(selector);
    }


    /**
     * クラスをマッチしたノードを取得する
     * @param el
     * @param selector
     * @returns {NodeListOf<Element>}
     */
    public static getClass(el: HTMLElement, selector: string): NodeListOf<Element> {
        return el.getElementsByClassName(selector);
    }


    /**
     * EventListenerを追加
     * @param el
     * @param type
     * @param handler
     * @param bubbling
     */
    public static addEvent(el: any, type: string, handler: Function, bubbling: boolean = false): void {
        if (document.addEventListener) {
            if (el && (el.nodeName || el === window || typeof el === "object")) {
                el.addEventListener(type, handler, bubbling);
            } else if (el && el.length) {
                for (let i: number = 0; i < el.length; i++) {
                    Dom.addEvent(el[i], type, handler);
                }
            }
        } else {
            if (el && (el.nodeName || el === window || typeof el === "object")) {
                el.attachEvent(
                    "on" + type, () => {
                        return handler.call(el, window.event);
                    }
                );
            } else if (el && el.length) {
                for (let i: number = 0; i < el.length; i++) {
                    Dom.addEvent(el[i], type, handler);
                }
            }
        }
    }

    /**
     * EventListenerを削除
     * @param el
     * @param type
     * @param handler
     * @param bubbling
     */
    public static removeEvent(el: any, type: string, handler: Function, bubbling: boolean = false) {
        if (document.addEventListener) {
            if (el && (el.nodeName || el === window || typeof el === "object")) {
                el.removeEventListener(type, handler, bubbling);
            } else if (el && el.length) {
                for (let i: number = 0; i < el.length; i++) {
                    Dom.removeEvent(el[i], type, handler);
                }
            }
        } else {
            if (el && (el.nodeName || el === window || typeof el === "object")) {
                el.attachEvent(
                    "off" + type, () => {
                        return handler.call(el, window.event);
                    }
                );
            } else if (el && el.length) {
                for (let i: number = 0; i < el.length; i++) {
                    Dom.removeEvent(el[i], type, handler);
                }
            }
        }
    }


    /**
     *
     * @param el
     * @returns {{top: number, left: number}}
     */
    public static getRect(el: HTMLElement | Element): { top: number, left: number } {
        const box: ClientRect = el.getBoundingClientRect();
        return {
            top: box.top + window.pageYOffset - document.documentElement.clientTop,
            left: box.left + window.pageXOffset - document.documentElement.clientLeft
        };
    }

    /**
     * indexを取得する
     * @param parent
     * @param el
     * @returns {number}
     */
    public static getElementsIndex(parent: HTMLElement, el: HTMLElement): number {
        return <number>[].indexOf.call(parent, el);
    }

    /**
     * DOMの準備が完了したかを取得する
     * @param callback
     */
    public static documentReady(callback: any) {
        if (document.readyState !== "loading") {
            callback();
        } else {
            document.addEventListener('DOMContentLoaded', callback);
        }
    }


    /**
     * 指定した要素の属性を取得
     * @param el
     * @param type
     * @returns {string}
     */
    public static getAttribute(el: HTMLElement, type: string) {
        return el.getAttribute(type);
    }


    /**
     * 指定の要素の属性に値を代入する
     * @param el
     * @param type
     * @param val
     */
    public static setAttribute(el: any, type: string, val: any) {
        el.setAttribute(type, val);
    }

    /**
     * addClassする
     * @param el
     * @param classNames
     */
    public static addClass(el: any, classNames: string[]) {
        if (!Array.isArray(classNames) || classNames.length < 1) {
            throw new TypeError("String型を指定");
        }
        for (let i: number = 0; i < classNames.length; i++) {
            if (el.classList) {
                el.classList.add(classNames[i]);
            } else {
                el.className += " " + classNames[i];
            }
        }
    }

    /**
     * removeClassする
     * @param el
     * @param classNames
     */
    public static removeClass(el: any, classNames: string[]) {
        if (!Array.isArray(classNames) || classNames.length < 1) {
            throw new TypeError("String型を指定");
        }
        for (let i: number = 0; i < classNames.length; i++) {
            if (el.classList) {
                el.classList.remove(classNames[i]);
            } else {
                el.className = el.className.replace(new RegExp("(^|\\b)" + classNames[i].split(" ").join("|") + "(\\b|$)", "gi"), " ");
            }
        }
    }


    /**
     * classがあるか
     * @param el
     * @param className
     * @returns {boolean}
     */
    public static hasClass(el: any, className: string): boolean {
        if (typeof className !== "string") {
            throw new TypeError("String型を指定");
        }
        if (el.classList) {
            return el.classList.contains(className);
        } else {
            return new RegExp("(^| )" + className + "( |$)", "gi").test(el.className);
        }
    }

    public static toggleClass(el: any, className: string) {
        if (typeof className !== "string") {
            throw new TypeError("String型を指定");
        }
        if (el.classList) {
            el.classList.toggle(className);
        } else {
            let classes: string[] = el.className.split(" ");
            const existingIndex: number = classes.indexOf(className);
            if (existingIndex >= 0) {
                classes.splice(existingIndex, 1);
            } else {
                classes.push(className);
            }

            el.className = classes.join(" ");
        }
    }

    /**
     * 要素の後にHTMLを追加
     * @param el
     * @param htmlString
     */
    public static addAfterDom(el: any, htmlString: string) {
        return el.insertAdjacentHTML("afterend", htmlString);
    }

    /**
     * 要素の後に前にHTMLを追加
     * @param el
     * @param htmlString
     */
    public static addBeforeDom(el: any, htmlString: string) {
        return <HTMLElement>el.insertAdjacentHTML("beforebegin", htmlString);
    }


    /**
     * 要素の後にDOMを追加
     * @param el
     * @param parent
     */
    public static appendDom(el: any, parent: HTMLElement) {
        parent.appendChild(el);
    }

    /**
     * 要素の前にDOMを追加
     * @param el
     * @param parent
     */
    public static prependDom(el: any, parent: HTMLElement) {
        parent.insertBefore(el, parent.firstChild);
    }

    /**
     * 要素を削除する
     * @static
     * @param el
     */
    public static removeDom(el: any) {
        el.parentNode.removeChild(el);
    }


    /**
     * テキスト要素を取得する
     * @param el
     * @returns {string}
     */
    public static getDomTxt(el: HTMLElement) {
        return el.textContent;
    }

    /**
     * テキスト要素を差し替える
     * @param el
     * @param str
     */
    public static setDomTxt(el: HTMLElement, str: string) {
        el.textContent = str;
    }

}

