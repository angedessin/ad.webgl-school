import TouchEvent from "../events/TouchEvent";
import Dom from "../views/Dom";

const _win: any = window;
const _doc: any = document;
let _mobileBox: HTMLElement;
let _isDebugMobile: boolean = false;

declare const APP_ENV: string;
export default class Log {
    /**
     * console.logを出力する
     * @param className
     * @param methodName
     * @param val
     */
    public static trace(className: string, methodName: string, val: any) {
        if (!_win.console) {
            _win.console = {
                log: (msg: any): void => {
                }
            };
        }


        if (APP_ENV === "production") {
            return;
        }

        if (typeof("console") !== "undefined") {
            console.log(`>>>>>>>>>>> ${className} : ${methodName} :`, val);
        }


        if (_isDebugMobile && _mobileBox) {
            const p: HTMLElement = _doc.createElement("div");
            p.textContent = val;
            _mobileBox.insertBefore(p, _mobileBox.firstChild);
        }
    }

    /**
     * モバイルのデバックをスタートさせる
     */
    public static startDebugOfMobile() {
        if (APP_ENV === "production") {
            return;
        }
        const debugDom: HTMLElement = _doc.createElement("div");
        const debugBtn: HTMLElement = _doc.createElement("div");
        debugDom.id = "data-debug-sp";
        debugDom.className = "debug-sp";
        debugBtn.id = "data-debug-sp-btn";
        debugBtn.className = "debug-sp-btn";

        _doc.appendChild(debugDom);
        _doc.appendChild(debugBtn);

        const btn: HTMLElement = Dom.getElement("data-debug-sp-btn");
        btn.style.position = "fixed";
        btn.style.top = "0";
        btn.style.left = "0";
        btn.style.zIndex = "1000000";
        btn.style.width = "40px";
        btn.style.height = "40px";

        let counter: number = 0;
        _mobileBox = Dom.getElement("data-debug-sp");
        _mobileBox.style.display = "none";

        const onClick: EventListener = (e): void => {
            counter += 1;
            if (counter > 3) {
                btn.removeEventListener(TouchEvent.TOUCH_END, onClick);
                btn.style.display = "none";
                _mobileBox.style.display = "block";
                _isDebugMobile = true;
            }
        };
        btn.addEventListener(TouchEvent.TOUCH_END, onClick);
    }
}

