export default class Browser {
    /**
     * UAを取得する
     * @public
     * @returns {string}
     */
    public static getUserAgent(): string {
        return navigator.userAgent;
    }

    /**
     * 小文字に変換されたUAを取得する
     * @public
     * @returns {string}
     */
    public static getUserAgentLowerCase(): string {
        return Browser.getUserAgent().toLowerCase();
    }

    /**
     * バージョンを取得
     * @public
     * @returns {string}
     */
    public static getAppVersion(): string {
        return navigator.appVersion;
    }


    /**
     * 小文字に変換されたバージョンを取得
     * @public
     * @returns {string}
     */
    public static getAppVersionLowerCase(): string {
        return Browser.getAppVersion().toLowerCase();
    }

    /**
     * スマホかどうかを判別
     * @public
     * @returns {boolean}
     */
    public static isMobile(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return (ua.indexOf("windows") !== -1 && ua.indexOf("phone") !== -1)
            || (ua.indexOf("iphone")) !== -1
            || (ua.indexOf("ipod")) !== -1
            || (ua.indexOf("android") !== -1 && ua.indexOf("mobile") !== -1)
            || (ua.indexOf("firefox") !== -1 && ua.indexOf("mobile") !== -1)
            || (ua.indexOf("blackberry")) !== -1;
    }

    /**
     * タブレットかどうかを判別
     * @public
     * @returns {boolean}
     */
    public static isTablet(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return (ua.indexOf("windows") !== -1 && ua.indexOf("touch") !== -1)
            || (ua.indexOf("ipad")) !== -1
            || (ua.indexOf("android") !== -1 && ua.indexOf("mobile") === -1)
            || (ua.indexOf("firefox") !== -1 && ua.indexOf("tablet") !== -1)
            || (ua.indexOf("kindle")) !== -1
            || (ua.indexOf("silk")) !== -1
            || (ua.indexOf("playbook")) !== -1;
    }

    /**
     * IE9かを判別する
     * @public
     * @returns {boolean}
     */
    public static isIE9(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        const appVersion: string = Browser.getAppVersionLowerCase();
        const isIE: boolean = ua.indexOf("msie") !== -1;
        return isIE && (appVersion.indexOf("msie 9.") !== -1);
    }

    /**
     * IE10かを判別する
     * @public
     * @returns {boolean}
     */
    public static isIE10(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        const appVersion: string = Browser.getAppVersionLowerCase();
        const isIE: boolean = ua.indexOf("msie") !== -1;
        return isIE && (appVersion.indexOf("msie 10.") !== -1);
    }

    /**
     * IE11かを判別する
     * @public
     * @returns {boolean}
     */
    public static isIE11(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("trident/7") > -1;
    }

    /**
     * Edgeかを判別する
     * @public
     * @returns {boolean}
     */
    public static isEdge(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("edge") > -1;
    }

    /**
     * IEかを判別する
     * @public
     * @returns {boolean}
     */
    public static isIE(): boolean {
        const ua: string = Browser.getUserAgent();
        return !!(ua.match(/MSIE/) || ua.match(/Trident/));
    }


    /**
     * Firefoxかを判別する
     * @public
     * @returns {boolean}
     */
    public static isFirefox(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("firefox") != -1;
    }

    /**
     * Chromeかを判別する
     * @public
     * @returns {boolean}
     */
    public static isChrome(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("chrome") != -1;
    }


    /**
     * Androidかを判別する
     * @returns {boolean}
     */
    public static isAndroid(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("android") !== -1;
    }


    public static isAndroidBrowser(): boolean {
        const ua: string = Browser.getUserAgent();
        let isAndroidBrowser: boolean = false;
        if (/Android/.test(ua) && /Linux; U;/.test(ua) && !/Chrome/.test(ua)) {
            isAndroidBrowser = true
        }

        return isAndroidBrowser;
    }

    /**
     * Androidのバージョンを取得する
     * @returns {any}
     */
    public static getAndroidVersion(): number {
        if (Browser.isAndroid()) {
            const ua: string = Browser.getUserAgent();
            return parseFloat(ua.slice(ua.indexOf("Android") + 8));
        }
        return null;
    }

    /**
     * iOSかを判別する
     * @returns {boolean}
     */
    public static isiOS(): boolean {
        const ua: string = Browser.getUserAgentLowerCase();
        return ua.indexOf("ipad") != -1 || ua.indexOf("iphone") != -1 || ua.indexOf("iPod") != -1;
    }


    /**
     * iOSのバージョンを取得する
     * @returns {any}
     */
    public static getiOSVersion(): number {
        if (Browser.isiOS()) {
            const ua: string = Browser.getUserAgent();
            let osv: number = null;
            if (/iPad/.test(ua)) {
                ua.match(/CPU OS (\w+){1,3}/g);
                osv = Number((RegExp.$1.replace(/_/g, "") + "00").slice(0, 3));
            } else if (/iPhone/.test(ua)) {
                ua.match(/iPhone OS (\w+){1,3}/g);
                osv = Number((RegExp.$1.replace(/_/g, "") + "00").slice(0, 3));
            }
            osv = Number(osv) / 100;

            return osv;
        }
        return null;
    }


    /**
     * URLのパラメータを取得
     * @public
     * @return {object} GET値をオブジェクト型で返す
     */
    public static getUrlParams(): any {
        let i: number = 0;
        let key: string;
        let keySearch: number;
        const param: any = location.search.substring(1).split("&");
        let val: any;
        let urlVars: any = {};
        while (i < param.length) {
            keySearch = param[i].search(RegExp("="));
            key = "";

            if (keySearch !== -1) {
                key = param[i].slice(0, keySearch);
            }

            val = param[i].slice(param[i].indexOf("=", 0) + 1);

            if (key !== "") {
                urlVars[key] = decodeURI(val);
            }
            i++;
        }
        return urlVars;
    }
}
