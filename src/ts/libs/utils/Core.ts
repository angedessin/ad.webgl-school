import {Promise} from "es6-promise";

export interface ITime {
    h: number;
    m: number;
    s: number;
    ms: number;
}
export default class Core {
    /**
     * 文字列かを判別
     * @param value
     * @returns {boolean}
     */
    public static isString(value: any): boolean {
        return typeof value === "string";
    }

    /**
     * 数字かを判別
     * @param value
     * @returns {boolean}
     */
    public static isNumber(value: any): boolean {
        return typeof value === "number";
    }

    /**
     * 配列かを判別
     * @param value
     * @returns {boolean}
     */
    public static isArray(value: any): boolean {
        return Array.isArray(value);
    }

    /**
     * 関数かを判別
     * @param value
     * @returns {boolean}
     */
    public static isFunction(value: any): boolean {
        return typeof value === "function";
    }

    /**
     * objectかを判別
     * @param value
     * @returns {boolean}
     */
    public static isObject(value: any): boolean {
        return Object.prototype.toString.call(value) === "[object Object]";
    }

    /**
     * UnixTimeを取得
     * @returns {number}
     */
    public static getUnixTime(): number {
        return Math.floor(new Date().getTime() / 1000);
    }


    /**
     * localStorageを追加
     * @param name
     * @param value
     */
    public static setLocalStorage(name: string, value: any) {
        localStorage.setItem(name, JSON.stringify(value));
    }

    /**
     * localStorageを取得　
     * @param name
     * @returns {any}
     */
    public static getLocalStorage(name: string): string {
        return JSON.parse(localStorage.getItem(name));
    }

    /**
     * localStorageをクリア
     */
    public static clearLocalStorage(): void {
        localStorage.clear();
    }

    /**
     * localStorageを削除
     * @param name
     */
    public static removeLocalStorageItem(name): void {
        localStorage.removeItem(name);
    }


    /**
     * SessionStorageを追加
     * @param name
     * @param value
     */
    public static setSessionStorage(name: string, value: any) {
        sessionStorage.setItem(name, JSON.stringify(value));
    }


    /**
     * SessionStorageを取得
     * @param name
     * @returns {any}
     */
    public static getSessionStorage(name: string): string {
        return JSON.parse(sessionStorage.getItem(name));
    }


    /**
     * 配列内の最大値を取得
     * @param array
     * @returns {any}
     */
    public static max(array: number[]): number {
        return Math.max.apply(null, array);
    }

    /**
     * 配列内の最小値を取得
     * @param array
     * @returns {any}
     */
    public static min(array: number[]): number {
        return Math.min.apply(null, array);
    }


    /**
     *
     * @param val
     * @param min
     * @param max
     * @returns {number}
     */
    public static clamp(val, min, max): number {
        return Math.min(Math.max(val, min), max);
    };


    /**
     * base64のコードに変更
     * @param data
     * @returns {string}
     * @constructor
     */
    public static createBase64(data): string {
        const bytes = new Uint8Array(data);
        let binaryData = "";
        for (let i = 0; i < bytes.byteLength; i++) {
            binaryData += String.fromCharCode(bytes[i]);
        }

        let dataType;
        if (bytes[0] === 0xff && bytes[1] === 0xd8 && bytes[bytes.byteLength - 2] === 0xff && bytes[bytes.byteLength - 1] === 0xd9) {
            dataType = "data:image/jpeg;base64,";
        }
        else if (bytes[0] === 0x89 && bytes[1] === 0x50 && bytes[2] === 0x4e && bytes[3] === 0x47) {
            dataType = "data:image/png;base64,";
        }
        else if (bytes[0] === 0x47 && bytes[1] === 0x49 && bytes[2] === 0x46 && bytes[3] === 0x38) {
            dataType = "data:image/gif;base64,";
        }
        else if (bytes[0] === 0x42 && bytes[1] === 0x4d) {
            dataType = "data:image/bmp;base64,";
        }
        else {
            dataType = "data:image/unknown;base64,";
        }

        return dataType + window.btoa(binaryData);
    }


    /**
     * 横向き縦向きの状態を取得する
     * @returns {string}
     */
    public static getOrientation(): string {
        return (Math.abs(Number(window.orientation)) === 90) ? "landscape" : "portrait";
    }


    /**
     * windowの横幅を取得する
     * @returns {number}
     */
    public static getInnerW() {
        return window.innerWidth;
    }

    /**
     * windowの縦幅を取得する
     * @returns {number}
     */
    public static getInnerH() {
        return window.innerHeight;
    }


    /**
     * ドキュメントのタイトルを変更する
     * @param title
     */
    public static updateDocTitle(title: string): void {
        document.title = title;
    }


    public static clearTimeout(timers: any[]): void {
        for (let i: number = 0; i < timers.length; i++) {
            clearTimeout(timers[i]);
        }
    }


    /**
     * 配列のシャッフル
     * @param array
     * @returns {any}
     */
    public static shuffleArray(array): any[] {
        let n: number = array.length;
        let t: number;
        let i: number;

        while (n) {
            i = Math.floor(Math.random() * n--);
            t = array[n];
            array[n] = array[i];
            array[i] = t;
        }

        return array;
    }


    /**
     * Promiseオブジェクトを作成
     * @param delay
     * @returns {Promise}
     */
    public static setPromise(delay?: number): Promise<{}> {
        return new Promise((resolve: Function, reject: Function) => {
            const timer = setTimeout(() => {
                clearTimeout(timer);
                resolve();
            }, delay);
        });
    }

    public static getLocationPath(): string {
        const pathname: string = location.pathname;
        const paths: string[] = pathname.split("/").reverse().slice(1).reverse();
        let path: string;
        if (paths.length < 2) {
            path = paths.join("/") + "/";
        } else {
            let newPaths: string[] = [];
            for (let i: number = 0; i < paths.length; i++) {
                newPaths.push(paths[i]);
            }
            path = newPaths.join("/") + "/";
        }
        return path;
    }


    public static getNowTime(): ITime {
        const nowTime: Date = new Date();
        let h: number = nowTime.getHours();
        const m: number = nowTime.getMinutes();
        const s: number = nowTime.getSeconds();
        const ms: number = nowTime.getMilliseconds();
        if (h > 11) {
            h = h - 12;
        }
        return {h: h, m: m, s: s, ms: ms};
    }


    public static getNowUnixTime(): number {
        const date = new Date();
        const time = date.getTime();
        return Math.floor(time / 1000);
    }

    public static getDiffDay(currentTime: number): number {
        const diff: number = Math.abs(Core.getUnixTime() - currentTime);
        return diff / (60 * 60 * 24);
    }

    /**
     * 16進数カラーを取得
     * @param data
     * @returns {string}
     */
    public static rgbToHex(data: string): string {
        return data.match(/\d+/g).map((a): string => {
            return ("0" + parseInt(a).toString(16)).slice(-2)
        }).join("");
    }
}
