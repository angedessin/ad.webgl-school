attribute vec3 position;
attribute vec4 color;
attribute vec2 texCoord;
attribute vec4 random;
uniform mat4      mvpMatrix;
uniform float     time;
uniform sampler2D textureUnit;
varying vec4 vColor;
void main(){
    // テクスチャを頂点シェーダで参照する
    // RGBAが入っている
    vec4 samplerColor = texture2D(textureUnit, texCoord);

    // RGB から加重平均を取る（ざっくりと輝度に相当）
    // dot = 内積
    // x * 1.0 + y * 1.0 + z * 1.0
    // float luminance = samplerColor.r + samplerColor.g + samplerColor.b;
    // 上記でも求められる。
    // 深い意味はありませんが内積を必ずしもベクトル演算にしか使えないと思わないように。
    float luminance = dot(samplerColor.rgb, vec3(1.0));

    // ランダムな値からサインとコサインを生成する
    float s = sin(random.x * time * 3.0) * random.z * 0.05;
    float c = cos(random.y * time * 3.0) * random.z * 0.05;

    // 座標変換する前にサインとコサイン、輝度を足し込む
    // zを足し込まれると手前に表示される。
    // 明るい場所ほど手前に来る
    // 白黒の画像でやると面白い
    gl_Position = mvpMatrix * vec4(position + vec3(c, s, luminance), 1.0);

    // 点の大きさにも乱数の影響を与える
    gl_PointSize = 1.0 + 3.0 * random.w;

    vColor = color;
}