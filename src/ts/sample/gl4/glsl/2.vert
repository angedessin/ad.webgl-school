attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
uniform mat4 mvpMatrix;
uniform bool edge;
varying vec3 vNormal;
varying vec4 vColor;
void main(){
    vNormal = normal;
    vColor = color;
    // エッジラインモードのときは法線方向に少しだけ膨らませる
    if(edge){
        gl_Position = mvpMatrix * vec4(position + normal * 0.01, 1.0);
    }else{
        gl_Position = mvpMatrix * vec4(position, 1.0);
    }
}