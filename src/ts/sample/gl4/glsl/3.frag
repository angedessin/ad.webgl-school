precision mediump float;
uniform mat4 nMatrix;
uniform vec3 eyePosition;
uniform vec3 ambient;
varying vec3 vPosition;
varying vec3 vNormal;
varying vec4 vColor;
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));

const float fogStart = 5.0;                  // フォグが掛かり始める最至近距離 @@@
const float fogEnd   = 25.0;                 // 完全にフォグが掛かる最遠距離 @@@
const vec4  fogColor = vec4(vec3(0.7), 1.0); // フォグの色 @@@

void main(){
    // カメラから頂点までの距離を計測 @@@
    // eyePosition : カメラがどこにあるか？
    // 終点 - 始点 = 始点から終点へ向かうベクトル
    // length => ベクトルの長さを計測
    float distance = length(vPosition - eyePosition);

    // 計測した距離からフォグの影響度を示す係数を求める @@@
    // (10.0 - 5.0) / (25.0 - 5.0)
    // 5.0 / 20.0 === 0.25 => 25%
    // clamp => 最小値と最大値を設定してまとめる。この場合、0から1に収める
    float fog = clamp((distance - fogStart) / (fogEnd - fogStart), 0.0, 1.0);

    // 以下はライトの処理が中心
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;
    float diffuse = max(dot(n, lightDirection), 0.2);
    vec3 eye = normalize(vPosition - eyePosition);
    vec3 ref = reflect(eye, n);
    float specular = max(dot(ref, lightDirection), 0.0);
    specular = pow(specular, 5.0);
    vec3 ambientColor = min(ambient + diffuse, 1.0);
    vec4 destColor = vColor * vec4(ambientColor, 1.0);
    destColor += vec4(vec3(specular) + ambient, 0.0);

    // フォグの影響を考慮した色を求める @@@
    // mix 線形補間(線形に混ざり合わせる)
    // 値1、値2、係数(0.0 ~ 1.0)
    // 0.0, 10.0, 0.5 => 5.0
    // 色を塗ってく。背景色に合わせれば溶け込んでいくような色になる
    gl_FragColor = mix(destColor, fogColor, fog);
}