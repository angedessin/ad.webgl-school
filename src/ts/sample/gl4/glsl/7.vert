attribute vec3 position;
attribute vec4 random;
uniform mat4  mvpMatrix;
uniform float time;
void main(){
    float s = sin(random.x * time * 3.0) * random.z * 0.1;
    float c = cos(random.y * time * 3.0) * random.z * 0.1;
    float z = cos((random.x + random.y) * time) * random.z * 0.1;
    gl_Position = mvpMatrix * vec4(position + vec3(c, s, z), 1.0);
    gl_PointSize = 28.0 + 100.0 * random.w;
}