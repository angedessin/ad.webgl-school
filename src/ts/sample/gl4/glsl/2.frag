precision mediump float;
uniform mat4 nMatrix;
uniform bool edge;
varying vec3 vNormal;
varying vec4 vColor;
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));
void main(){
    // エッジラインモードのときは黒っぽい色にして出力
    if(edge){
        gl_FragColor = vec4(0.05, 0.0, 0.0, 1.0);
        return;
    }
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;
    float diffuse = max(dot(n, lightDirection), 0.0);
    float shade = 1.0;
    if(diffuse < 0.1){
        shade = 0.5;
    }else if(diffuse < 0.3){
        shade = 0.7;
    }else if(diffuse < 0.7){
        shade = 0.9;
    }
    gl_FragColor = vColor * vec4(vec3(shade), 1.0);
}