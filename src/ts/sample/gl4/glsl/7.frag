precision mediump float;
uniform sampler2D textureUnit;
void main(){
    gl_FragColor = texture2D(textureUnit, gl_PointCoord.st);
}