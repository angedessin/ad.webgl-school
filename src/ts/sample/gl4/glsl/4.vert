attribute vec3 position;
attribute vec4 color;
uniform mat4 mvpMatrix;
uniform mat4 mMatrix;
varying vec4 vColor;


const float min = 0.5;
const float max = 1.0;

void main(){
    vColor = color;
    vec3 pos = (mMatrix * vec4(position, 1.0)).xyz;
    float size = 1.0 - clamp((pos.z - min) / (max - min), 0.0, 1.0);

    gl_Position = mvpMatrix * vec4(position, 1.0);
    // gl_PointSize = size * 10.0;
    gl_PointSize = 1.0;
}

