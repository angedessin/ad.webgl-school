attribute vec3 position;
attribute vec4 color;
attribute vec4 random;
uniform mat4 mvpMatrix;
uniform float time;
varying vec4 vColor;

void main(){
    vColor = color;


    // ランダムな値からサインとコサインを生成する @@@
    // 時間に対してランダムの値を格納する
    // 値によって円運動の速度が早くなったり、遅くなったりする
    // random.xyは回転運動の速度
    // random.zは回転運動の半径の大きさ
    // random.wは点の大きさ
    float s = sin(random.x * time * 3.0) * random.z * 0.05;
    float c = sin(random.y * time * 3.0) * random.z * 0.05;

    // 座標変換する前にサインとコサインを足し込む
    gl_Position = mvpMatrix * vec4(position + vec3(c, s, 0.0), 1.0);

     // 点の大きさにも乱数の影響を与える
    gl_PointSize = 1.0 + 5.0 * random.w;
}


