attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
uniform mat4 mMatrix;       // model(回転・平行移動・拡大縮小)
uniform mat4 mvpMatrix;     // model * view * projection
varying vec3 vPosition;
varying vec3 vNormal;
varying vec4 vColor;
void main(){
    // モデル座標変換後の、頂点の位置。頂点がどこにいたか分かる
    vPosition = (mMatrix * vec4(position, 1.0)).xyz;
    vNormal = normal;
    vColor = color;
    gl_Position = mvpMatrix * vec4(position, 1.0);
}