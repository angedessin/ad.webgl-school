precision mediump float;
uniform mat4 nMatrix;
varying vec3 vNormal;
varying vec4 vColor;
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));
void main(){
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;
    float diffuse = max(dot(n, lightDirection), 0.0);
    float shade = 1.0;
    // 拡散光の強度を少ない階調に制限する
    if(diffuse < 0.1){
        shade = 0.5;
    }else if(diffuse < 0.3){
        shade = 0.7;
    }else if(diffuse < 0.7){
        shade = 0.9;
    }
    gl_FragColor = vColor * vec4(vec3(shade), 1.0);
}