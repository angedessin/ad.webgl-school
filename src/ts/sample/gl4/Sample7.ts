import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import MouseEvent from "../../libs/events/MouseEvent";
import Event from "../../libs/events/Event";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/7.vert");
const fs: string = require("./glsl/7.frag");
declare const gl3: any;

export default class Sample6 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;

    private _position: number[];
    private _VBO: WebGLBuffer[];


    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    // カメラ関連
    private _cameraDistance: number = 5.0;
    private _cameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    private _centerPoint: number[] = [0.0, 0.0, 0.0];
    private _cameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _dCameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    // private _dCenterPoint: number[] = [0.0, 0.0, 0.0];
    private _dCameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _cameraRotateX: number = 0.0;
    private _cameraRotateY: number = 0.0;
    private _cameraScale: number = 0.0;
    private _clickStart: boolean = false;
    private _prevPosition: number[] = [0, 0];
    private _offsetPosition: number[] = [0, 0];
    private _qt = gl3.qtn.identity(gl3.qtn.create());
    private _qtx = gl3.qtn.identity(gl3.qtn.create());
    private _qty = gl3.qtn.identity(gl3.qtn.create());

    // canvasサイズ
    private _cw: number;
    private _ch: number;


    constructor() {
        super();
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseWheel = this._onMouseWheel.bind(this);
        this._cameraUpdate = this._cameraUpdate.bind(this);


        this._setCanvas();

        gl3.create_texture(
            "assets/images/gl3/ph_lenna.jpg",
            0, (): void => {
                this._initProgram();
                this._initBuffers();
                this._initCamera();
                this._initScene();
                this._render();
            }
        );


    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        // イベントの登録
        gl3.canvas.addEventListener(MouseEvent.MOUSE_DOWN, this._onMouseDown, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_UP, this._onMouseUp, false);
        gl3.canvas.addEventListener(Event.MOUSE_WHEEL, this._onMouseWheel, false);


        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "random"],
            [3, 4],
            ["mvpMatrix", "time", "textureUnit"],
            ["matrix4fv", "1f", "1i"]
        );
    }

    private _initBuffers(): void {
        const {p, r} = this._getVertex();
        this._position = p;
        this._VBO = [
            gl3.create_vbo(p),
            gl3.create_vbo(r)
        ];

    }

    private _getVertex(): { p: number[], c: number[], r: number[], t: number[] } {
        const position: number[] = [];
        const color: number[] = [];
        const random: number[] = [];
        const texCoord: number[] = [];
        const width: number = 6.0;
        const half: number = width / 2.0;
        const resolution: number = 10;
        const offset: number = width / resolution;
        let x: number;
        let y: number;
        let s: number;
        let t: number;

        for (let i: number = 0; i < resolution; i++) {
            x = -half + offset * i;
            s = i / resolution;
            for (let j: number = 0; j < resolution; j++) {
                y = -half + offset * j;
                t = 1.0 - j / resolution;
                position.push(x, y, 0.0);
                color.push(0.2, 0.7, 1.0, 0.5);
                texCoord.push(s, t);
                random.push(
                    Math.random(),
                    Math.random(),
                    Math.random(),
                    Math.random()
                );
            }
        }

        return {
            p: position,
            c: color,
            r: random,
            t: texCoord
        };

    }

    private _initCamera(): void {

    }

    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);

        this._gl.enable(this._gl.BLEND);
        this._gl.blendFuncSeparate(this._gl.SRC_ALPHA, this._gl.ONE, this._gl.ONE, this._gl.ONE);


        this._gl.activeTexture(this._gl.TEXTURE0);
        this._gl.bindTexture(this._gl.TEXTURE_2D, gl3.textures[0].texture);

    }

    protected _render(): void {
        super._render();
        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();

        // カメラの設定
        this._cameraUpdate();
        const camera = gl3.camera.create(
            this._cameraPosition,
            this._centerPoint,
            this._cameraUpDirection,
            60, this._cw / this._ch, 0.1, this._cameraDistance * 5.0
        );


        // ビュー・プロジェクション行列
        gl3.mat4.vpFromCamera(camera, this._vMatrix, this._pMatrix, this._vpMatrix);


        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        gl3.scene_clear([0.0, 0.0, 0.0, 1.0], 1.0);

        this._prg.set_program();

        this._nowTime = (Date.now() - this._startTime) / 1000;

        // プログラムにトーラスのVBOとIBO
        this._prg.set_attribute(this._VBO);

        // モデル座標変換
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime * 0.1, [0.0, 1.0, 0.0], this._mMatrix);

        // 行列を掛け合わせる
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);


        // uniform 変数をシェーダーにプッシュ
        this._prg.push_shader([
            this._mvpMatrix,
            this._nowTime,
            0
        ]);

        // プログラムにトーラスのVBOとIBO
        gl3.draw_arrays(gl3.gl.POINTS, this._position.length / 3);
        // gl3.draw_arrays(this._gl.LINES, this._position.length / 3);
        // gl3.draw_arrays(this._gl.LINE_STRIP, this._position.length / 3);

        this._gl.flush();

        requestAnimationFrame(this._render);
    }


    private _onMouseDown(e): void {
        this._clickStart = true;
        this._prevPosition = [e.pageX, e.pageY];
        e.preventDefault();
    }

    private _onMouseMove(e): void {
        if (!this._clickStart) {
            return;
        }
        const w: number = gl3.canvas.width;
        const h: number = gl3.canvas.height;
        const s: number = 1.0 / Math.min(w, h);
        this._offsetPosition = [
            e.pageX - this._prevPosition[0],
            e.pageY - this._prevPosition[1]
        ];
        this._prevPosition = [e.pageX, e.pageY];
        switch (e.buttons) {
            case 1:
                this._cameraRotateX += this._offsetPosition[0] * s;
                this._cameraRotateY += this._offsetPosition[1] * s;
                this._cameraRotateX = this._cameraRotateX % 1.0;
                this._cameraRotateY = Math.min(Math.max(this._cameraRotateY % 1.0, -0.25), 0.25);
                break;
        }
    }

    private _onMouseUp(e): void {
        this._clickStart = false;
    }

    private _onMouseWheel(e): void {
        const w: number = e.wheelDelta;
        if (w > 0) {
            this._cameraScale = 0.8;
        } else if (w < 0) {
            this._cameraScale = -0.8;
        }
    }

    private _cameraUpdate(): void {
        const v: number[] = [1.0, 0.0, 0.0];
        this._cameraScale *= 0.75;
        this._cameraDistance += this._cameraScale;
        this._cameraDistance = Math.min(Math.max(this._cameraDistance, 5.0), 20.0);
        this._dCameraPosition[2] = this._cameraDistance;
        gl3.qtn.identity(this._qt);
        gl3.qtn.identity(this._qtx);
        gl3.qtn.identity(this._qty);
        gl3.qtn.rotate(this._cameraRotateX * gl3.PI2, [0.0, 1.0, 0.0], this._qtx);
        gl3.qtn.toVecIII(v, this._qtx, v);
        gl3.qtn.rotate(this._cameraRotateY * gl3.PI2, v, this._qty);
        gl3.qtn.multiply(this._qtx, this._qty, this._qt);
        gl3.qtn.toVecIII(this._dCameraPosition, this._qt, this._cameraPosition);
        gl3.qtn.toVecIII(this._dCameraUpDirection, this._qt, this._cameraUpDirection);
    }
}

