import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/1.vert");
const fs: string = require("./glsl/1.frag");
declare const gl3: any;

export default class Sample1 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _normal: Array<number>;
    private _index: Array<number>;
    private _VBO: Array<WebGLBuffer>;
    private _IBO: WebGLBuffer;

    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _nMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._initCamera();
        this._initScene();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "normal", "color"],
            [3, 3, 4],
            ["mvpMatrix", "nMatrix"],
            ["matrix4fv", "matrix4fv"]
        );
    }

    private _initBuffers(): void {
        const torus = gl3.mesh.torus(32, 32, 0.5, 1.0, [1.0, 1.0, 1.0, 1.0]);
        this._position = torus.position;
        this._color = torus.color;
        this._normal = torus.normal;
        this._index = torus.index;

        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._normal),
            gl3.create_vbo(this._color)
        ];

        this._IBO = gl3.create_ibo(this._index);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 5.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);

        this._prg.set_program();
        this._prg.set_attribute(this._VBO, this._IBO);

        this._nowTime = (Date.now() - this._startTime) / 1000;
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [1.0, 1.0, 0.0], this._mMatrix);

        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        // - 法線変換用の行列 ---------------------------------------------
        // 法線は、頂点がどちらを向いているのかを表すベクトル。
        // つまり、頂点が「モデル座標変換によって動く」とき、一緒に法線も変換しないと
        // 法線の向きが一定のままとなりおかしな状態になります。
        // 一般に、ライティングを行う際はモデル座標変換の状態を元に、
        // 法線変換用の行列を別途生成してシェーダに渡す。
        // 通常、法線用の行列にはモデル座標変換の逆転置行列を用いる。
        // ----------------------------------------------------------------
        gl3.mat4.transpose(this._mMatrix, this._nMatrix);
        gl3.mat4.inverse(this._nMatrix, this._nMatrix);

        this._prg.push_shader([this._mvpMatrix, this._nMatrix]);
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);

        this._gl.flush();

        requestAnimationFrame(this._render);
    }
}

