import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import MouseEvent from "../../libs/events/MouseEvent";
import Event from "../../libs/events/Event";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/11.vert");
const fs: string = require("./glsl/11.frag");

declare const gl3: any;
declare const dat: any;


export default class Sample11 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;

    // sphere1
    private _index1: number[];
    private _VBO1: WebGLBuffer[];
    private _IBO1: WebGLBuffer;

    // sphere2
    private _index2: number[];
    private _VBO2: WebGLBuffer[];
    private _IBO2: WebGLBuffer;

    private _gui: dat.GUI = new dat.GUI();


    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _nMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    // カメラ関連
    private _cameraDistance: number = 5.0;
    private _cameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    private _centerPoint: number[] = [0.0, 0.0, 0.0];
    private _cameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _dCameraPosition: number[] = [0.0, 0.0, this._cameraDistance];

    // private _dCenterPoint: number[] = [0.0, 0.0, 0.0];
    private _dCameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _cameraRotateX: number = 0.0;
    private _cameraRotateY: number = 0.0;
    private _cameraScale: number = 0.0;
    private _clickStart: boolean = false;
    private _prevPosition: number[] = [0, 0];
    private _offsetPosition: number[] = [0, 0];
    private _qt = gl3.qtn.identity(gl3.qtn.create());
    private _qtx = gl3.qtn.identity(gl3.qtn.create());
    private _qty = gl3.qtn.identity(gl3.qtn.create());

    // light
    private _lightDirection: number[] = [1.0, 1.0, 1.0];
    private _ambient: number[] = [0.2, 0.2, 0.2];

    // アルファブレンディング
    private _alpha: number = 0.75;
    private _isBlendMode: boolean = true;
    private _useCulling: boolean = true;
    private _isHide: boolean = false;

    // canvasサイズ
    private _cw: number;
    private _ch: number;


    // テクスチャ
    private _texture: WebGLTexture;
    private _count: number = 0;

    constructor() {
        super();
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseWheel = this._onMouseWheel.bind(this);
        this._cameraUpdate = this._cameraUpdate.bind(this);

        this._initGui();
        this._setCanvas();
    }


    private _initGui(): void {
        const controller = {
            alpha: this._alpha,
            blend: this._isBlendMode,
            hide: this._isHide,
            culling: this._useCulling
        };
        this._gui.add(controller, "alpha", 0.0, 1.0).step(0.1).onChange((e): void => {
            this._alpha = e;
        });

        this._gui.add(controller, "blend").onChange((e): void => {
            this._isBlendMode = e;
        });

        this._gui.add(controller, "culling").onChange((e): void => {
            this._useCulling = e;
        });

        this._gui.add(controller, "hide").onChange((e): void => {
            this._isHide = e;
        });

    }

    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";
        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        // イベントの登録
        gl3.canvas.addEventListener(MouseEvent.MOUSE_DOWN, this._onMouseDown, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_UP, this._onMouseUp, false);
        gl3.canvas.addEventListener(Event.MOUSE_WHEEL, this._onMouseWheel, false);


        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();


        const image = new Image();
        image.onload = (): void => {

            // 空のテクスチャオブジェクトを生成
            this._texture = this._gl.createTexture();
            // テクスチャオブジェクトをバインド
            this._gl.bindTexture(this._gl.TEXTURE_2D, this._texture);
            // バインド済みテクスチャオブジェクトに画像を適用
            this._gl.texImage2D(this._gl.TEXTURE_2D, 0, this._gl.RGBA, this._gl.RGBA, this._gl.UNSIGNED_BYTE, image);
            // バインド済みテクスチャのミップマップを自動生成
            this._gl.generateMipmap(this._gl.TEXTURE_2D);
            // バインドを解除
            this._gl.bindTexture(this._gl.TEXTURE_2D, null);

            // ロジックの呼び出し
            this._initProgram();
            this._initBuffers();
            this._initCamera();
            this._initScene();
            this._render();
        };

        image.src = "assets/images/gl3/ph_sample.jpg";
    }

    private _initProgram(): void {
        const attributeNames: string[] = ["position", "normal", "color", "texCoord"];
        const strides: number[] = [3, 3, 4, 2];
        const uniformNames: string[] = [
            "mMatrix", "mvpMatrix", "nMatrix",
            "eyePosition", "ambient", "direction",
            "alpha", "textureUnit", "useTexture"
        ];

        const uniformTypes: string[] = [
            "matrix4fv", "matrix4fv", "matrix4fv",
            "3fv", "3fv", "3fv",
            "1f", "1i", "1f"
        ];

        this._prg = gl3.program.create_from_source(vs, fs, attributeNames, strides, uniformNames, uniformTypes);
    }

    private _initBuffers(): void {
        const data1 = this._createBuffer(gl3.mesh.sphere(64, 64, 1.5, [0.5, 0.5, 1.0, 1.0]));
        this._VBO1 = data1.vbo;
        this._IBO1 = data1.ibo;
        this._index1 = data1.index;

        const data2 = this._createBuffer(gl3.mesh.sphere(64, 64, 1.0, [0.5, 0.9, 1.0, 1.0]));
        this._VBO2 = data2.vbo;
        this._IBO2 = data2.ibo;
        this._index2 = data2.index;
    }


    private _createBuffer(sphere): { index: number[], vbo: WebGLBuffer[], ibo: WebGLBuffer } {
        const index: number[] = sphere.index;
        const vbo: WebGLBuffer[] = [
            gl3.create_vbo(sphere.position),
            gl3.create_vbo(sphere.normal),
            gl3.create_vbo(sphere.color),
            gl3.create_vbo(sphere.texCoord)
        ];
        const ibo: WebGLBuffer = gl3.create_ibo(index);

        return {
            index: index,
            vbo: vbo,
            ibo: ibo
        }
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 5.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        // カリングする面は裏面
        this._gl.cullFace(this._gl.BACK);

        this._gl.activeTexture(this._gl.TEXTURE0);
        this._gl.bindTexture(this._gl.TEXTURE_2D, this._texture);
    }

    protected _render(): void {
        super._render();
        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();

        // カメラの設定
        this._cameraUpdate();
        const camera = gl3.camera.create(
            this._cameraPosition,
            this._centerPoint,
            this._cameraUpDirection,
            60, this._cw / this._ch, 0.1, this._cameraDistance * 2.0
        );

        // ビュー・プロジェクション行列
        gl3.mat4.vpFromCamera(camera, this._vMatrix, this._pMatrix, this._vpMatrix);


        // シーン設定
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.0, 0.0, 0.0, 1.0]);

        this._count += 0.5;
        this._nowTime = (Date.now() - this._startTime) / 1000;
        this._prg.set_program();

        if (this._useCulling) {
            // カリングを有効化
            this._gl.enable(this._gl.CULL_FACE);
        } else {
            // カリングを無効化
            this._gl.disable(this._gl.CULL_FACE);
        }

        // ブレンドの設定
        if (this._isBlendMode) {
            // アルファブレンドON
            this._gl.enable(this._gl.BLEND);
            this._gl.blendFuncSeparate(this._gl.SRC_ALPHA, this._gl.ONE_MINUS_SRC_ALPHA, this._gl.ONE, this._gl.ONE);

            // 深度テストを無効
            this._gl.disable(this._gl.DEPTH_TEST);
        } else {
            // アルファブレントOFF
            this._gl.disable(this._gl.BLEND);

            // 深度テストを有効
            this._gl.enable(this._gl.DEPTH_TEST);
        }

        // テクスチャ裏面の描画
        this._gl.cullFace(this._gl.FRONT);
        this._drawSphere(this._VBO1, this._IBO1, this._index1, this._alpha, 1.0);

        // 球体の描画
        this._gl.cullFace(this._gl.BACK);
        this._drawSphere(this._VBO2, this._IBO2, this._index2, !this._isHide ? 1.0 : 0.0, 0.0); // 球体

        // テクスチャ表面の描画
        this._gl.cullFace(this._gl.BACK);
        this._drawSphere(this._VBO1, this._IBO1, this._index1, this._alpha, 1.0);


        this._gl.flush();

        requestAnimationFrame(this._render);
    }

    private _drawSphere(vbo, ibo, index, alpha: number, useTexture: number) {
        const rad: number = (this._count % 360) * Math.PI / 180;
        const x: number = Math.cos(rad);
        const z: number = Math.sin(rad);

        this._prg.set_attribute(vbo, ibo);
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.translate(this._mMatrix, [x, 0.0, z], this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [0.0, 1.0, 0.0], this._mMatrix);
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        // 法線変換行列の生成
        gl3.mat4.transpose(this._mMatrix, this._nMatrix);
        gl3.mat4.inverse(this._nMatrix, this._nMatrix);

        this._prg.push_shader([
            this._mMatrix, this._mvpMatrix, this._nMatrix,
            this._cameraPosition, this._ambient, this._lightDirection,
            alpha, 0, useTexture
        ]);

        gl3.draw_elements(gl3.gl.TRIANGLES, index.length);
    }


    private _onMouseDown(e): void {
        this._clickStart = true;
        this._prevPosition = [e.pageX, e.pageY];
        e.preventDefault();
    }

    private _onMouseMove(e): void {
        if (!this._clickStart) {
            return;
        }
        const w: number = gl3.canvas.width;
        const h: number = gl3.canvas.height;
        const s: number = 1.0 / Math.min(w, h);
        this._offsetPosition = [
            e.pageX - this._prevPosition[0],
            e.pageY - this._prevPosition[1]
        ];
        this._prevPosition = [e.pageX, e.pageY];
        switch (e.buttons) {
            case 1:
                this._cameraRotateX += this._offsetPosition[0] * s;
                this._cameraRotateY += this._offsetPosition[1] * s;
                this._cameraRotateX = this._cameraRotateX % 1.0;
                this._cameraRotateY = Math.min(Math.max(this._cameraRotateY % 1.0, -0.25), 0.25);
                break;
        }
    }

    private _onMouseUp(e): void {
        this._clickStart = false;
    }

    private _onMouseWheel(e): void {
        const w: number = e.wheelDelta;
        if (w > 0) {
            this._cameraScale = 0.8;
        } else if (w < 0) {
            this._cameraScale = -0.8;
        }
    }

    private _cameraUpdate(): void {
        const v: number[] = [1.0, 0.0, 0.0];
        this._cameraScale *= 0.75;
        this._cameraDistance += this._cameraScale;
        this._cameraDistance = Math.min(Math.max(this._cameraDistance, 5.0), 20.0);
        this._dCameraPosition[2] = this._cameraDistance;
        gl3.qtn.identity(this._qt);
        gl3.qtn.identity(this._qtx);
        gl3.qtn.identity(this._qty);
        gl3.qtn.rotate(this._cameraRotateX * gl3.PI2, [0.0, 1.0, 0.0], this._qtx);
        gl3.qtn.toVecIII(v, this._qtx, v);
        gl3.qtn.rotate(this._cameraRotateY * gl3.PI2, v, this._qty);
        gl3.qtn.multiply(this._qtx, this._qty, this._qt);
        gl3.qtn.toVecIII(this._dCameraPosition, this._qt, this._cameraPosition);
        gl3.qtn.toVecIII(this._dCameraUpDirection, this._qt, this._cameraUpDirection);
    }
}

