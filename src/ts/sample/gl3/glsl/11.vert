attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
attribute vec2 texCoord;
uniform mat4 mMatrix;
uniform mat4 mvpMatrix;
//uniform float alpha;
varying vec3 vPosition;
varying vec3 vNormal;
varying vec4 vColor;
varying vec2 vTexCoord;

void main() {
    vPosition = (mMatrix * vec4(position, 1.0)).xyz;
    vNormal = normal;
    vTexCoord = texCoord;
    // vColor = vec4(color.rgb, color.a * alpha);
    vColor = color;
    gl_Position = mvpMatrix * vec4(position, 1.0);

}

