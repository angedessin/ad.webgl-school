precision mediump float;
uniform sampler2D textureUnit; // テクスチャユニット
varying vec2 vTexCoord;
void main(){
    // テクスチャからテクスチャ座標を元に画素を参照して色を取り出す
    gl_FragColor = texture2D(textureUnit, vTexCoord);
}