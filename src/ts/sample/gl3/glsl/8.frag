precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;
void main(){
    gl_FragColor = texture2D(textureUnit, vTexCoord);
}