precision mediump float;
uniform mat4 nMatrix;
uniform vec3 eyePosition;
uniform vec3 direction;
uniform vec3 ambient;
uniform float alpha;
uniform sampler2D textureUnit;
uniform float useTexture;
varying vec3 vPosition;
varying vec3 vNormal;
varying vec4 vColor;
varying vec2 vTexCoord;

void main() {
    vec3 lightDirection  = normalize(direction);

    // 法線を行列で変換
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;

    // 拡散光の内積を算出
    float diffuse = max(dot(n, lightDirection), 0.0);

    // カメラから対象のフラグメントへ向かうベクトル
    vec3 eye = normalize(vPosition - eyePosition);

    // 視線ベクトルの反射ベクトルを求める
    vec3 ref = reflect(eye, n);

    // 視線ベクトルの反射ベクトルを求める
    float specular = max(dot(ref, lightDirection), 0.0);

    // 反射光を強調するためにべき乗する
    specular = pow(specular, 5.0);

    // 拡散光と反射光、環境光の影響を考慮して出力カラーを計算
    vec3 ambientColor = min(ambient + diffuse, 1.0);
    vec4 destColor;

    if(useTexture > 0.0) {
        destColor =  texture2D(textureUnit, vTexCoord) * vec4(ambientColor, 1.0);
    } else {
        destColor = vColor * vec4(ambientColor, 1.0);
    }

    gl_FragColor = vec4(destColor.r, destColor.g ,destColor.b, destColor.a * alpha) + vec4(vec3(specular) + ambient, 0.0);
}