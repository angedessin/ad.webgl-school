attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
uniform mat4 mMatrix;       // モデル座標変換行列
uniform mat4 mvpMatrix;     // 座標変換行列
varying vec3 vPosition;     // 頂点座標をフラグメントシェーダーに渡す
varying vec3 vNormal;
varying vec4 vColor;


void main(){
    // 頂点の純粋な座標をフラグメントシェーダーに渡す
    vPosition = (mMatrix * vec4(position, 1.0)).xyz;
    vNormal = normal;
    vColor = color;
    gl_Position = mvpMatrix * vec4(position, 1.0);
}

