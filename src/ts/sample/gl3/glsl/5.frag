precision mediump float;
uniform mat4 nMatrix;
uniform vec3 eyePosition;
uniform vec3 ambient; // 環境光
varying vec3 vPosition;
varying vec3 vNormal;
varying vec4 vColor;
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));
void main(){
    // 法線を変換
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;
    // diffuse（拡散光）
    float diffuse = max(dot(n, lightDirection), 0.0);

    // カメラから対象のフラグメントへと向かうベクトル
    vec3 eye = normalize(vPosition - eyePosition);
    // 視線ベクトルの反射ベクトルを求める
    vec3 ref = reflect(eye, n);
    // specular（反射光のベース）
    float specular = max(dot(ref, lightDirection), 0.0);
    // 反射効果を強調するためにべき算する
    specular = pow(specular, 5.0);

    // 拡散光と反射光、環境光の影響を考慮して出力カラーを計算
    vec3 ambientColor = min(ambient + diffuse, 1.0);
    vec4 destColor = vColor * vec4(ambientColor, 1.0);
    gl_FragColor = destColor + vec4(vec3(specular) + ambient, 0.0);
}