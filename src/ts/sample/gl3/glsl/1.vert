attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
uniform mat4 mvpMatrix;     // 座標変換行列
uniform mat4 nMatrix;       // 法線変換行列
varying vec4 vColor;


// ライトの向き
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));

void main(){
    // 法線を行列で変換
    vec3 n = (nMatrix * vec4(normal, 0.0)).xyz;

    // 拡散光の影響度を内積を使って算出
    float diffuse = max(dot(n, lightDirection), 0.0);

    // 拡散光の係数を頂点カラーに乗算
    vColor = color * vec4(vec3(diffuse), 1.0);

    gl_Position = mvpMatrix * vec4(position, 1.0);
}

