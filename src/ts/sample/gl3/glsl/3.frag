precision mediump float;
uniform mat4 nMatrix;
varying vec3 vNormal;
varying vec4 vColor;

// ライトの向き
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));

void main(){
    // フラグメントシェーダで法線を変換
    vec3 n = (nMatrix * vec4(vNormal, 0.0)).xyz;

    // 内積の計算もフラグメントシェーダで
    float diffuse = max(dot(n, lightDirection), 0.0);

    // 拡散光の係数を頂点カラーに乗算
    gl_FragColor = vColor * vec4(vec3(diffuse), 1.0);
}