import Event from "../../libs/events/Event";
import MouseEvent from "../../libs/events/MouseEvent";
import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/8.vert");
const fs: string = require("./glsl/8.frag");
declare const gl3: any;

export default class Sample8 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _cw: number;
    private _ch: number;

    private _gl: WebGLRenderingContext;
    private _prg;
    private _position: number[];
    private _index: number[];
    private _VBO: Array<WebGLBuffer>;
    private _IBO: WebGLBuffer;

    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    // カメラ関連
    private _cameraDistance: number = 3.0;
    private _cameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    private _centerPoint: number[] = [0.0, 0.0, 0.0];
    private _cameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _dCameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    // private _dCenterPoint: number[] = [0.0, 0.0, 0.0];
    private _dCameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _cameraRotateX: number = 0.0;
    private _cameraRotateY: number = 0.0;
    private _cameraScale: number = 0.0;
    private _clickStart: boolean = false;
    private _prevPosition: number[] = [0, 0];
    private _offsetPosition: number[] = [0, 0];
    private _qt = gl3.qtn.identity(gl3.qtn.create());
    private _qtx = gl3.qtn.identity(gl3.qtn.create());
    private _qty = gl3.qtn.identity(gl3.qtn.create());

    // テクスチャ
    private _texture: WebGLTexture;
    private _texCoord: number[];


    constructor() {
        super();
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseWheel = this._onMouseWheel.bind(this);
        this._cameraUpdate = this._cameraUpdate.bind(this);

        this._setCanvas();


    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        // イベントの登録
        gl3.canvas.addEventListener(MouseEvent.MOUSE_DOWN, this._onMouseDown, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_UP, this._onMouseUp, false);
        gl3.canvas.addEventListener(Event.MOUSE_WHEEL, this._onMouseWheel, false);

        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._cw = Core.getInnerH();

        const image = new Image();
        image.onload = (): void => {

            // 空のテクスチャオブジェクトを生成
            this._texture = this._gl.createTexture();
            // テクスチャオブジェクトをバインド
            this._gl.bindTexture(this._gl.TEXTURE_2D, this._texture);
            // バインド済みテクスチャオブジェクトに画像を適用
            this._gl.texImage2D(this._gl.TEXTURE_2D, 0, this._gl.RGBA, this._gl.RGBA, this._gl.UNSIGNED_BYTE, image);
            // バインド済みテクスチャのミップマップを自動生成
            this._gl.generateMipmap(this._gl.TEXTURE_2D);
            // バインドを解除
            this._gl.bindTexture(this._gl.TEXTURE_2D, null);

            // ロジックの呼び出し
            this._initProgram();
            this._initBuffers();
            this._initCamera();
            this._initScene();
            this._render();
        };

        image.src = "assets/images/gl3/sample.png";

    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "texCoord"],
            [3, 2],
            ["mvpMatrix", "textureUnit"],
            ["matrix4fv", "1i"]
        );
    }

    private _initBuffers(): void {
        this._position = [
            -1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0
        ];

        this._texCoord = [
            0.0, 0.0,
            1.0, 0.0,
            0.0, 1.0,
            1.0, 1.0
        ];

        this._index = [
            0, 2, 1,
            1, 2, 3,
            0, 1, 2,
            2, 1, 3
        ];


        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._texCoord)
        ];

        this._IBO = gl3.create_ibo(this._index);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 5.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);

        // アクティブなテクスチャ
        this._gl.activeTexture(this._gl.TEXTURE0);
        // テクスチャをバインド
        this._gl.bindTexture(this._gl.TEXTURE_2D, this._texture);

    }

    protected _render(): void {
        super._render();
        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();


        // カメラの設定
        this._cameraUpdate();
        const camera = gl3.camera.create(
            this._cameraPosition,
            this._centerPoint,
            this._cameraUpDirection,
            60, this._cw / this._ch, 0.1, this._cameraDistance * 2.0
        );


        // ビュー・プロジェクション行列
        gl3.mat4.vpFromCamera(camera, this._vMatrix, this._pMatrix, this._vpMatrix);


        // シーンのクリア
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0], 1.0);
        // プログラムをセット
        this._prg.set_program();
        // プログラムに VBO と IBO をアタッチ
        this._prg.set_attribute(this._VBO, this._IBO);
        // 時間の経過を得る（Date.now は現在時刻のタイムスタンプをミリ秒で返す）
        this._nowTime = (Date.now() - this._startTime) / 1000;
        // モデル座標変換
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [0.0, 0.0, 1.0], this._mMatrix);
        // 行列を掛け合わせる
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);
        this._prg.push_shader([this._mvpMatrix, 0]);


        // . （左下） gl.NEAREST ..........................................
        // いわゆるニアレストネイバー法。
        // 参照した画素の色をそのまま採用する。
        // ................................................................
        // ビューポートを設定する
        gl3.scene_view(camera, 0, 0, this._cw / 2, this._ch / 2);
        // テクスチャパラメータの設定
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MAG_FILTER, this._gl.NEAREST);
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MIN_FILTER, this._gl.NEAREST);
        // ドローコール（描画命令）
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
        // . （右下） gl.LINEAR ..........................................
        // いわゆるバイリニアフィルタリング。
        // 周辺の画素も参照しながら線形補間した結果を利用する。
        // ................................................................
        // ビューポートを設定する
        gl3.scene_view(camera, this._cw / 2, 0, this._cw / 2, this._ch / 2);
        // テクスチャパラメータの設定
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MAG_FILTER, this._gl.LINEAR);
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MIN_FILTER, this._gl.LINEAR);
        // ドローコール（描画命令）
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
        // . （左上） gl.NEAREST_MIPMAP_NEAREST ..........................
        // ミップマップを利用してニアレストネイバー法を適用する。
        // ................................................................
        // ビューポートを設定する
        gl3.scene_view(camera, 0, this._ch / 2, this._cw / 2, this._ch / 2);
        // テクスチャパラメータの設定
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MAG_FILTER, this._gl.LINEAR);
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MIN_FILTER, this._gl.NEAREST_MIPMAP_NEAREST);
        // ドローコール（描画命令）
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
        // . （右上） gl.LINEAR_MIPMAP_LINEAR .............................
        // ミップマップを利用してさらにリニアで補間する。
        // ................................................................
        // ビューポートを設定する
        gl3.scene_view(camera, this._cw / 2, this._ch / 2, this._cw / 2, this._ch / 2);
        // テクスチャパラメータの設定
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MAG_FILTER, this._gl.LINEAR);
        this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_MIN_FILTER, this._gl.LINEAR_MIPMAP_LINEAR);
        // ドローコール（描画命令）
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
        this._gl.flush();

        requestAnimationFrame(this._render);
    }


    private _onMouseDown(e): void {
        this._clickStart = true;
        this._prevPosition = [e.pageX, e.pageY];
        e.preventDefault();
    }

    private _onMouseMove(e): void {
        if (!this._clickStart) {
            return;
        }
        const w: number = gl3.canvas.width;
        const h: number = gl3.canvas.height;
        const s: number = 1.0 / Math.min(w, h);
        this._offsetPosition = [
            e.pageX - this._prevPosition[0],
            e.pageY - this._prevPosition[1]
        ];
        this._prevPosition = [e.pageX, e.pageY];
        switch (e.buttons) {
        case 1:
            this._cameraRotateX += this._offsetPosition[0] * s;
            this._cameraRotateY += this._offsetPosition[1] * s;
            this._cameraRotateX = this._cameraRotateX % 1.0;
            this._cameraRotateY = Math.min(Math.max(this._cameraRotateY % 1.0, -0.25), 0.25);
            break;
        }
    }

    private _onMouseUp(e): void {
        this._clickStart = false;
    }

    private _onMouseWheel(e): void {
        const w: number = e.wheelDelta;
        if (w > 0) {
            this._cameraScale = 0.8;
        } else if (w < 0) {
            this._cameraScale = -0.8;
        }
    }

    private _cameraUpdate(): void {
        const v: number[] = [1.0, 0.0, 0.0];
        this._cameraScale *= 0.75;
        this._cameraDistance += this._cameraScale;
        this._cameraDistance = Math.min(Math.max(this._cameraDistance, 1.5), 20.0);
        this._dCameraPosition[2] = this._cameraDistance;
        gl3.qtn.identity(this._qt);
        gl3.qtn.identity(this._qtx);
        gl3.qtn.identity(this._qty);
        gl3.qtn.rotate(this._cameraRotateX * gl3.PI2, [0.0, 1.0, 0.0], this._qtx);
        gl3.qtn.toVecIII(v, this._qtx, v);
        gl3.qtn.rotate(this._cameraRotateY * gl3.PI2, v, this._qty);
        gl3.qtn.multiply(this._qtx, this._qty, this._qt);
        gl3.qtn.toVecIII(this._dCameraPosition, this._qt, this._cameraPosition);
        gl3.qtn.toVecIII(this._dCameraUpDirection, this._qt, this._cameraUpDirection);
    }
}

