import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import MouseEvent from "../../libs/events/MouseEvent";
import Event from "../../libs/events/Event";
import BaseSample from "../../common/BaseSample";

const vsM: string = require("./glsl/1_main.vert");
const fsM: string = require("./glsl/1_main.frag");
const vsP: string = require("./glsl/1_post.vert");
const fsP: string = require("./glsl/1_post.frag");

declare const gl3: any;

export default class Sample1 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;

    private _mainPrg;
    private _postPrg;

    // sphere
    private _sphereIndex: number[];
    private _sphereVBO: WebGLBuffer[];
    private _sphereIBO: WebGLBuffer;

    // plane
    private _planeIndex: number[];
    private _planeVBO: WebGLBuffer[];
    private _planeIBO: WebGLBuffer;


    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;


    // カメラ関連
    private _cameraDistance: number = 3.0;
    private _cameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    private _centerPoint: number[] = [0.0, 0.0, 0.0];
    private _cameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _dCameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    // private _dCenterPoint: number[] = [0.0, 0.0, 0.0];
    private _dCameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _cameraRotateX: number = 0.0;
    private _cameraRotateY: number = 0.0;
    private _cameraScale: number = 0.0;
    private _clickStart: boolean = false;
    private _prevPosition: number[] = [0, 0];
    private _offsetPosition: number[] = [0, 0];
    private _qt = gl3.qtn.identity(gl3.qtn.create());
    private _qtx = gl3.qtn.identity(gl3.qtn.create());
    private _qty = gl3.qtn.identity(gl3.qtn.create());

    // canvasサイズ
    private _cw: number;
    private _ch: number;

    private _frameBuffer: WebGLFramebuffer;

    constructor() {
        super();
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseWheel = this._onMouseWheel.bind(this);
        this._cameraUpdate = this._cameraUpdate.bind(this);
        this._shaderLoaded = this._shaderLoaded.bind(this);
        this._initProgram = this._initProgram.bind(this);

        this._setCanvas();

    }

    private IMAGE_PATH: string = "assets/images/gl5/";

    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        // イベントの登録
        gl3.canvas.addEventListener(MouseEvent.MOUSE_DOWN, this._onMouseDown, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_UP, this._onMouseUp, false);
        gl3.canvas.addEventListener(Event.MOUSE_WHEEL, this._onMouseWheel, false);

        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();


        gl3.create_texture(`${this.IMAGE_PATH}test.jpg`, 0, this._initProgram);


    }

    private _initProgram(): void {
        this._mainPrg = gl3.program.create_from_source(
            vsM, fsM,
            ["position", "texCoord"],
            [3, 2],
            ["mvpMatrix", "textureUnit"],
            ["matrix4fv", "1i"]
        );

        this._postPrg = gl3.program.create_from_source(
            vsP, fsP,
            ["position"],
            [3],
            ["textureUnit"],
            ["1i"]
        );
        this._shaderLoaded();
    }

    private _shaderLoaded(): void {
        if (this._mainPrg.prg !== null && this._postPrg.prg !== null && true) {
            this._initBuffers();
            this._initCamera();
            this._initScene();
            this._render();
        }
    }

    private _initBuffers(): void {
        const sphere = gl3.mesh.sphere(64, 64, 1.0, [1.0, 1.0, 1.0, 1.0]);

        this._sphereVBO = [
            gl3.create_vbo(sphere.position),
            gl3.create_vbo(sphere.texCoord)
        ];

        this._sphereIndex = sphere.index;
        this._sphereIBO = gl3.create_ibo(this._sphereIndex);

        // 板ポリゴンの頂点データを定義
        const planePosition: number[] = [
            -1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0
        ];

        const planeIndex: number[] = [
            0, 2, 1,
            1, 2, 3
        ];

        this._planeVBO = [
            gl3.create_vbo(planePosition)
        ];
        this._planeIndex = planeIndex;
        this._planeIBO = gl3.create_ibo(this._planeIndex);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 5.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }


    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);

        // フレームバッファ
        this._frameBuffer = gl3.create_framebuffer(this._cw, this._ch, 1);


        this._gl.activeTexture(this._gl.TEXTURE0);
        this._gl.bindTexture(this._gl.TEXTURE_2D, gl3.textures[0].texture);
        this._gl.activeTexture(this._gl.TEXTURE1);
        this._gl.bindTexture(this._gl.TEXTURE_2D, gl3.textures[1].texture);
    }

    protected _render(): void {
        super._render();
        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();

        // カメラの設定
        this._cameraUpdate();
        const camera = gl3.camera.create(
            this._cameraPosition,
            this._centerPoint,
            this._cameraUpDirection,
            60, this._cw / this._ch, 0.1, this._cameraDistance * 2.0
        );


        // ビュー・プロジェクション行列
        gl3.mat4.vpFromCamera(camera, this._vMatrix, this._pMatrix, this._vpMatrix);


        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0], 1.0);


        this._nowTime = (Date.now() - this._startTime) / 1000;

        this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, this._frameBuffer["framebuffer"]);


        // シーンのクリアとビューの設定
        gl3.scene_view(camera, 0, 0, this._cw, this._ch);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0], 1.0);


        // フレームバッファに対してレンダリングする
        this._mainPrg.set_program();
        this._mainPrg.set_attribute(this._sphereVBO, this._sphereIBO);

        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [0.0, 1.0, 0.0], this._mMatrix);
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        this._mainPrg.push_shader([this._mvpMatrix, 0]);
        gl3.draw_elements(gl3.gl.TRIANGLES, this._sphereIndex.length);


        // スクリーンにポストプロセスで描画する
        this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, null);
        gl3.scene_view(camera, 0, 0, this._cw, this._ch);
        gl3.scene_clear([0.0, 0.0, 0.0, 1.0], 1.0);

        this._postPrg.set_program();
        this._postPrg.set_attribute(this._planeVBO, this._planeIBO);

        this._postPrg.push_shader([1]);

        gl3.draw_elements(gl3.gl.TRIANGLES, this._planeIndex.length);


        this._gl.flush();

        requestAnimationFrame(this._render);
    }


    private _onMouseDown(e): void {
        this._clickStart = true;
        this._prevPosition = [e.pageX, e.pageY];
        e.preventDefault();
    }


    private _onMouseMove(e): void {
        if (!this._clickStart) {
            return;
        }
        const w: number = gl3.canvas.width;
        const h: number = gl3.canvas.height;
        const s: number = 1.0 / Math.min(w, h);
        this._offsetPosition = [
            e.pageX - this._prevPosition[0],
            e.pageY - this._prevPosition[1]
        ];
        this._prevPosition = [e.pageX, e.pageY];
        switch (e.buttons) {
        case 1:
            this._cameraRotateX += this._offsetPosition[0] * s;
            this._cameraRotateY += this._offsetPosition[1] * s;
            this._cameraRotateX = this._cameraRotateX % 1.0;
            this._cameraRotateY = Math.min(Math.max(this._cameraRotateY % 1.0, -0.25), 0.25);
            break;
        }
    }

    private _onMouseUp(e): void {
        this._clickStart = false;
    }

    private _onMouseWheel(e): void {
        const w: number = e.wheelDelta;
        if (w > 0) {
            this._cameraScale = 0.8;
        } else if (w < 0) {
            this._cameraScale = -0.8;
        }
    }


    private _cameraUpdate(): void {
        const v: number[] = [1.0, 0.0, 0.0];
        this._cameraScale *= 0.75;
        this._cameraDistance += this._cameraScale;
        this._cameraDistance = Math.min(Math.max(this._cameraDistance, 3.0), 20.0);
        this._dCameraPosition[2] = this._cameraDistance;
        gl3.qtn.identity(this._qt);
        gl3.qtn.identity(this._qtx);
        gl3.qtn.identity(this._qty);
        gl3.qtn.rotate(this._cameraRotateX * gl3.PI2, [0.0, 1.0, 0.0], this._qtx);
        gl3.qtn.toVecIII(v, this._qtx, v);
        gl3.qtn.rotate(this._cameraRotateY * gl3.PI2, v, this._qty);
        gl3.qtn.multiply(this._qtx, this._qty, this._qt);
        gl3.qtn.toVecIII(this._dCameraPosition, this._qt, this._cameraPosition);
        gl3.qtn.toVecIII(this._dCameraUpDirection, this._qt, this._cameraUpDirection);
    }
}

