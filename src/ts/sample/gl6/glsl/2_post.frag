// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// エッジ検出として比較的メジャーなラプラシアンフィルタを実装します。
// ラプラシアンフィルタのカーネルは uniform 変数として JavaScript 側から受け取る
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
uniform vec2      resolution;   // スクリーンの解像度
uniform float     laplacian[9]; // ラプラシアンフィルタのカーネル
varying vec2 vTexCoord;
const float redScale   = 0.298912;
const float greenScale = 0.586611;
const float blueScale  = 0.114478;
const vec3  monochromeScale = vec3(redScale, greenScale, blueScale);
void main(){
    // 隣のピクセルを参照するためにオフセットする量を求める
    // 座標は0.0 ~ 1.0
    vec2 offsetSize = 1.0 / resolution;     // スクリーン(フレームバッファ)の解像度 = 1 / 1920

    // 出力カラーを計算するための変数を宣言
    vec3 dest = vec3(0.0);

    // 順番にカーネルの値に基づき色を足しこんでいく
    // 1テクセル分下や横などができる
    // カーネルの形のように周辺のテクセルを見てる
    // laplaction[index] = カーネルの係数
    dest += texture2D(textureUnit, vTexCoord + vec2(-1.0,  1.0) * offsetSize).rgb * laplacian[0];
    dest += texture2D(textureUnit, vTexCoord + vec2( 0.0,  1.0) * offsetSize).rgb * laplacian[1];
    dest += texture2D(textureUnit, vTexCoord + vec2( 1.0,  1.0) * offsetSize).rgb * laplacian[2];
    dest += texture2D(textureUnit, vTexCoord + vec2(-1.0,  0.0) * offsetSize).rgb * laplacian[3];
    dest += texture2D(textureUnit, vTexCoord + vec2( 0.0,  0.0) * offsetSize).rgb * laplacian[4];
    dest += texture2D(textureUnit, vTexCoord + vec2( 1.0,  0.0) * offsetSize).rgb * laplacian[5];
    dest += texture2D(textureUnit, vTexCoord + vec2(-1.0, -1.0) * offsetSize).rgb * laplacian[6];
    dest += texture2D(textureUnit, vTexCoord + vec2( 0.0, -1.0) * offsetSize).rgb * laplacian[7];
    dest += texture2D(textureUnit, vTexCoord + vec2( 1.0, -1.0) * offsetSize).rgb * laplacian[8];

    // 内積を使ってモノクロスケールを計算する
    float mono = dot(dest, monochromeScale);
    gl_FragColor = vec4(vec3(mono), 1.0);
}
