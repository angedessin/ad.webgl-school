// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// モザイクシェーダは、テクスチャの色の参照に使われるテクスチャ座標を
// 頂点ベースではなく、スクリーンスペースをベースに算出。
// floorは、小数点以下を切り捨てる関数。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
uniform vec2      resolution; // スクリーン解像度
const float size = 20.0;      // モザイク化する際の矩形の大きさ
void main(){
    // スクリーン座標をサイズで割ってからサイズを掛ける
    // gl_FragCoord : フレームバッファ上の 今まさに処理しようとしているピクセルの位置(0 ~ hogehoge)
    // 0 ~ 100ピクセル
    // floor ==　小数点以下を切り捨て
    // 半端な値を切り捨てて、等倍に戻す
    // gl_FragCoord.xyでも可
    vec2 texCoord = floor(gl_FragCoord.st / size) * size;

    // テクスチャ座標に変換するために解像度で割る
    gl_FragColor = texture2D(textureUnit, texCoord / resolution);
}




