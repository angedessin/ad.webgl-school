// post.frag ------------------------------------------------------------------
// バリューノイズと呼ばれる簡単なノイズ生成のロジックをシェーダのみで実装した例
// ノイズをひとつ扱えるだけで、表現力の底が変わってくる。
// ----------------------------------------------------------------------------
// 1. 乱数生成器
// 2. 保管し合う関数
// 3. 隣と保管しあう
// 4. 乱数を合成
// 5. シームレスノイズ


precision mediump float;
uniform sampler2D textureUnit;
uniform vec2      resolution; // スクリーン解像度
varying vec2 vTexCoord;

const int   oct  = 8;         // オクターブ
const float per  = 0.5;       // パーセンテージ
const float PI   = 3.1415926; // 円周率

// 2つの値を補完するための関数
float interpolate(float a, float b, float x){
    float f = (1.0 - cos(x * PI)) * 0.5;
    return a * (1.0 - f) + b * f;
}

// 乱数生成器
float rnd(vec2 p){
    return fract(sin(dot(p ,vec2(12.9898,78.233))) * 43758.5453);
}

// 保管しながら乱数を処理をする
// 隣の色を保管し合う
float irnd(vec2 p){
    vec2 i = floor(p);
    vec2 f = fract(p);
    vec4 v = vec4(rnd(vec2(i.x,       i.y      )),
                  rnd(vec2(i.x + 1.0, i.y      )),
                  rnd(vec2(i.x,       i.y + 1.0)),
                  rnd(vec2(i.x + 1.0, i.y + 1.0)));
    return interpolate(interpolate(v.x, v.y, f.x), interpolate(v.z, v.w, f.x), f.y);
}

// 複数の乱数を合成をしている
float noise(vec2 p){
    float t = 0.0;
    for(int i = 0; i < oct; i++){
        float freq = pow(2.0, float(i));
        float amp  = pow(per, float(oct - i));
        t += irnd(vec2(p.x / freq, p.y / freq)) * amp;
    }
    return t;
}

// シームレスノイズ
float snoise(vec2 p, vec2 q, vec2 r){
    return noise(vec2(p.x,       p.y      )) *        q.x  *        q.y  +
           noise(vec2(p.x,       p.y + r.y)) *        q.x  * (1.0 - q.y) +
           noise(vec2(p.x + r.x, p.y      )) * (1.0 - q.x) *        q.y  +
           noise(vec2(p.x + r.x, p.y + r.y)) * (1.0 - q.x) * (1.0 - q.y);
}




void main(void){
    // ノイズを得る
    float r = snoise(gl_FragCoord.st, gl_FragCoord.st / resolution, resolution);

    // テクスチャの色
    vec4 samplerColor = texture2D(textureUnit, vTexCoord);
    // テクスチャの色にノイズの値を掛ける
    gl_FragColor = samplerColor * vec4(vec3(r), 1.0);
}

