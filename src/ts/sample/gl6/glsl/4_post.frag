// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// ハーフトーン調の模様を生み出すには結構工夫が要ります。
// ポイントは、gl_FragCoord から sin を用いた計算を行う部分。シェーダにおける演
// 算は得てして、非常にシンプルな数学の問題なので落ち着いて考えましょう。
// 今回は少し複雑なので、紙に書き出すなどしながら考えてみましょう。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;
const float redScale   = 0.298912; // 赤のスケール値
const float greenScale = 0.586611; // 緑のスケール値
const float blueScale  = 0.114478; // 青のスケール値
const vec3  monochromeScale = vec3(redScale, greenScale, blueScale);
void main(){
    // テクスチャをサンプリング
    vec4 samplerColor = texture2D(textureUnit, vTexCoord);
    // モノクロスケールを求める
    float mono = dot(samplerColor.rgb, monochromeScale);
    // モノクロスケールを 4 倍して 2 を引く（0.0 ~ 1.0 が -2.0 ~ 2.0 になる）
    mono = mono * 4.0 - 2.0;
    // ドット模様を作るための計算（0.0 ~ 2.0）
    float crossDot = sin(gl_FragCoord.s) * sin(gl_FragCoord.t) + 1.0;
    // 最終出力カラーを算出
    gl_FragColor = vec4(vec3(crossDot + mono) / 2.0, 1.0);
}
