// main.frag ------------------------------------------------------------------
// メインプログラムのフラグメントシェーダ。
// ここでは単にテクスチャ座標を使ってサンプリングした色を出力。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;
void main(){
    gl_FragColor = texture2D(textureUnit, vTexCoord);
}
