// post.frag ------------------------------------------------------------------
// ガウシアンブラーは 2passで描画を行うシェーダ。
// 横と、縦、それぞれを別々に処理するため、今までのポストプロセスのなかでもパス
// が一度多い分だけ負荷は高め。
// しかし、それに見合った高品質なぼかし効果が得られますので、使いみちは結構いろいろ考えられる。
// ----------------------------------------------------------------------------
precision mediump float;
uniform vec2  resolution; // 画面解像度
uniform bool  horizontal; // 縦横のどちらにぼかすかのフラグ（true == 横）
uniform float weight[20]; // ガウシアンブラーに使うガウス係数(配列)
uniform sampler2D texture;

void main(){
    // 1pxあたりの幅を求めている
    vec2 tFrag = 1.0 / resolution; // フラグメントあたりの大きさ

    // fc = fragCoordの略
    // 今処理されてようとしているピクセルの座標
    vec2 fc = gl_FragCoord.st;     // ピクセルサイズの座標

    // 計算結果の格納用
    vec2 texCoord = vec2(0.0);     // テクスチャ座標格納用

    // 最終出力カラー(本来のテクスチャの色に、釣鐘の中心部分の係数をかけている)
    vec4 destColor = vec4(texture2D(texture, fc * tFrag).rgb * weight[0], 1.0);

    // フラグの状態によってどの方向にシフトするのかを変化させる
    // 1pxごとにブラー処理
    if(horizontal){
        // 20 回の繰り返し処理でぼかしていく
        // ラプラシアンフィルターと同じように1pxずつずらしながらぼかす
        for(int i = 1; i < 20; ++i){
            texCoord = (fc + vec2(float(i), 0.0)) * tFrag;  // 左に 1px シフト
            destColor += vec4(texture2D(texture, texCoord).rgb * weight[i], 1.0);
            texCoord = (fc + vec2(-float(i), 0.0)) * tFrag; // 右に 1px シフト
            destColor += vec4(texture2D(texture, texCoord).rgb * weight[i], 1.0);
        }
    } else {
        for(int i = 1; i < 20; ++i){
            texCoord = (fc + vec2(0.0, float(i))) * tFrag;  // 上に 1px シフト
            destColor += vec4(texture2D(texture, texCoord).rgb * weight[i], 1.0);
            texCoord = (fc + vec2(0.0, -float(i))) * tFrag; // 下に 1px シフト
            destColor += vec4(texture2D(texture, texCoord).rgb * weight[i], 1.0);
        }
    }


    gl_FragColor = destColor;
}

