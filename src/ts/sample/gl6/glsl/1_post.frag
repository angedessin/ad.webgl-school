// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// なにかと汎用性の高いモノクロ化をシェーダを使って実装。
// シェーダを使ってシーンをモノクロ化する場合、
// サンプリングして取得した画素の情報を元に、
// 輝度を算出してそれを出力する色として採用。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;

// 定数
const float redScale   = 0.298912; // 赤のスケール値
const float greenScale = 0.586611; // 緑のスケール値
const float blueScale  = 0.114478; // 青のスケール値
const vec3  monochromeScale = vec3(redScale, greenScale, blueScale);

void main() {
    // 一度フレームバッファに焼いたシーン
    vec4 destColor = texture2D(textureUnit, vTexCoord);

    // 内積を使ってモノクロスケールを計算する
    // x * x + y * y + z * z
    float mono = dot(destColor.rgb, monochromeScale);

    gl_FragColor = vec4(vec3(mono), destColor.a);
}
