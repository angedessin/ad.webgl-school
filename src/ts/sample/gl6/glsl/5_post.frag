// post.frag ------------------------------------------------------------------
// ホワイトノイズ、と言った場合には、ここでの例のように単に砂嵐のようなノイズの
// 値そのままの状態のことを言う。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
uniform float     time; // 時間の経過
varying vec2 vTexCoord;

// ノイズ生成用の関数を定義
float rnd(vec2 n){
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

// fract == 小数点以下の部分だけを抜き出す

void main(){
    // 乱数を得る
    float r = rnd(gl_FragCoord.st + mod(time, 1.0));

    // テクスチャの色
    vec4 samplerColor = texture2D(textureUnit, vTexCoord);

    // テクスチャの色にノイズの値を掛ける
    // ノイズの品質が悪いと規則性が見える
    gl_FragColor = vec4(samplerColor.rgb * r, samplerColor.a);
}
