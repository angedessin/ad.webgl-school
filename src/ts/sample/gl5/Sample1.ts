import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import MouseEvent from "../../libs/events/MouseEvent";
import Event from "../../libs/events/Event";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/1.vert");
const fs: string = require("./glsl/1.frag");
declare const gl3: any;

export default class Sample1 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;

    // torus
    private _torusIndex: number[];
    private _torusVBO: WebGLBuffer[];
    private _torusIBO: WebGLBuffer;

    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _nMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;


    // カメラ関連
    private _cameraDistance: number = 5.0;
    private _cameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    private _centerPoint: number[] = [0.0, 0.0, 0.0];
    private _cameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _dCameraPosition: number[] = [0.0, 0.0, this._cameraDistance];
    // private _dCenterPoint: number[] = [0.0, 0.0, 0.0];
    private _dCameraUpDirection: number[] = [0.0, 1.0, 0.0];
    private _cameraRotateX: number = 0.0;
    private _cameraRotateY: number = 0.0;
    private _cameraScale: number = 0.0;
    private _clickStart: boolean = false;
    private _prevPosition: number[] = [0, 0];
    private _offsetPosition: number[] = [0, 0];
    private _qt = gl3.qtn.identity(gl3.qtn.create());
    private _qtx = gl3.qtn.identity(gl3.qtn.create());
    private _qty = gl3.qtn.identity(gl3.qtn.create());

    // canvasサイズ
    private _cw: number;
    private _ch: number;


    constructor() {
        super();
        this._onMouseDown = this._onMouseDown.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseUp = this._onMouseUp.bind(this);
        this._onMouseWheel = this._onMouseWheel.bind(this);
        this._cameraUpdate = this._cameraUpdate.bind(this);

        this._setCanvas();

    }

    private IMAGE_PATH: string = "assets/images/gl5/";

    // - キューブマップテクスチャの初期化 ---------------------------------
    // キューブ環境マッピングを行う上で、実は一番大変なのは初期化処理
    // 正六面体の各面にマッピングするための合計 6 枚のテクスチャを順番に読み込み、
    // 次々とアタッチする。
    // ポジションとネガティブの、それぞれの意味だけはしっかり理解する
    // またキューブテクスチャは読み込む画像は複数だが、
    // テクスチャオブジェクトとしては単体なので、気をつける必要がある
    // --------------------------------------------------------------------
    private _sourceArr: string[] = [
        `${this.IMAGE_PATH}cube_PX.png`,
        `${this.IMAGE_PATH}cube_PY.png`,
        `${this.IMAGE_PATH}cube_PZ.png`,
        `${this.IMAGE_PATH}cube_NX.png`,
        `${this.IMAGE_PATH}cube_NY.png`,
        `${this.IMAGE_PATH}cube_NZ.png`
    ];

    private _targetArray = [];

    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;


        // キューブマップ用のターゲット定数配列
        // 画像と紐付ける必要がある
        this._targetArray = [
            this._gl.TEXTURE_CUBE_MAP_POSITIVE_X,
            this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
            this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
            this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
            this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
            this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z
        ];


        // イベントの登録
        gl3.canvas.addEventListener(MouseEvent.MOUSE_DOWN, this._onMouseDown, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove, false);
        gl3.canvas.addEventListener(MouseEvent.MOUSE_UP, this._onMouseUp, false);
        gl3.canvas.addEventListener(Event.MOUSE_WHEEL, this._onMouseWheel, false);

        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();


        gl3.create_texture_cube(this._sourceArr, this._targetArray, 0, (): void => {
            this._initProgram();
            this._initBuffers();
            this._initCamera();
            this._initScene();
            this._render();
        });
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "normal"],
            [3, 3],
            ["mMatrix", "mvpMatrix", "nMatrix", "eyePosition", "cubeTexture", "reflection"],
            ["matrix4fv", "matrix4fv", "matrix4fv", "3fv", "1i", "1i"]
        );
    }

    private _initBuffers(): void {
        const torus = gl3.mesh.torus(64, 64, 0.5, 1.5);

        this._torusVBO = [
            gl3.create_vbo(torus.position),
            gl3.create_vbo(torus.normal)
        ];

        this._torusIndex = torus.index;
        this._torusIBO = gl3.create_ibo(this._torusIndex);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 5.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);

        // キューブマップテクスチャをバインドしておく
        this._gl.activeTexture(this._gl.TEXTURE0);
        this._gl.bindTexture(this._gl.TEXTURE_CUBE_MAP, gl3.textures[0].texture);
    }

    protected _render(): void {
        super._render();
        gl3.canvas.width = this._cw = Core.getInnerW();
        gl3.canvas.height = this._ch = Core.getInnerH();

        // カメラの設定
        this._cameraUpdate();
        const camera = gl3.camera.create(
            this._cameraPosition,
            this._centerPoint,
            this._cameraUpDirection,
            60, this._cw / this._ch, 0.1, this._cameraDistance * 2.0
        );


        // ビュー・プロジェクション行列
        gl3.mat4.vpFromCamera(camera, this._vMatrix, this._pMatrix, this._vpMatrix);


        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0], 1.0);


        this._prg.set_program();
        this._prg.set_attribute(this._torusVBO, this._torusIBO);

        this._nowTime = (Date.now() - this._startTime) / 1000;

        // モデル座標変換
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [1.0, 0.0, 1.0], this._mMatrix);

        // 行列を掛け合わせる
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        // 法線変換行列の生成
        gl3.mat4.transpose(this._mMatrix, this._nMatrix);
        gl3.mat4.inverse(this._nMatrix, this._nMatrix);

        // uniform 変数をシェーダーにプッシュ
        this._prg.push_shader([
            this._mMatrix, this._mvpMatrix, this._nMatrix, this._cameraPosition, 0, true
        ]);

        // プログラムにトーラスのVBOとIBO
        gl3.draw_elements(gl3.gl.TRIANGLES, this._torusIndex.length);


        this._gl.flush();

        requestAnimationFrame(this._render);
    }


    private _onMouseDown(e): void {
        this._clickStart = true;
        this._prevPosition = [e.pageX, e.pageY];
        e.preventDefault();
    }

    private _onMouseMove(e): void {
        if (!this._clickStart) {
            return;
        }
        const w: number = gl3.canvas.width;
        const h: number = gl3.canvas.height;
        const s: number = 1.0 / Math.min(w, h);
        this._offsetPosition = [
            e.pageX - this._prevPosition[0],
            e.pageY - this._prevPosition[1]
        ];
        this._prevPosition = [e.pageX, e.pageY];
        switch (e.buttons) {
        case 1:
            this._cameraRotateX += this._offsetPosition[0] * s;
            this._cameraRotateY += this._offsetPosition[1] * s;
            this._cameraRotateX = this._cameraRotateX % 1.0;
            this._cameraRotateY = Math.min(Math.max(this._cameraRotateY % 1.0, -0.25), 0.25);
            break;
        }
    }

    private _onMouseUp(e): void {
        this._clickStart = false;
    }

    private _onMouseWheel(e): void {
        const w: number = e.wheelDelta;
        if (w > 0) {
            this._cameraScale = 0.8;
        } else if (w < 0) {
            this._cameraScale = -0.8;
        }
    }

    private _cameraUpdate(): void {
        const v: number[] = [1.0, 0.0, 0.0];
        this._cameraScale *= 0.75;
        this._cameraDistance += this._cameraScale;
        this._cameraDistance = Math.min(Math.max(this._cameraDistance, 5.0), 20.0);
        this._dCameraPosition[2] = this._cameraDistance;
        gl3.qtn.identity(this._qt);
        gl3.qtn.identity(this._qtx);
        gl3.qtn.identity(this._qty);
        gl3.qtn.rotate(this._cameraRotateX * gl3.PI2, [0.0, 1.0, 0.0], this._qtx);
        gl3.qtn.toVecIII(v, this._qtx, v);
        gl3.qtn.rotate(this._cameraRotateY * gl3.PI2, v, this._qty);
        gl3.qtn.multiply(this._qtx, this._qty, this._qt);
        gl3.qtn.toVecIII(this._dCameraPosition, this._qt, this._cameraPosition);
        gl3.qtn.toVecIII(this._dCameraUpDirection, this._qt, this._cameraUpDirection);
    }
}

