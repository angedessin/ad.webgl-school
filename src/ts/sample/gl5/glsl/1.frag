precision mediump float;
uniform vec3        eyePosition; // カメラの位置
uniform samplerCube cubeTexture; // キューブテクスチャユニット
uniform bool        reflection;  // 反射を表現するかどうかのフラグ
varying vec3        vPosition;
varying vec3        vNormal;
void main(){
    // 頂点座標とカメラの位置から視線ベクトルを算出
    // eyePosition(カメラ位置)
    // カメラから頂点を見つめるベクトル
    vec3 eyeDirection = vPosition - eyePosition;

    // 反射ベクトルに用いる変数
    vec3 reflectVector = vNormal;

    // もし反射有効なら reflect で反射ベクトルを求める
    // 背景用のオブジェクトを置きたいときのため
    if(reflection){
        // 視線ベクトルと法線を使って反射を求める
        reflectVector = reflect(eyeDirection, vNormal);
    }
    // 反射ベクトルを使ってキューブマップテクスチャからサンプリング
    // vec3のtexture座標のものになる
    vec4 envColor = textureCube(cubeTexture, reflectVector);
    gl_FragColor = envColor;
}