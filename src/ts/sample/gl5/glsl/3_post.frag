// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// フレームバッファに描画された内容をテクスチャとして受け取り、
// それをそのままスクリーンに出すだけの単純なシェーダ。
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;
void main(){
    vec4 destColor = texture2D(textureUnit, vTexCoord);
    gl_FragColor = destColor;
}
