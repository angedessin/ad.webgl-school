// main.vert ------------------------------------------------------------------
// メインプログラム
// テクスチャ座標をフラグメントシェーダに渡して、座標変換のみを行う
// ----------------------------------------------------------------------------
attribute vec3 position;
attribute vec2 texCoord;
uniform mat4 mvpMatrix;
varying vec2 vTexCoord;
void main(){
    vTexCoord = texCoord;
    gl_Position = mvpMatrix * vec4(position, 1.0);
}

