attribute vec3 position;
attribute vec3 normal;
uniform mat4 mMatrix;
uniform mat4 mvpMatrix;
uniform mat4 nMatrix;
varying vec3 vPosition;
varying vec3 vNormal;
void main(){
    vPosition = (mMatrix * vec4(position, 1.0)).xyz;
    vNormal = (nMatrix * vec4(normal, 0.0)).xyz;
    gl_Position = mvpMatrix * vec4(position, 1.0);
}