precision mediump float;
uniform vec3        eyePosition; // カメラの位置
uniform samplerCube cubeTexture; // キューブテクスチャユニット
uniform bool        refraction;  // 屈折を表現するかどうかのフラグ
uniform float       eta;         // 屈折率 @@@
varying vec3        vPosition;
varying vec3        vNormal;
void main(){
    // 頂点座標とカメラの位置から視線ベクトルを算出（正規化する） @@@
    vec3 eyeDirection = normalize(vPosition - eyePosition);
    // 屈折ベクトルに用いる変数
    vec3 refractVector = vNormal;
    // もし屈折有効なら refract で屈折ベクトルを求める @@@
    if(refraction){
        refractVector = refract(eyeDirection, vNormal, eta);
    }
    // 屈折ベクトルを使ってキューブマップテクスチャからサンプリング
    vec4 envColor = textureCube(cubeTexture, refractVector);
    gl_FragColor = envColor;
}