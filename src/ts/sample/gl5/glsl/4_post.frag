// post.frag ------------------------------------------------------------------
// ポストプロセスのフラグメントシェーダ。
// ネガポジ反転シェーダは、出力される RGB 値を反転させてから出力。
// シェーダ内でフレームバッファの描画結果をサンプリングしてRGB 値を取り出し、
// これを反転させたあと gl_FragColor に出力
// ----------------------------------------------------------------------------
precision mediump float;
uniform sampler2D textureUnit;
varying vec2 vTexCoord;
void main(){

    // R 0 ~ 1
    // nR 1.0 - R
    // フレームバッファに焼いたシーンの色
    vec4 dest = texture2D(textureUnit, vTexCoord);
    vec4 R = texture2D(textureUnit, vTexCoord + vec2(0.1, 0.0));
    vec4 B = texture2D(textureUnit, vTexCoord - vec2(0.1, 0.0));

    // 1.0から引くことで、ネガポジ反転をしている(RGB)
    gl_FragColor = vec4(dest.rgb - vec3(R.r, 1.0, B.b), 1.0);
}
