import * as THREE from "three";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";
import KeyboardEvent from "../../libs/events/KeyboardEvent";
import MouseEvent from "../../libs/events/MouseEvent";
import Core from "../../libs/utils/Core";

interface ICameraParameter {
    fovy: number;
    aspect: number;
    near: number;
    far: number;
    x: number;
    y: number;
    z: number;
    lookAt: THREE.Vector3;
}

interface IRendererParameter {
    clearColor: number;
    width: number;
    height: number;
}

interface IMaterialParameter {
    color: number;
    specular: number;
}

export default class Sample9 extends BaseSample {
    private _w: number = Core.getInnerW();
    private _h: number = Core.getInnerH();
    private _world: HTMLElement = Dom.getElement("world");

    private CAMERA_PARAMETER: ICameraParameter = {
        fovy   : 60,
        aspect : this._w / this._h,
        near   : 0.1,
        far    : 10.0,
        x      : 0.0,
        y      : 2.0,
        z      : 5.0,
        lookAt : new THREE.Vector3(0.0, 0.0, 0.0)
    };

    private RENDERER_PARAMETER: IRendererParameter = {
        clearColor : 0x333333,
        width      : this._w,
        height     : this._h
    };

    private MATERIAL_PARAMETER: IMaterialParameter = {
        color    : 0xff9933,
        specular : 0xffffff
    };


    // three
    private _scene: THREE.Scene = new THREE.Scene();
    private _camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
        this.CAMERA_PARAMETER.fovy,
        this.CAMERA_PARAMETER.aspect,
        this.CAMERA_PARAMETER.near,
        this.CAMERA_PARAMETER.far
    );

    private _renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();
    private _geometry: THREE.BoxGeometry = new THREE.BoxGeometry(1.0, 1.0, 1.0);

    // three.js の組み込みのマテリアルの中から、フォンシェーディング用のもの
    private _material: THREE.MeshPhongMaterial = new THREE.MeshPhongMaterial(this.MATERIAL_PARAMETER);
    private _box: THREE.Mesh = new THREE.Mesh(this._geometry, this._material);

    private _directional: THREE.DirectionalLight = new THREE.DirectionalLight(0xffffff);
    private _ambient: THREE.AmbientLight = new THREE.AmbientLight(0xffffff, 0.2);

    // private _controls = new THREE.OrbitControls(this._camera, this._renderer.domElement);


    constructor() {
        super();
        this._onMouseMove = this._onMouseMove.bind(this);

        window.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove);

        window.addEventListener(KeyboardEvent.KEY_DOWN, (e: any): void => {
            const isRun: boolean = e.keyCode !== 27;
            if (this._isRun === isRun) {
                return;
            } else {
                this._isRun = isRun;
            }
            if (this._isRun) {
                this._render();
            }
        });
        this._initCamera();
        this._initRenderer();
        this._initLight();
        this._initScene();
        this._render();
    }

    /**
     * マウスが動いたときの処理
     * @param e
     * @private
     */
    private _onMouseMove(e): void {
        const w: number = Core.getInnerW();
        const h: number = Core.getInnerH();
        // 原点をスクリーンの中心に移動する
        let x: number = e.clientX * 2.0 - w;
        let y: number = e.clientY * 2.0 - h;

        // 大きさ(長さ)
        const length: number = Math.sqrt(x * x + y * y);

        // 単位化する(絶対1になる)
        x /= length;
        y /= length;

        this._box.position.x = x;
        this._box.position.y = -y;

    }

    private _initCamera(): void {
        const {x, y, z, lookAt}:ICameraParameter = this.CAMERA_PARAMETER;
        this._camera.position.x = x;
        this._camera.position.y = y;
        this._camera.position.z = z;
        this._camera.lookAt(lookAt);
    }

    private _initRenderer(): void {
        const {width, height}:IRendererParameter = this.RENDERER_PARAMETER;
        this._renderer.setClearColor(new THREE.Color(this.RENDERER_PARAMETER.clearColor));
        this._renderer.setSize(width, height);
        this._world.appendChild(this._renderer.domElement);
    }

    private _initScene(): void {
        this._scene.add(this._box);
        this._renderer.render(this._scene, this._camera);
    }

    private _initLight(): void {
        this._directional.position.set(1.0, 1.0, 1.0);
        this._scene.add(this._directional);
        this._scene.add(this._ambient);
    }

    protected _render(): void {
        super._render();
        this._box.rotation.y += 0.01;


        this._renderer.render(this._scene, this._camera);
        if (this._isRun) {
            requestAnimationFrame(this._render);
        }
    }

}