import * as THREE from "three";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";
import KeyboardEvent from "../../libs/events/KeyboardEvent";
// import MouseEvent from "../../libs/events/MouseEvent";
import Core from "../../libs/utils/Core";


interface ICameraParameter {
    fovy: number;
    aspect: number;
    near: number;
    far: number;
    x: number;
    y: number;
    z: number;
    lookAt: THREE.Vector3;
}

interface IRendererParameter {
    clearColor: number;
    width: number;
    height: number;
}

interface IMaterialParameter {
    color?: number;
    specular?: number;
    size?: number;
}

export default class Sample11 extends BaseSample {
    private _w: number = Core.getInnerW();
    private _h: number = Core.getInnerH();
    private _world: HTMLElement = Dom.getElement("world");

    private CAMERA_PARAMETER: ICameraParameter = {
        fovy   : 60,
        aspect : this._w / this._h,
        near   : 0.1,
        far    : 10.0,
        x      : 0.0,
        y      : 2.0,
        z      : 5.0,
        lookAt : new THREE.Vector3(0.0, 0.0, 0.0)
    };

    private RENDERER_PARAMETER: IRendererParameter = {
        clearColor : 0x333333,
        width      : this._w,
        height     : this._h
    };

    private MATERIAL_PARAMETER: IMaterialParameter = {
        color    : 0xff9933,
        specular : 0xffffff
    };

    private MATERIAL_PARAMETER_POINT: IMaterialParameter = {
        color : 0xff9933,
        size  : 0.1
    };


    // three
    private _scene: THREE.Scene = new THREE.Scene();
    private _camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
        this.CAMERA_PARAMETER.fovy,
        this.CAMERA_PARAMETER.aspect,
        this.CAMERA_PARAMETER.near,
        this.CAMERA_PARAMETER.far
    );

    private _renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();

    // ジオメトリとはあくまでも「形」の定義であり、いわばそれは骨格のようなもの
    // これをどのような外見を持つオブジェクトにするのかは、
    // ジオメトリをどのようにメッシュなどと紐付けるかによって様々に変化する
    private _geometry: THREE.BoxGeometry = new THREE.BoxGeometry(1.0, 1.0, 1.0);

    private _material: any = new THREE.MeshPhongMaterial(this.MATERIAL_PARAMETER);

    // point material
    private _materialPoint: THREE.PointsMaterial = new THREE.PointsMaterial(this.MATERIAL_PARAMETER_POINT);

    // 面を持ったっポリゴン
    private _boxSurface: THREE.Mesh = new THREE.Mesh(this._geometry, this._material);

    // 線をも持ったポリゴン
    private _boxLine: THREE.Line = new THREE.Line(this._geometry, this._material);

    // ポイントを持ったポリゴン。傾きを見せたくない場合使う
    private _boxPoints: THREE.Points = new THREE.Points(this._geometry, this._materialPoint);


    private _directional: THREE.DirectionalLight = new THREE.DirectionalLight(0xffffff);
    private _ambient: THREE.AmbientLight = new THREE.AmbientLight(0xffffff, 0.2);

    private _count: number = 0;


    constructor() {
        super();
        window.addEventListener(KeyboardEvent.KEY_DOWN, (e: any): void => {
            const isRun: boolean = e.keyCode !== 27;
            if (this._isRun === isRun) {
                return;
            } else {
                this._isRun = isRun;
            }
            if (this._isRun) {
                this._render();
            }
        });
        this._initCamera();
        this._initRenderer();
        this._initLight();
        this._initScene();
        this._render();
    }


    private _initCamera(): void {
        const {x, y, z, lookAt}:ICameraParameter = this.CAMERA_PARAMETER;
        this._camera.position.x = x;
        this._camera.position.y = y;
        this._camera.position.z = z;
        this._camera.lookAt(lookAt);
    }

    private _initRenderer(): void {
        const {width, height}:IRendererParameter = this.RENDERER_PARAMETER;
        this._renderer.setClearColor(new THREE.Color(this.RENDERER_PARAMETER.clearColor));
        this._renderer.setSize(width, height);
        this._world.appendChild(this._renderer.domElement);
    }

    private _initScene(): void {
        this._scene.add(this._boxSurface);
        this._scene.add(this._boxLine);
        this._scene.add(this._boxPoints);
        this._renderer.render(this._scene, this._camera);
    }

    private _initLight(): void {
        this._directional.position.set(1.0, 1.0, 1.0);
        this._scene.add(this._directional);
        this._scene.add(this._ambient);
    }

    protected _render(): void {
        super._render();
        this._renderer.render(this._scene, this._camera);
        this._count += 1;
        let s: number = Math.sin(this._count * 0.05) * 2.0;
        let c: number = Math.cos(this._count * 0.05) * 2.0;

        this._boxSurface.position.x = c;
        this._boxSurface.position.z = s;
        this._boxSurface.rotation.x += 0.01;
        this._boxSurface.rotation.y += 0.01;

        const rad: number = Math.PI * 2.0 / 3.0;
        s = Math.sin(rad + this._count * 0.05) * 2.0;
        c = Math.cos(rad + this._count * 0.05) * 2.0;
        this._boxLine.position.x = c;
        this._boxLine.position.z = s;
        this._boxLine.rotation.x += 0.01;
        this._boxLine.rotation.y += 0.01;

        s = Math.sin(rad + rad + this._count * 0.05) * 2.0;
        c = Math.cos(rad + rad + this._count * 0.05) * 2.0;
        this._boxPoints.position.x = c;
        this._boxPoints.position.z = s;
        this._boxPoints.rotation.x += 0.01;
        this._boxPoints.rotation.y += 0.01;

        if (this._isRun) {
            requestAnimationFrame(this._render);
        }
    }

}