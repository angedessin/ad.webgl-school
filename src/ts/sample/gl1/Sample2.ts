import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/2.vert");
const fs: string = require("./glsl/2.frag");
declare const gl3: any;

export default class Sample2 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _VBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            console.log("initialize error");
            return;
        }

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            [],
            []
        );
    }

    private _initBuffers(): void {
        // 頂点に新たな属性を持たせる
        // 頂点とは、3D シーンを構成する最も基本的な単位
        // 頂点がひとつも無い場合、スクリーンにはオブジェクトが映し出されることない
        // 頂点に任意の色を割当てたいのなら、頂点自身が「色という属性」を持っている状態すればいい
        // VBO として頂点の色データを用意してやることによって、シェーダへと色に関する情報を送り込むことができる
        this._position = [
            0.0, 0.5, 0.0, // ひとつ目の頂点の x, y, z 座標
            0.5, -0.5, 0.0, // ふたつ目の頂点の x, y, z 座標
            -0.5, -0.5, 0.0  // みっつ目の頂点の x, y, z 座標
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0, // ひとつ目の頂点の R, G, B, A カラー
            0.0, 1.0, 0.0, 1.0, // ふたつ目の頂点の R, G, B, A カラー
            0.0, 0.0, 1.0, 1.0  // みっつ目の頂点の R, G, B, A カラー
        ];

        // 座標データから頂点バッファを生成
        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);
        this._prg.set_program();
        this._prg.set_attribute(this._VBO);
        gl3.draw_arrays(gl3.gl.TRIANGLES, 3);
    }
}