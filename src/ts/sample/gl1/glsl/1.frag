// 全てのピクセルの色を決める
// 頂点が描かれることになったピクセルの全ての色をピクセルレベルで決める
// 背景の色の設定はできない
precision mediump float;

void main() {
    // gl_FragColor これが最終的に画面に出る色 RGBA (0.0 ~ 1.0)
    gl_FragColor = vec4(1.0);
}