// 頂点シェーダは、頂点の個数回分呼び出される
// attributeなどは修飾子
// vector3 (x, y, z), attibute == vbo
attribute vec3 position;


// main関数を必ず記述する
// 頂点シェーダの場合は、必ずgl_Positionになにかしら出力しなければならない
void main() {
    // gl プレフィックスが付いているのはGLSLの組み込み変数
    // gl_Position 座標変化済みの頂点の座標
    gl_Position = vec4(position, 1.0);
}