import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/3.vert");
const fs: string = require("./glsl/3.frag");
declare const gl3: any;

export default class Sample3 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _VBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            console.log("initialize error");
            return;
        }

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }


    private _initProgram(): void {

        // uniform 変数定義の紐付け
        // uniform 変数の場合はその名前とデータ型を指定する必要がある
        // データ型は f が float を、v が vector を表しており、vec2 のデータを
        // シェーダに送りたいのであれば 2fv というようにデータ型に応じて指定する
        // （WebGL API の仕様に準拠）
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["globalColor"],
            ["4fv"]
        );
    }

    private _initBuffers(): void {
        this._position = [
            0.0, 0.5, 0.0, // ひとつ目の頂点の x, y, z 座標
            0.5, -0.5, 0.0, // ふたつ目の頂点の x, y, z 座標
            -0.5, -0.5, 0.0  // みっつ目の頂点の x, y, z 座標
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0, // ひとつ目の頂点の R, G, B, A カラー
            0.0, 1.0, 0.0, 1.0, // ふたつ目の頂点の R, G, B, A カラー
            0.0, 0.0, 1.0, 1.0  // みっつ目の頂点の R, G, B, A カラー
        ];

        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);
        this._prg.set_program();
        this._prg.set_attribute(this._VBO);
        this._prg.push_shader([[1.0, 0.2, 0.2, 1.0]]);
        gl3.draw_arrays(gl3.gl.TRIANGLES, 3);
    }
}

