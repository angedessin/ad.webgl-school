import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/1.vert");
const fs: string = require("./glsl/1.frag");
declare const gl3: any;

export default class Sample1 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _VBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            console.log("initialize error");
            return;
        }

        // キャンバスの初期サイズの設定
        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        //  WebGL に欠かせない概念に「シェーダ」
        // そして JavaScript のアプリケーション側からこのシェーダに
        // アクセスするために必要になるのが、プログラムオブジェクト。
        // 要はシェーダをひとつのプログラムとしてみなす
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position"],       // 頂点シェーダーの中に書かれている定義
            [3],                // ストライド(要素の数) vec3 === 3, vec2 === 2(1セットですよ)
            [],
            []
        );
    }

    private _initBuffers(): void {

        // 頂点データ（ジオメトリ）
        // three.js にはジオメトリという概念があり、しかも組み込みで
        // 最初から複数のジオメトリが用意されていた
        // WebGL にはそもそも組み込みのジオメトリなんてものは存在しない
        // 全部自分で頂点を定義して、組み上げていってやらなくてはならない
        // ここでは、単純な三角形のポリゴンを一枚だ定義
        this._position = [
            0.0, 0.5, 0.0, // ひとつ目の頂点の x, y, z 座標
            0.5, -0.5, 0.0, // ふたつ目の頂点の x, y, z 座標
            -0.5, -0.5, 0.0  // みっつ目の頂点の x, y, z 座標
        ];

        // Vertex Buffer Object
        // VBO は GPU 上のメモリ領域に作られるオブジェクトで、
        // WebGL ではこの VBO という概念に座標などの頂点データを流し込むことで、
        // GPU が参照しやすい領域に頂点の情報を置いておくことができる
        this._VBO = [
            gl3.create_vbo(this._position)
        ]
    }

    protected _render(): void {
        super._render();

        // ビューポートの設定とクリア
        // ビューポートの大きさはいつでも任意に変更できる
        // 特に理由がない限りは Canvas Element と同じ大きさにするべき
        // そうすることで、ドットバイドットで、正しい解像度で描画が行われる
        // また、このビューポートは明示的にリセットされるべき
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        // シーンのクリア(RGBA)
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);

        // 利用するプログラム（シェーダ）を指定
        // シェーダとは GPU 上で動作する一種のプログラムですので、
        // 実際になにかしらの描画を行う前に、
        // まずはどのシェーダを使うのかを明示的に示してやる必要がある
        this._prg.set_program();


        // プログラムをセット
        this._prg.set_attribute(this._VBO);

        // ドローコール
        // 既に GPU 上には描くべき頂点の情報や、それを描くシェーダの情報が全て揃ったことになる
        // これらの全ての準備が整ったら、やっと「絵を描け！」と命令することができる
        // ドローコールとは、draw call であり、これが描画命令に相当
        gl3.draw_arrays(gl3.gl.TRIANGLES, 3);
    }

}