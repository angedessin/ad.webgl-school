import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/4.vert");
const fs: string = require("./glsl/4.frag");
declare const gl3: any;

export default class Sample4 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _VBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            console.log("initialize error");
            return;
        }

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["globalColor"],
            ["4fv"]
        );
    }

    private _initBuffers(): void {
        const pos: number = 0.65;
        this._position = [
            -pos, 0.0, 0.0,
            0.0, pos, 0.0,
            pos, 0.0, 0.0,

            pos, 0.0, 0.0,
            1 - pos, -pos, 0.0,
            -pos, 0.0, 0.0,

            -pos, 0.0, 0.0,
            1 - pos, -pos, 0.0,
            -(1 - pos), -pos, 0.0

        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 0.0, 1.0, 1.0,
            0.0, 0.0, 1.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 0.0, 1.0, 1.0
        ];


        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);
        this._prg.set_program();
        this._prg.set_attribute(this._VBO);
        this._prg.push_shader([[1.0, 1.0, 1.0, 1.0]]);


        // - プリミティブタイプ -------------------------------------------
        // WebGL には代表的なプリミティブタイプには以下のようなものがある。
        // * gl.POINTS
        // * gl.LINES
        // * gl.LINE_STRIP
        // * gl.TRIANGLES
        // * gl.TRIANGLE_STRIP
        //
        // また、ドローコールを行う glcubic.js のメソッドの、第二引数をみると、
        // 「3」という数値が書いてある
        // これは「何個の頂点を描くか」ということを表す数値
        // 三角形がひとつなら、頂点は三個あればいいので「3」だが、
        // 五角形を描画するとなれば、この部分についても変更を行う必要がある
        // ----------------------------------------------------------------
        gl3.draw_arrays(gl3.gl.TRIANGLES, this._position.length / 3);
    }
}

