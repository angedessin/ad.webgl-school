attribute vec3 position; // 頂点座標
attribute vec3 normal;   // 法線
attribute vec4 color;    // 頂点カラー
uniform mat4 mvpMatrix;
varying vec4 vColor;

void main(){
    // ここでは仮に、そのまま法線の色を乗算して出力
    // RGBA === XYZW(数字の塊)
    vColor = color * vec4(normal, 1.0);
    gl_Position = mvpMatrix * vec4(position, 1.0);
}

