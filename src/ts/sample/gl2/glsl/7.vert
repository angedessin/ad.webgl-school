attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;
uniform mat4 mvpMatrix;
varying vec4 vColor;

// ライト（光線）の向き
// 正規化するのは、向きだけに注目したいため
const vec3 lightDirection = normalize(vec3(1.0, 1.0, 1.0));

void main(){
    // 拡散光の影響度を内積を使って算出
    // 負の数値になる場合があるので、maxで最低値を0にする
    // 色は-1でバクにならないが作法として包む必要がある
    // 0.0 ~ 1.0
    float diffuse = max(
        // 同じ向きがであればあるほど1.0に近い
        // color * シンクロ率
        dot(normal, lightDirection),
        0.0
    );

    // 拡散光の係数を頂点カラーに乗算
    vColor = color * vec4(vec3(diffuse), 1.0);

    // ベクトルの一致している場合まま色を出る
    // シンクロ率が高ければ明るくなる
    gl_Position = mvpMatrix * vec4(position, 1.0);
}

