import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/5.vert");
const fs: string = require("./glsl/5.frag");
declare const gl3: any;


interface ICircle {
    p: Array<number>;
    c: Array<number>;
    i: Array<number>;
    n: Array<number>;
}

interface IPillar {
    p: Array<number>;
    c: Array<number>;
    i: Array<number>;
    n: Array<number>;
}

interface IVector3 {
    x: number;
    y: number;
    z: number;
}

export default class Sample5 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _normal: Array<number>;
    private _index: Array<number>;
    private _VBO: Array<WebGLBuffer>;
    private _IBO: WebGLBuffer;

    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    private _lightDirection: Array<number> = [1.0, 1.0, 1.0];

    constructor() {
        super();
        this._render = this._render.bind(this);
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._initCamera();
        this._initScene();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "normal", "color"],
            [3, 3, 4],
            ["mvpMatrix", "lightDirection"],
            ["matrix4fv", "3fv"]
        );
    }


    private _initBuffers(): void {
        const {p, n, i, c}:IPillar = this._getPillarParams(6, 1.0, 0.5, [1.0, 1.0, 0.0, 0.0]);
        // const {p, i, c, n}:ICircle = this._getCircleParams(6, 0.0, [1.0, 1.0, 0.0, 0.0]);
        this._position = p;
        this._color = c;
        this._normal = n;
        this._index = i;
        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._normal),
            gl3.create_vbo(this._color)
        ];
        this._IBO = gl3.create_ibo(this._index);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 3.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.BACK);
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);

        this._prg.set_program();
        this._prg.set_attribute(this._VBO, this._IBO);

        this._nowTime = (Date.now() - this._startTime) / 1000;
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [1.0, 1.0, 0.0], this._mMatrix);
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);


        this._prg.push_shader([this._mvpMatrix, this._lightDirection]);
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);

        this._gl.flush();

        requestAnimationFrame(this._render);
    }


    _getPillarParams(length: number, h: number, size: number, color: Array<number> = [0.0, 0.0, 1.0, 1.0]): IPillar {
        const pos: Array<number> = [];
        const col: Array<number> = [];
        const idx: Array<number> = [];
        const nor: Array<number> = [];
        let currentIndex: number = 0;

        for (let i = 0; i < length; i++) {
            const theta: number = 360 / length;
            const halfH: number = h * 0.5;
            const radian: number = theta * Math.PI / 180;
            const x: number = Math.cos(radian * i) * size;
            const z: number = Math.sin(radian * i) * size;
            const vector1: IVector3 = this._getUnitVector(x, halfH, z);
            const vector2: IVector3 = this._getUnitVector(x, -halfH, z);

            // 頂点
            pos.push(
                x, halfH, z,
                x, -halfH, z
            );

            // カラー
            col.push(
                color[0], color[1], color[2], color[3],
                color[0], color[1], color[2], color[3]
            );

            // 法線
            nor.push(
                vector1.x, vector1.y, vector1.z,
                vector2.x, vector2.y, vector2.z
            );

            // index
            if (i === length - 1) {
                idx.push(
                    // 表面
                    currentIndex, 0, currentIndex + 1,
                    currentIndex + 1, 0, 1,

                    // 裏面
                    currentIndex, currentIndex + 1, 0,
                    0, currentIndex + 1, 1
                );
            } else {
                idx.push(
                    // 表側から見たとき
                    currentIndex, currentIndex + 2, currentIndex + 1,
                    currentIndex + 1, currentIndex + 2, currentIndex + 3,

                    // 裏側から見たとき
                    currentIndex, currentIndex + 1, currentIndex + 2,
                    currentIndex + 2, currentIndex + 1, currentIndex + 3
                );
            }
            currentIndex += 2;
        }

        return {p : pos, c : col, n : nor, i : idx};
    }

    _getCircleParams(length: number, depth: number = 0.0, color: Array<number> = [1.0, 0.0, 0.0, 1.0]): ICircle {
        const pos: Array<number> = [0.0, 0.0, 0.0];
        const col: Array<number> = [color[0], color[1], color[2], color[3]];
        const nor: Array<number> = [0.0, 0.0, 0.0, 0.0];
        const idx: Array<number> = [];

        for (let i = 0; i < length; i++) {
            const radian = Math.PI / 180;
            const theta = 360 / length;
            const x = Math.cos(radian * theta * i);
            const y = Math.sin(radian * theta * i);
            const vector: IVector3 = this._getUnitVector(x, y, depth);
            // color
            pos.push(x, y, depth);
            col.push(color[0], color[1], color[2], color[3]);
            nor.push(vector.x, vector.y, vector.z);

            if (i !== length - 1) {
                idx.push(
                    0, i + 2, i + 1,
                    0, i + 1, i + 2
                );
            } else {
                idx.push(
                    0, 1, i + 1,
                    0, i + 1, 1
                );
            }
        }


        return {p : pos, c : col, n : nor, i : idx};
    }

    private _getUnitVector(x: number, y: number, z: number): IVector3 {
        const size: number = Math.sqrt(x * x + y * y + z * z);
        return {
            x : x / size,
            y : y / size,
            z : z / size
        };
    }
}

