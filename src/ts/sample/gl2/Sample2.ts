import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/2.vert");
const fs: string = require("./glsl/2.frag");
declare const gl3: any;

export default class Sample4 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _index: Array<number>;
    private _VBO;
    private _IBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["globalColor"],
            ["4fv"]
        );
    }

    private _initBuffers(): void {
        // ふたつの三角形ポリゴンを定義
        // WebGL では、奥行きに関する情報を扱う Z 値の扱いが「右手座標系」
        // 親指を X の正方向、人差し指を Y の正方向に向けたとき、
        // 中指がどちらに向くかが、Z の正方向を表します。DirectX は左手系
        this._position = [
            -0.5, 0.5, 0.5,     // ひとつ目の三角形の第一頂点
            0.5, 0.0, 0.5,      // ひとつ目の三角形の第二頂点
            -0.5, -0.5, 0.5,    // ひとつ目の三角形の第三頂点
            0.5, 0.5, -0.5,     // ふたつ目の三角形の第一頂点
            -0.5, 0.0, -0.5,    // ふたつ目の三角形の第二頂点
            0.5, -0.5, -0.5     // ふたつ目の三角形の第三頂点
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0, // ひとつ目の三角形は赤に
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0  // ふたつ目の三角形は緑に
        ];

        this._index = [
            0, 2, 1,    // ひとつ目の三角形に使うインデックス
            3, 5, 4     // ふたつ目の三角形に使うインデックス
        ];

        // 座標データから頂点バッファを生成
        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];

        // インデックス配列からインデックスバッファを生成
        this._IBO = gl3.create_ibo(this._index);
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);
        this._prg.set_program();
        this._prg.set_attribute(this._VBO, this._IBO);
        this._prg.push_shader([[1.0, 1.0, 1.0, 1.0]]);
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
    }
}

