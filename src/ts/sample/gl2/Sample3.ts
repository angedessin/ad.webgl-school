import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/3.vert");
const fs: string = require("./glsl/3.frag");
declare const gl3: any;

export default class Sample4 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _index: Array<number>;
    private _VBO: Array<WebGLBuffer>;
    private _IBO: WebGLBuffer;

    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    constructor() {
        super();

        this._render = this._render.bind(this);
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._initCamera();
        this._initScene();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["mvpMatrix", "globalColor"],
            ["matrix4fv", "4fv"]
        );
    }

    private _initBuffers(): void {
        // ふたつの三角形ポリゴンを定義
        // WebGL では、奥行きに関する情報を扱う Z 値の扱いが「右手座標系」
        // 親指を X の正方向、人差し指を Y の正方向に向けたとき、
        // 中指がどちらに向くかが、Z の正方向を表します。DirectX は左手系
        this._position = [
            // WebGLは右手系なので、手前に来ればくるほどプラス
            // DirectXは左手系
            -0.5, 0.5, 0.5,     // ひとつ目の三角形の第一頂点
            0.5, 0.0, 0.5,      // ひとつ目の三角形の第二頂点
            -0.5, -0.5, 0.5,    // ひとつ目の三角形の第三頂点
            0.5, 0.5, -0.5,     // ふたつ目の三角形の第一頂点
            -0.5, 0.0, -0.5,    // ふたつ目の三角形の第二頂点
            0.5, -0.5, -0.5     // ふたつ目の三角形の第三頂点
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0, // ひとつ目の三角形は赤に
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0  // ふたつ目の三角形は緑に
        ];

        this._index = [
            0, 2, 1,    // ひとつ目の三角形に使うインデックス
            3, 5, 4     // ふたつ目の三角形に使うインデックス
        ];


        // 座標データから頂点バッファを生成
        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];

        // インデックス配列からインデックスバッファを生成
        this._IBO = gl3.create_ibo(this._index);
    }

    private _initCamera(): void {
        gl3.mat4.lookAt([0.0, 0.0, 3.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0], this._vMatrix);
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        // 描画のための設定を行う
        // 深度テストもカリングも、既定値は「無効の状態」
        // 意図的に有効化を行ってやる必要があり、これには gl.enable を使う
        // 反対に、無効化させたい場合には gl.disable を使う
        // カリングは、ポリゴンの面の向きを元に特定の向きの面を描画
        // これにより負荷の軽減などの様々な効果を得られる
        // 一般に、カリングするのは裏面であることが多いが、
        // 表面をカリング面を設定できる。
        this._gl.enable(this._gl.DEPTH_TEST);   // 深度テストを有効化
        this._gl.enable(this._gl.CULL_FACE);    // カリングを有効化(既定値は無効)
        this._gl.enable(this._gl.BACK);         // カリング面の設定
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);

        this._prg.set_program();
        this._prg.set_attribute(this._VBO, this._IBO);

        this._nowTime = (Date.now() - this._startTime) / 1000;
        gl3.mat4.identity(this._mMatrix);
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [0.0, 1.0, 0.0], this._mMatrix);
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        this._prg.push_shader([this._mvpMatrix, [1.0, 1.0, 1.0, 1.0]]);
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);

        requestAnimationFrame(this._render);
    }
}

