import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/4.vert");
const fs: string = require("./glsl/4.frag");
declare const gl3: any;

export default class Sample4 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _gl: WebGLRenderingContext;
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _index: Array<number>;
    private _VBO: Array<WebGLBuffer>;
    private _IBO: WebGLBuffer;


    // - 行列の種類 -------------------------------------------------------
    // WebGL には、API として行列処理をサポートする機能はない
    // JavaScript の行列処理は、自分でなんとかする必要がある
    // glcubic.js に行列処理用の一連のヘルパー系メソッドがある
    // 行列の中身は実態としては 単なる長さ 16 の配列
    // --------------------------------------------------------------------

    // . mMatrix - モデル座標変換行列 .....................................
    // モデル座標変換とは、別名「ワールド座標変換」とも呼ばれる
    // この座標変換は、頂点を三次元空間内で移動させたり、回転させたりする効果がある
    // ....................................................................

    // . vMatrix - ビュー座標変換行列 .....................................
    // ビュー座標変換は、その名の示すとおりビュー、
    // つまりカメラに映るものについての影響を頂点に与える効果がある。
    // より具体的に言うと、カメラがどこにあるのか、
    // あるいはどこを向いているのかなどを考慮して、
    // 頂点に適切に処理を加えてくれる変換です。
    // ....................................................................

    // . pMatrix - プロジェクション座標変換行列 ...........................
    // プロジェクション座標変換は、いわゆるプロジェクターのように
    // 「投影」に関する変換を行う座標変換
    // 三次元空間のどこからどこまでの範囲を、スクリーンに出すかを決める役割を持つ
    // ....................................................................
    private _mMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _vMatrix = gl3.mat4.identity(gl3.mat4.create());
    private _pMatrix = gl3.mat4.identity(gl3.mat4.create());


    // v と p を掛け合わせたもの
    private _vpMatrix = gl3.mat4.identity(gl3.mat4.create());

    // mvp 全てを掛け合わせたもの
    private _mvpMatrix = gl3.mat4.identity(gl3.mat4.create());

    private _startTime: number = Date.now();
    private _nowTime: number = 0;

    constructor() {
        super();
        this._render = this._render.bind(this);
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._initCamera();
        this._initScene();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        this._gl = gl3.gl;

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["mvpMatrix", "globalColor"],
            ["matrix4fv", "4fv"]
        );
    }

    private _initBuffers(): void {
        this._position = [
            -0.5, 0.5, 0.5,
            0.5, 0.0, 0.5,
            -0.5, -0.5, 0.5,
            0.5, 0.5, -0.5,
            -0.5, 0.0, -0.5,
            0.5, -0.5, -0.5
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            1.0, 0.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0
        ];

        this._index = [
            0, 2, 1,
            3, 5, 4
        ];


        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];

        this._IBO = gl3.create_ibo(this._index);
    }

    private _initCamera(): void {
        // - ビュー座標変換行列 -----------------------------------------------
        // glcubic.js では、mat4.lookAt メソッドでビュー座標変換行列を生成できる。
        // 第一引数：カメラの座標
        // 第二引数：カメラの注視点（見つめる座標）
        // 第三引数：カメラの上方向（天井が向いている方向）
        // 戻り値はなく、第四引数に与えた行列が更新される
        // --------------------------------------------------------------------
        gl3.mat4.lookAt(
            [0.0, 0.0, 3.0],    // z軸の3下がったときにある
            [0.0, 0.0, 0.0],    // 原点のところを見ている
            [0.0, 1.0, 0.0],    // カメラの上方向
            this._vMatrix
        );

        // - プロジェクション座標変換行列 -------------------------------------
        // glcubic.js では、mat4.perspective メソッドでビュー座標変換行列を生成できる。
        // 第一引数：カメラの視野角（Field of View）
        // 第二引数：視錐台の縦横比（幅 / 高さ）
        // 第三引数：ニアクリップ面（視錐台のカメラに近い面）
        // 第四引数：ファークリップ面（視錐台のカメラから遠い面）
        // 戻り値はなく、第五引数に与えた行列が更新される
        // --------------------------------------------------------------------
        // windowサイズが変わった場合はカメラの処理も変える必要がある
        // アスペクト比は幅/高さ
        gl3.mat4.perspective(45, 1.0, 0.1, 6.0, this._pMatrix);

        // 行列を掛け合わせる
        gl3.mat4.multiply(this._pMatrix, this._vMatrix, this._vpMatrix);
    }

    private _initScene(): void {
        // 深度テストとカリングを有効化する
        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.enable(this._gl.BACK);
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);

        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);

        this._prg.set_program();
        this._prg.set_attribute(this._VBO, this._IBO);

        // 時間の経過を得る（Date.now は現在時刻のタイムスタンプをミリ秒で返す）
        this._nowTime = (Date.now() - this._startTime) / 1000;

        // モデル座標変換用の変数をいったんリセット（単位行列に戻す）
        gl3.mat4.identity(this._mMatrix);

        // . モデル座標変換で回転を加える .................................
        // 頂点に回転を加えるモデル座標変換は、mat4.rotateで行える
        // 第一引数：処理のベースとなる行列変数
        // 第二引数：回転する量（ラジアン単位）
        // 第三引数：回転する際の軸を表すベクトル
        // 戻り値はなく、第四引数に与えられた行列が更新される
        // ................................................................
        // 別々に動かしたい場合はそれぞれに対して行列を作る
        gl3.mat4.rotate(this._mMatrix, this._nowTime, [0.0, 1.0, 0.0], this._mMatrix);

        // 行列を掛け合わせる
        gl3.mat4.multiply(this._vpMatrix, this._mMatrix, this._mvpMatrix);

        // uniform 変数をシェーダにプッシュ
        this._prg.push_shader([this._mvpMatrix, [1.0, 1.0, 1.0, 1.0]]);

        // ドローコール（描画命令）
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);

        requestAnimationFrame(this._render);
    }
}

