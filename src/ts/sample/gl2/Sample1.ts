import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";

const vs: string = require("./glsl/1.vert");
const fs: string = require("./glsl/1.frag");
declare const gl3: any;

export default class Sample4 extends BaseSample {
    private _world: HTMLElement = Dom.getElement("world");
    private _prg;
    private _position: Array<number>;
    private _color: Array<number>;
    private _index: Array<number>;
    private _VBO;
    private _IBO;

    constructor() {
        super();
        this._setCanvas();
        this._initProgram();
        this._initBuffers();
        this._render();
    }


    private _setCanvas(): void {
        const doc: Document = document;
        const canvas: HTMLCanvasElement = doc.createElement("canvas");
        const df: DocumentFragment = doc.createDocumentFragment();
        df.appendChild(canvas);

        this._world.appendChild(df);
        canvas.id = "canvas";

        gl3.initGL("canvas");
        if (!gl3.ready) {
            return;
        }

        const canvasSize: number = Math.min(Core.getInnerW(), Core.getInnerH());
        gl3.canvas.width = canvasSize;
        gl3.canvas.height = canvasSize;
    }

    private _initProgram(): void {
        this._prg = gl3.program.create_from_source(
            vs, fs,
            ["position", "color"],
            [3, 4],
            ["globalColor"],
            ["4fv"]
        );
    }

    private _initBuffers(): void {
        this._position = [
            0.0, 0.5, 0.0,
            0.5, 0.1, 0.0,
            -0.5, 0.1, 0.0,
            0.3, -0.5, 0.0,
            -0.3, -0.5, 0.0
        ];

        this._color = [
            1.0, 0.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 0.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            0.0, 0.0, 0.0, 1.0
        ];


        // インデックスバッファ用の配列を作る
        // インデックスバッファとは、頂点に与えられた連番を用いて、
        // どのようにプリミティブを描画していくのかを指定するためのバッファ
        this._index = [
            0, 1, 2,
            2, 1, 3,
            2, 3, 4
        ];


        this._VBO = [
            gl3.create_vbo(this._position),
            gl3.create_vbo(this._color)
        ];


        // インデックスバッファを生成する
        // 頂点属性を格納する VBO と同じように、インデックスデータの配列は、
        // それ専用のバッファである IBO を生成して利用
        // glcubic.js にはインデックスバッファを生成するためのヘルパー関数
        // VBO と同じタイミングで IBO を生成しておく
        this._IBO = gl3.create_ibo(this._index);
    }

    protected _render(): void {
        super._render();
        gl3.scene_view(null, 0, 0, gl3.canvas.width, gl3.canvas.height);
        gl3.scene_clear([0.7, 0.7, 0.7, 1.0]);
        this._prg.set_program();


        // インデックスバッファは、VBO と同じように GPU 上のメモリ空間に置かれている
        // lcubic.js の場合は、program.set_attribute をコールする際に、
        // 引数にVBO と IBO を同時に渡すことができるようになっている
        this._prg.set_attribute(this._VBO, this._IBO);
        this._prg.push_shader([[1.0, 1.0, 1.0, 1.0]]);


        // IBO を使って描画を行う際には、なんとドローコールも別のものを使う
        // 一般に、IBO を利用したほうが GPU 内部での処理がスムーズに行われ、描画効率が良い
        gl3.draw_elements(gl3.gl.TRIANGLES, this._index.length);
    }
}

