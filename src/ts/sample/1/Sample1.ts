import * as THREE from "three";
import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";


interface ICameraParameter {
    fovy: number;
    aspect: number;
    near: number;
    far: number;
    x: number;
    y: number;
    z: number;
    lookAt: THREE.Vector3;
}

interface IRendererParameter {
    clearColor: number;
    width: number;
    height: number;
}

interface IMaterialParameter {
    color: number;
}

export default class Sample1 extends BaseSample {
    private _w: number = Core.getInnerW();
    private _h: number = Core.getInnerH();
    private _world: HTMLElement = Dom.getElement("world");

    // 各種パラメータを設定するために定数オブジェクトを定義
    private CAMERA_PARAMETER: ICameraParameter = {
        fovy   : 60,
        aspect : this._w / this._h,
        near   : 0.1,
        far    : 10.0,
        x      : 0.0,
        y      : 2.0,
        z      : 5.0,
        lookAt : new THREE.Vector3(0.0, 0.0, 0.0)
    };

    // レンダラに関するパラメータ
    private RENDERER_PARAMETER: IRendererParameter = {
        clearColor : 0x333333,
        width      : this._w,
        height     : this._h
    };

    // マテリアルに関するパラメータ
    private MATERIAL_PARAMETER: IMaterialParameter = {
        color : 0xff9933
    };

    // three.js 定義されているオブジェクトに関連した変数を宣言

    // シーン
    private _scene: THREE.Scene = new THREE.Scene();

    // カメラ
    private _camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
        this.CAMERA_PARAMETER.fovy,
        this.CAMERA_PARAMETER.aspect,
        this.CAMERA_PARAMETER.near,
        this.CAMERA_PARAMETER.far
    );

    // レンダラ
    private _renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();

    // ジオメトリ
    private _geometry: THREE.BoxGeometry = new THREE.BoxGeometry(1.0, 1.0, 1.0);

    // マテリアル
    private _material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial(this.MATERIAL_PARAMETER);

    // ボックスメッシュ
    private _box: THREE.Mesh = new THREE.Mesh(this._geometry, this._material);


    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        super();
        this._initCamera();
        this._initRenderer();
        this._initScene();
    }


    /**
     * カメラの初期設定
     * @private
     */
    private _initCamera(): void {
        const {x, y, z, lookAt}:ICameraParameter = this.CAMERA_PARAMETER;
        this._camera.position.x = x;
        this._camera.position.y = y;
        this._camera.position.z = z;
        this._camera.lookAt(lookAt);
    }

    /**
     * レンダラの初期設定
     * @private
     */
    private _initRenderer(): void {
        const {width, height}:IRendererParameter = this.RENDERER_PARAMETER;
        this._renderer.setClearColor(new THREE.Color(this.RENDERER_PARAMETER.clearColor));
        this._renderer.setSize(width, height);
        this._world.appendChild(this._renderer.domElement);
    }

    /**
     * シーンの初期設定
     * @private
     */
    private _initScene(): void {
        this._scene.add(this._box);
        this._renderer.render(this._scene, this._camera);
    }


}