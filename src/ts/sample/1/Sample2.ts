import * as THREE from "three";
import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import BaseSample from "../../common/BaseSample";
import KeyboardEvent from "../../libs/events/KeyboardEvent";


interface ICameraParameter {
    fovy: number;
    aspect: number;
    near: number;
    far: number;
    x: number;
    y: number;
    z: number;
    lookAt: THREE.Vector3;
}

interface IRendererParameter {
    clearColor: number;
    width: number;
    height: number;
}


interface IMaterialParameter {
    color: number;
}


export default class Sample2 extends BaseSample {
    private _w: number = Core.getInnerW();
    private _h: number = Core.getInnerH();
    private _world: HTMLElement = Dom.getElement("world");

    private CAMERA_PARAMETER: ICameraParameter = {
        fovy   : 60,
        aspect : this._w / this._h,
        near   : 0.1,
        far    : 10.0,
        x      : 0.0,
        y      : 2.0,
        z      : 5.0,
        lookAt : new THREE.Vector3(0.0, 0.0, 0.0)
    };

    private RENDERER_PARAMETER: IRendererParameter = {
        clearColor : 0x333333,
        width      : this._w,
        height     : this._h
    };

    private MATERIAL_PARAMETER: IMaterialParameter = {
        color : 0xff9933
    };


    // three
    private _scene: THREE.Scene = new THREE.Scene();
    private _camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
        this.CAMERA_PARAMETER.fovy,
        this.CAMERA_PARAMETER.aspect,
        this.CAMERA_PARAMETER.near,
        this.CAMERA_PARAMETER.far
    );

    private _renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();
    private _geometry: THREE.BoxGeometry = new THREE.BoxGeometry(1.0, 1.0, 1.0);
    private _material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial(this.MATERIAL_PARAMETER);
    private _box: THREE.Mesh = new THREE.Mesh(this._geometry, this._material);


    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        super();
        window.addEventListener(KeyboardEvent.KEY_DOWN, (e: any): void => {
            const isRun: boolean = e.keyCode !== 27;
            if (this._isRun === isRun) {
                return;
            } else {
                this._isRun = isRun;
            }
            if (this._isRun) {
                this._render();
            }
        });
        this._initCamera();
        this._initRenderer();
        this._initScene();
        this._render();
    }


    /**
     * カメラの初期設定
     * @private
     */
    private _initCamera(): void {
        const {x, y, z, lookAt}:ICameraParameter = this.CAMERA_PARAMETER;
        this._camera.position.x = x;
        this._camera.position.y = y;
        this._camera.position.z = z;
        this._camera.lookAt(lookAt);
    }


    /**
     * レンダラの初期設定
     * @private
     */
    private _initRenderer(): void {
        const {width, height}:IRendererParameter = this.RENDERER_PARAMETER;
        this._renderer.setClearColor(new THREE.Color(this.RENDERER_PARAMETER.clearColor));
        this._renderer.setSize(width, height);
        this._world.appendChild(this._renderer.domElement);
    }

    /**
     * シーンの初期設定
     * @private
     */
    private _initScene(): void {
        this._scene.add(this._box);
        this._renderer.render(this._scene, this._camera);
    }


    /**
     * レンダリング
     * @private
     */
    protected _render(): void {
        super._render();
        this._camera.rotation.y += 0.05;
        this._renderer.render(this._scene, this._camera);
        if (this._isRun) {
            // requestAnimationFrame アニメーション処理のための再帰呼び出し
            // ブラウザの更新頻度に応じた描画
            // 60fps = 1秒間に60回
            requestAnimationFrame(this._render);
        }
    }

}