import EventDispatcher from "../libs/events/EventDispatcher";
import Browser from "../libs/utils/Browser";
import Log from "../libs/utils/Log";


// 講義1
import Sample1 from "../sample/1/Sample1";
import Sample2 from "../sample/1/Sample2";
import Sample3 from "../sample/1/Sample3";
import Sample4 from "../sample/1/Sample4";
import Sample5 from "../sample/1/Sample5";
import Sample6 from "../sample/1/Sample6";
import Sample7 from "../sample/1/Sample7";

// 講義2
import Sample10 from "../sample/2/Sample10";
import Sample11 from "../sample/2/Sample11";
import Sample12 from "../sample/2/Sample12";
import Sample13 from "../sample/2/Sample13";
import Sample8 from "../sample/2/Sample8";
import Sample9 from "../sample/2/Sample9";

// 講義3
import Gl1 from "../sample/gl1/Sample1";
import Gl2 from "../sample/gl1/Sample2";
import Gl3 from "../sample/gl1/Sample3";
import Gl4 from "../sample/gl1/Sample4";

// 講義4
import Gl5 from "../sample/gl2/Sample1";
import Gl6 from "../sample/gl2/Sample2";
import Gl7 from "../sample/gl2/Sample3";
import Gl8 from "../sample/gl2/Sample4";
import Gl9 from "../sample/gl2/Sample5";
import Gl10 from "../sample/gl2/Sample6";
import Gl11 from "../sample/gl2/Sample7";

// 講義5
import Gl12 from "../sample/gl3/Sample1";
import Gl13 from "../sample/gl3/Sample2";
import Gl14 from "../sample/gl3/Sample3";
import Gl15 from "../sample/gl3/Sample4";
import Gl16 from "../sample/gl3/Sample5";
import Gl17 from "../sample/gl3/Sample6";
import Gl18 from "../sample/gl3/Sample7";
import Gl19 from "../sample/gl3/Sample8";
import Gl20 from "../sample/gl3/Sample9";
import Gl21 from "../sample/gl3/Sample10";
import Gl22 from "../sample/gl3/Sample11";

// 講義6
import Gl23 from "../sample/gl4/Sample1";
import Gl24 from "../sample/gl4/Sample2";
import Gl25 from "../sample/gl4/Sample3";
import Gl26 from "../sample/gl4/Sample4";
import Gl27 from "../sample/gl4/Sample5";
import Gl28 from "../sample/gl4/Sample6";
import Gl29 from "../sample/gl4/Sample7";

// 講義7
import Gl30 from "../sample/gl5/Sample1";
import Gl31 from "../sample/gl5/Sample2";
import Gl32 from "../sample/gl5/Sample3";
import Gl33 from "../sample/gl5/Sample4";

// 講義8
import Gl34 from "../sample/gl6/Sample1";
import Gl35 from "../sample/gl6/Sample2";
import Gl36 from "../sample/gl6/Sample3";
import Gl37 from "../sample/gl6/Sample4";
import Gl38 from "../sample/gl6/Sample5";
import Gl39 from "../sample/gl6/Sample6";
import Gl40 from "../sample/gl6/Sample7";
import Gl41 from "../sample/gl6/Sample8";

export default class ContentsManager extends EventDispatcher {
    private CLASS_NAME: string = "ContentsManager";
    private STUDY_1 = {
        1 : Sample1, 2 : Sample2, 3 : Sample3,
        4 : Sample4, 5 : Sample5, 6 : Sample6,
        7 : Sample7
    };

    private STUDY_2 = {
        1 : Sample8, 2 : Sample9, 3 : Sample10,
        4 : Sample11, 5 : Sample12, 6 : Sample13
    };

    private STUDY_3 = {
        1 : Gl1, 2 : Gl2, 3 : Gl3, 4 : Gl4
    };

    private STUDY_4 = {
        1 : Gl5, 2 : Gl6, 3 : Gl7, 4 : Gl8,
        5 : Gl9, 6 : Gl10, 7 : Gl11
    };

    private STUDY_5 = {
        1 : Gl12, 2 : Gl13, 3 : Gl14, 4 : Gl15,
        5 : Gl16, 6 : Gl17, 7 : Gl18, 8 : Gl19,
        9 : Gl20, 10 : Gl21, 11 : Gl22
    };

    private STUDY_6 = {
        1 : Gl23, 2 : Gl24, 3 : Gl25,
        4 : Gl26, 5 : Gl27, 6 : Gl28,
        7 : Gl29
    };

    private STUDY_7 = {
        1 : Gl30, 2 : Gl31, 3 : Gl32, 4 : Gl33
    };

    private STUDY_8 = {
        1 : Gl34, 2 : Gl35, 3 : Gl36, 4 : Gl37,
        5 : Gl38, 6 : Gl39, 7 : Gl40, 8 : Gl41
    };

    private STUDY = {
        1 : this.STUDY_1, 2 : this.STUDY_2, 3 : this.STUDY_3,
        4 : this.STUDY_4, 5 : this.STUDY_5, 6 : this.STUDY_6,
        7 : this.STUDY_7, 8 : this.STUDY_8
    };

    constructor() {
        super();
        this._selectGl();
    }


    private _selectGl(): void {
        const {number, index}: { number: number, index: number } = Browser.getUrlParams();
        Log.trace(this.CLASS_NAME, "_selectGl", `講義${number}, サンプル番号${index}`);
        if (number && index) {
            new this.STUDY[number][index];
        } else {
            Log.trace(this.CLASS_NAME, "_selectGl", `該当のファイルがない`);
        }

    }
}

