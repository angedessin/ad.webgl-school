import Dom from "./libs/views/Dom";
import ContentsManager from "./managers/ContentsManager";

Dom.documentReady((): void => {
    new ContentsManager();
});