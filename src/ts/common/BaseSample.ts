import Stats = require("stats.js");

export default class BaseSample {
    private _stats: Stats;
    protected _isRun: boolean = true;


    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        this._render = this._render.bind(this);
        this._setStats();
    }

    /**
     * stats.jsの設定
     * @private
     */
    protected _setStats(): void {
        // 0: fps, 1: ms, 2: mb, 3+: custom
        this._stats = new Stats();
        this._stats.showPanel(0);
        document.body.appendChild(this._stats.dom);
    }

    /**
     * レンダリング
     * @private
     */
    protected _render(): void {
        this._stats.update();
    }
}

