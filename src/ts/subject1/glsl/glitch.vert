varying vec3 vPosition;
varying vec2 vUv;

void main() {
    // 頂点座標をfsに渡す
    vPosition = position;

    // テクスチャを貼るためのUV座標
    vUv = uv;
    gl_Position = vec4(position, 1.0);
}
