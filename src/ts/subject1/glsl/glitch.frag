// 時間
uniform float time;
uniform vec2 resolution;
uniform sampler2D target;

uniform float strength;
uniform float rad;

varying vec3 vPosition;
varying vec2 vUv;

#pragma glslify: snoise3 = require(glsl-noise/simplex/3d);
#pragma glslify: cnoise3 = require(glsl-noise/classic/3d);

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
    // uTextureのtexCoords座標の色(vec4)を返す
    vec4 texture = texture2D(target, vUv);

    float noiseFlag = cos(time * rand(vec2(time)));

    // ノイズを作成
    float noise = (noiseFlag <=  -0.0) ? mod(rand(vec2(time + vPosition.x, time + vPosition.y)), strength) : 0.0;


    // ノイズの座標
    float noisePos = (noiseFlag <=  -0.5)
        ?  gl_FragCoord.x * smoothstep(-noise, 0.4, noise)
        : -gl_FragCoord.x * smoothstep(-noise, 0.4, noise);


    // RGBの設定
    float r = texture2D(target, vUv + vec2(noisePos, 0.0) / resolution * 0.2).r;
    float g = texture2D(target, vUv + vec2(noisePos, 0.0) / resolution * 0.4).g;
    float b = texture2D(target, vUv + vec2(noisePos, 0.0) / resolution * 0.6).b;

    // ノイズの位置設定
    float noiseX = snoise3(vec3(vPosition.x + time * 50.0, 0.0, time));
    float noiseY = snoise3(vec3(0.0, vPosition.y + time * 50.0, 0.0));


    vec3 color = vec3(r, g, b) - floor(smoothstep(0.2, 0.4, noiseX) * smoothstep(0.2, 0.4, noiseY));
    gl_FragColor = vec4(color, texture.a);
}
