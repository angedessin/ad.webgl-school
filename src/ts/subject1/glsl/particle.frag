uniform vec3 color;
uniform sampler2D texture;
varying vec3 vColor;
void main() {
    // パーティクルの色を更新
    gl_FragColor = vec4( color * vColor, 1.0 ) * texture2D(texture, gl_PointCoord );
}