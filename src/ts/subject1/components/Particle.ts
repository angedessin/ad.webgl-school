import * as THREE from "three";
import BaseComponents from "./BaseComponents";

const vs: string = require("../glsl/particle.vert");
const fs: string = require("../glsl/particle.frag");

// declare const THREE: any;

export default class Particle extends BaseComponents {
    private PARTICLE_TOTAL_COUNT: number = 20000;
    private RADIUS: number = 200;
    private MAX_SIZE: number = 1.5;
    private MIN_SIZE: number = 0.85;

    private _particle: THREE.Group = new THREE.Group();

    private _points: THREE.Points;

    private _geometry: THREE.BufferGeometry = new THREE.BufferGeometry();
    private _uniforms = {
        color: {type: "v3", value: new THREE.Color(0xffffff)},
        texture: {type: "t", value: new THREE.TextureLoader().load("../assets/images/subject1/spark1.png")}
    };
    private _material: THREE.ShaderMaterial = new THREE.ShaderMaterial({
        uniforms: this._uniforms,
        vertexShader: vs,
        fragmentShader: fs,
        blending: THREE.AdditiveBlending,
        depthTest: false,
        transparent: true
    });

    private _positions: Float32Array = new Float32Array(this.PARTICLE_TOTAL_COUNT * 3);
    private _colors: Float32Array = new Float32Array(this.PARTICLE_TOTAL_COUNT * 3);
    private _sizes: Float32Array = new Float32Array(this.PARTICLE_TOTAL_COUNT);
    private _color: THREE.Color = new THREE.Color();


    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        super();
        this._init();
    }


    /**
     * パーティクルを作成する
     * @private
     */
    private _init(): void {
        for (let i: number = 0, i3: number = 0; i < this.PARTICLE_TOTAL_COUNT; i++, i3 += 3) {
            this._positions[i3] = ( Math.random() * 2 - 1 ) * this.RADIUS;
            this._positions[i3 + 1] = ( Math.random() * 2 - 1 ) * this.RADIUS;
            this._positions[i3 + 2] = ( Math.random() * 2 - 1 ) * this.RADIUS;
            this._color.setHSL(i / this.PARTICLE_TOTAL_COUNT, 1.0, 0.5);
            this._colors[i3] = this._color.r;
            this._colors[i3 + 1] = this._color.g;
            this._colors[i3 + 2] = this._color.b;
            this._sizes[i] = this.MAX_SIZE;
        }

        // attributeを追加
        this._geometry.addAttribute("position", new THREE.BufferAttribute(this._positions, 3));
        this._geometry.addAttribute("customColor", new THREE.BufferAttribute(this._colors, 3));
        this._geometry.addAttribute("size", new THREE.BufferAttribute(this._sizes, 1));
        this._points = new THREE.Points(this._geometry, this._material);
        this._particle.add(this._points);
    }


    /**
     * パーティクルを更新する
     * @param color
     */
    render(color?: number): void {
        if (color) {
            this._color = new THREE.Color(color);
        }
        const time: number = Date.now() * 0.005;
        this._particle.rotation.y = 0.01 * time;

        for (let i: number = 0, i3: number = 0; i < this.PARTICLE_TOTAL_COUNT; i++, i3 += 3) {
            let size: number = this.MAX_SIZE * (1 + Math.sin(0.1 * i + time));

            if (size > this.MAX_SIZE) {
                size = this.MAX_SIZE;
            } else if (size < this.MIN_SIZE) {
                size = this.MIN_SIZE;
            }

            this._sizes[i] = size;

            if (color) {
                this._colors[i3] = this._color.r;
                this._colors[i3 + 1] = this._color.g;
                this._colors[i3 + 2] = this._color.b;
            }
        }

        this._geometry.attributes["size"].needsUpdate = true;

        if (color) {
            this._geometry.attributes["customColor"].needsUpdate = true;
        }

    }


    getParticle(): THREE.Group {
        return this._particle;
    }

}