import * as THREE from "three";
import Core from "../../libs/utils/Core";
import {default as GlitchConstants} from "../constants/GlitchConstants";
import BaseComponents from "./BaseComponents";

const vs: string = require("../glsl/glitch.vert");
const fs: string = require("../glsl/glitch.frag");

interface ISetting {
    strength: number;
}

interface IRenderParam {
    time: number;
    rad: number;
    target: THREE.WebGLRenderTarget;
}

export default class Glitch extends BaseComponents {
    _renderTarget: THREE.WebGLRenderTarget;
    private _dpr: number;
    private _uniforms = {
        time: {type: "f", value: 0},
        resolution: {type: "v2", value: new THREE.Vector2(Core.getInnerW(), Core.getInnerH())},
        target: {type: "t", value: null},
        strength: {type: "f", value: GlitchConstants.GLITCH_STRENGTH},
        rad: {type: "f", value: 0}
    };

    private _mesh: THREE.Mesh = new THREE.Mesh(
        new THREE.PlaneGeometry(2, 2),
        new THREE.ShaderMaterial({
            uniforms: this._uniforms,
            vertexShader: vs,
            fragmentShader: fs,
            transparent: true
        })
    );


    /**
     * コンストラクタ
     * @constructor
     * @param dpr
     */
    constructor(dpr: number) {
        super();
        this._dpr = dpr;
        this._renderTarget = new THREE.WebGLRenderTarget(Core.getInnerW() * this._dpr, Core.getInnerH() * this._dpr);
    }

    /**
     * メッシュを取得する
     * @returns {THREE.Mesh}
     */
    getMesh(): THREE.Mesh {
        return this._mesh;
    }

    /**
     * シーンを描画する
     * @param param
     */
    render(param: IRenderParam) {
        const {time, rad, target}:IRenderParam = param;
        this._uniforms.time.value += time;
        this._uniforms.rad.value = rad;
        this._uniforms.target.value = target.texture;
    }

    /**
     * リサイズ処理
     */
    resize() {
        this._uniforms.resolution.value.set(Core.getInnerW(), Core.getInnerH());
        this._renderTarget.setSize(Core.getInnerW() * this._dpr, Core.getInnerH() * this._dpr);
    }


    /**
     * ノイズの設定を変更
     * @param setting
     */
    updateSetting(setting: ISetting) {
        const {strength}:ISetting = setting;
        this._uniforms.strength.value = strength;

    }
}