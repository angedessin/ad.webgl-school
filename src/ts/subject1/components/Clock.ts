import {TimelineLite, TweenLite} from "gsap";
import * as THREE from "three";
import {
    default as ClockConstants,
    IHandParameter,
    ILineParameter,
    ITextParameter,
    ITime
} from "../constants/ClockConstants";
import BaseComponents from "./BaseComponents";


interface IMaterialParameter {
    color: number;
    specular?: number;
}

interface ITextTexture {
    text: string;
    color: string;
    size: number;
    family: string;
}


export default class Clock extends BaseComponents {
    // group
    private _clock: THREE.Group = new THREE.Group();
    private _times: THREE.Group = new THREE.Group();
    private _lines: THREE.Group = new THREE.Group();
    private _hands: THREE.Group = new THREE.Group();
    private _text: THREE.Group = new THREE.Group();

    // flag
    private _isLineReset: boolean = false;
    private _isResetAnimation: boolean = false;

    // particle
    private _prevSecond: number = 0;
    _lineColor: number = 0xffffff;
    _prevLineColor: number = 0x000000;


    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        super();
        this._initClockPartObject();
        this._initTimeObject();
        this._initHandsObject();
        this._initLineObject();
        this._initTextObject();
    }


    /**
     * 時計のgroupを取得する
     * @returns {THREE.Group}
     * @public
     */
    getClock(): THREE.Group {
        return this._clock;
    }

    /**
     * レンダリング
     * @public
     */
    render(): void {
        const {h, m, s, ms}:ITime = this.getNowTime();
        const rf: number = ClockConstants.RF;
        const deg: number = 360 / 60;
        const lDeg: number = 360 / 12;

        // 秒針
        const sRad: number = -(deg * rf) * (s + ms / 1000);
        const secondHand: THREE.Mesh = <THREE.Mesh>this._hands.getObjectByName(`${ClockConstants.SECOND_HAND_PARAMETER.id}-pivot`);
        secondHand.rotation.z = sRad;

        // 長針
        const mRad: number = -(deg * rf) * (m + s / 60);
        const minuteHand: THREE.Mesh = <THREE.Mesh>this._hands.getObjectByName(`${ClockConstants.MINUTE_HAND_PARAMETER.id}-pivot`);
        minuteHand.rotation.z = mRad;

        // 短針
        const lRad: number = -(lDeg * rf) * (h + m / 60);
        const longHand: THREE.Mesh = <THREE.Mesh>this._hands.getObjectByName(`${ClockConstants.SHORT_HAND_PARAMETER.id}-pivot`);

        longHand.rotation.z = lRad;

        if (s == 0 && this._isLineReset) {
            // 0の場合の処理
            this._initMeshOfSecond();
            this._isLineReset = false;
        } else if (!this._isResetAnimation) {
            // それ以外の場合の処理
            this._drawCurrentSecond(s, ms);
            if (s !== 0) {
                this._isLineReset = true;
            }
        }
    }


    /**
     * 時計の色を変更する
     * @param color
     * @public
     */
    updateColor(color: string) {
        const hex: number = this.changeRgbToHexNumberColor(color);
        this._clock.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                child["material"]["color"]["setHex"](hex);
            }
        });
    }


    /**
     * 時間オブジェクトの初期設定
     * @private
     */
    private _initTimeObject(): void {
        this._drawTimeLine(ClockConstants.MS_LINE_PARAMETER);
        this._drawTimeLine(ClockConstants.S_LINE_PARAMETER);
        this._drawTimeLine(ClockConstants.H_LINE_PARAMETER);
        this._times.name = "times";
        this._clock.add(this._times);
    }

    /**
     * テキストの初期設定
     * @private
     */
    private _initTextObject(): void {
        this._drawText(ClockConstants.TEXT_PARAMETER);
        this._text.name = "text";
        this._clock.add(this._text);
    }

    /**
     * 線オブジェクトの初期設定
     * @private
     */
    private _initLineObject(): void {
        this._lines.name = "lines";
        this._clock.add(this._lines);
        for (let i: number = 0; i < 60; i++) {
            for (let j: number = 100; j < 1100; j += 100) {
                const name: string = this._getCurrentLineName(i, j);
                const degree: number = 360 / 60;
                const time: number = ( i - 60 * 0.25) + (j * 0.001);
                const rad: number = -(degree * ClockConstants.RF * time);
                const x: number = Math.cos(rad) * ClockConstants.CURRENT_LINE_DISTANCE;
                const y: number = Math.sin(rad) * ClockConstants.CURRENT_LINE_DISTANCE;
                const geometry: THREE.Geometry = new THREE.Geometry();
                geometry.vertices.push(new THREE.Vector3(0.0, 0.0, 0.0));
                geometry.vertices.push(new THREE.Vector3(x, y, 0.0));

                const materials: THREE.LineBasicMaterial = new THREE.LineBasicMaterial({
                    color: 0x000000,
                    transparent: true,
                    opacity: 0
                });
                
                const line: THREE.Line = new THREE.Line(geometry, materials);
                line.name = name;
                this._lines.add(line);
            }
        }
        this._currentTimeUpToSecondsDraw();
    }


    /**
     * 針オブジェクトの初期設定
     * @private
     */
    private _initHandsObject(): void {
        this._drawClockHand(ClockConstants.SECOND_HAND_PARAMETER);
        this._drawClockHand(ClockConstants.MINUTE_HAND_PARAMETER);
        this._drawClockHand(ClockConstants.SHORT_HAND_PARAMETER);
        this._hands.name = "hands";
        this._clock.add(this._hands);
    }


    /**
     * 時計のパーツを作成する
     * @private
     */
    private _initClockPartObject(): void {
        const materialParameter: IMaterialParameter = {color: 0xffffff, specular: 0xffffff};
        const geometry: THREE.SphereGeometry = new THREE.SphereGeometry(0.05, 0.05, 500);
        const material: THREE.MeshPhongMaterial = new THREE.MeshPhongMaterial(materialParameter);
        const circle: THREE.Mesh = new THREE.Mesh(geometry, material);
        this._clock.add(circle);
    }


    /**
     * 線の時間をオブジェクトを作成
     * @param params
     * @private
     */
    private _drawTimeLine(params: ILineParameter): void {
        const {id, w, h, length, radius, color}:ILineParameter = params;
        const materialParameter: IMaterialParameter = {
            color: this.changeRgbToHexNumberColor(color),
            specular: 0xffffff
        };
        const geometry: THREE.BoxGeometry = new THREE.BoxGeometry(w, h, 0.05);
        const degree: number = 360 / length;
        let counter: number = 0;
        for (let i: number = length; i > 0; i--) {
            const material: THREE.MeshPhongMaterial = new THREE.MeshPhongMaterial(materialParameter);
            const time: THREE.Mesh = new THREE.Mesh(geometry, material);

            const rad: number = (degree * ClockConstants.RF) * (i + length * 0.25);

            const x: number = Math.cos(rad) * radius;
            const y: number = Math.sin(rad) * radius;
            const name: string = `${id}_${counter}`;

            time.position.set(x, y, 0);
            time.name = name;
            time.rotation.z = (degree * ClockConstants.RF) * i;
            counter += 1;
            switch (id) {
            case "s":
                if (i % 5 !== 0) {
                    this._times.add(time);
                }
                break;
            case "ms":
                if (i % 5 !== 0) {
                    this._times.add(time);
                }
                break;
            default:
                this._times.add(time);
                break;
            }
        }

    }


    /**
     * 時計の針を作成する
     * @param params
     * @returns {THREE.Mesh}
     * @private
     */
    private _drawClockHand(params: IHandParameter): THREE.Group {
        const {w, h, color, id, pos}:IHandParameter = params;
        const materialParameter: IMaterialParameter = {
            color: this.changeRgbToHexNumberColor(color),
            specular: 0xffffff
        };
        const geometry: THREE.BoxGeometry = new THREE.BoxGeometry(w, h, 0.05);
        const material: THREE.MeshPhongMaterial = new THREE.MeshPhongMaterial(materialParameter);
        const clockHand: THREE.Mesh = new THREE.Mesh(geometry, material);
        const pivot: THREE.Group = new THREE.Group;
        clockHand.position.set(pos.x, pos.y, pos.z);
        clockHand.name = id;
        pivot.name = `${id}-pivot`;
        pivot.add(clockHand);
        this._hands.add(pivot);
        return pivot;
    }


    /**
     * テキストを描画する
     * @param params
     * @private
     */
    private _drawText(params: ITextParameter): void {
        const {id, w, h, length, radius, color, size}:ITextParameter = params;
        const degree: number = 360 / length;
        const geometry: THREE.PlaneGeometry = new THREE.PlaneGeometry(w, h);
        let counter: number = 0;
        for (let i: number = length; i > 0; i--) {
            const currentNum: number = (counter === 0) ? 12 : counter;
            const config: ITextTexture = {
                text: `${currentNum}`,
                color: color,
                size: size,
                family: ClockConstants.FONT_FAMILY
            };
            const texture: THREE.Texture = this._createTextTexture(config);
            const material: THREE.Material = new THREE.MeshPhongMaterial({
                map: texture,
                color: color,
                transparent: true
            });
            const text: THREE.Mesh = new THREE.Mesh(geometry, material);
            const rad: number = (degree * ClockConstants.RF) * (i + length * 0.25);
            const x: number = Math.cos(rad) * radius;
            const y: number = Math.sin(rad) * radius;
            const name: string = `${id}_${currentNum}`;
            text.position.set(x, y, 0);
            text.name = name;
            text.rotation.z = (degree * ClockConstants.RF) * i;
            counter += 1;
            this._text.add(text);
        }
    }


    /**
     * テキストのtextureを作成する
     * @param config
     * @returns {THREE.Texture}
     * @private
     */
    private _createTextTexture(config: ITextTexture): THREE.Texture {
        const {text, color, family}:ITextTexture = config;
        const canvas: HTMLCanvasElement = document.createElement("canvas");
        const context: CanvasRenderingContext2D = canvas.getContext("2d");
        // const textWidth = context.measureText(text).width;
        // canvas.width = textWidth + 50;
        // canvas.height = size + 50;
        canvas.width = 64;
        canvas.height = 64;

        // 文字の描画開始
        context.beginPath();
        context.fillStyle = color;
        context.font = `${50}px ${family}`;
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillText(text, canvas.width / 2, canvas.height / 2);
        context.fill();

        // テクスチャの作成
        const texture: THREE.Texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;

        return texture;


    }

    /**
     * 現在の時間に基づいて線を引く
     * @param s
     * @param ms
     * @private
     */
    private _drawCurrentSecond(s: number, ms: number): void {
        const name: string = this._getCurrentLineName(s, ms);
        const color: number = this._getLineColor();

        // particleの色を変更する
        if (s % 1 === 0 && this._prevSecond !== s) {
            this._prevSecond = s;
            this._prevLineColor = this._lineColor;
            this._lineColor = color;
        }

        if (this._lines.getObjectByName(name)) {
            const line = <THREE.Line>this._lines.getObjectByName(name);
            line.material["color"]["setHex"](color);
            line.geometry["verticesNeedUpdate"] = true;
            const tl: TimelineLite = new TimelineLite();
            tl.add(TweenLite.fromTo(line.material, 0.85, {
                opacity: 0
            }, {
                opacity: 0.85,
                ease: Sine.easeOut
            }));
            tl.play();
        }
    }


    /**
     * 現在の時間の線の名前を取得
     * @param s
     * @param ms
     * @returns {string}
     * @private
     */
    private _getCurrentLineName(s: number, ms: number): string {
        return `${s}_${Math.ceil(ms / 100) * 100}`;
    }


    /**
     * 線の色を取得
     * @returns {Number}
     * @private
     */
    private _getLineColor(): number {
        return Number(`0x${ Math.floor(Math.random() * 16777215).toString(16)}`);
    }


    /**
     * 現在の時間までの線を描画する
     * @private
     */
    private _currentTimeUpToSecondsDraw(): void {
        const {s}:ITime = this.getNowTime();
        for (let i: number = 0; i < s + 1; i++) {
            for (let j: number = 100; j < 1100; j += 100) {
                this._drawCurrentSecond(i, j);
            }
        }
    }


    /**
     * 線のmeshを削除
     * @private
     */
    private _initMeshOfSecond(): void {
        this._isResetAnimation = true;
        const lines: Array<THREE.Object3D> = this._lines.children;
        for (let i: number = 0; i < lines.length; i++) {
            const tl: TimelineLite = new TimelineLite();
            const target: THREE.Mesh = <THREE.Mesh >lines[i];
            tl.add(TweenLite.to(target.material, 0.15, {
                opacity: 0.0,
                delay: 0.0025 * i,
                ease: Sine.easeOut,
                onComplete: (): void => {
                    target.material["color"]["setHex"](0x000000);
                    if (i === lines.length - 1) {
                        this._currentTimeUpToSecondsDraw();
                        this._isResetAnimation = false;
                    }
                }
            }));
            tl.play();
        }
    }
}


