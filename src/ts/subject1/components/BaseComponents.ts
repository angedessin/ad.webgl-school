import EventDispatcher from "../../libs/events/EventDispatcher";
import {ITime} from "../constants/ClockConstants";
export default class BaseComponents extends EventDispatcher {
    constructor() {
        super();
    }

    /**
     * 現在の時間を取得する
     * @returns {{h: number, m: number, s: number, ms: number}}
     * @private
     */
    getNowTime(): ITime {
        const nowTime: Date = new Date();
        let h: number = nowTime.getHours();
        const m: number = nowTime.getMinutes();
        const s: number = nowTime.getSeconds();
        const ms: number = nowTime.getMilliseconds();
        if (h > 11) {
            h = h - 12;
        }
        return {h: h, m: m, s: s, ms: ms};
    };

    /**
     * カラーコードを変更する
     * @param color
     * @returns {Number}
     */
    changeRgbToHexNumberColor(color: string): number {
        return Number(color.replace("#", "0x"));
    }

}