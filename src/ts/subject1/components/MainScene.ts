import * as THREE from "three";
import MouseEvent from "../../libs/events/MouseEvent";
import ResizeManager from "../../libs/managers/ResizeManager";
import Core from "../../libs/utils/Core";
import Dom from "../../libs/views/Dom";
import {default as ClockConstants} from "../constants/ClockConstants";

import {default as GlitchConstants} from "../constants/GlitchConstants";
import BaseComponents from "./BaseComponents";
import Clock from "./Clock";
import Glitch from "./Glitch";
import Particle from "./Particle";
import Stats = require("stats.js");
declare const dat: any;

interface ICameraParameter {
    fovy: number;
    aspect: number;
    near: number;
    far: number;
    x: number;
    y: number;
    z: number;
    lookAt: THREE.Vector3;
}


interface IRendererParameter {
    clearColor: number;
    w: number;
    h: number;
}


export default class MainScene extends BaseComponents {
    private _w: number = Core.getInnerW();
    private _h: number = Core.getInnerH();
    private _world: HTMLElement = Dom.getElement("world");
    private _isRun: boolean = true;
    private _threeClock: THREE.Clock = new THREE.Clock();

    // debug
    private _stats: Stats;
    private _gui: dat.GUI = new dat.GUI();
    private _controls;

    // class
    private _resizeManager: ResizeManager = new ResizeManager(50);

    // renderer
    private RENDERER_PARAMETER: IRendererParameter = {
        clearColor: 0x000000,
        w: this._w,
        h: this._h
    };
    private _renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();

    // camera
    private CAMERA_PARAMETER: ICameraParameter = {
        fovy: 60,
        aspect: this._w / this._h,
        near: 0.1,
        far: 200.0,
        x: 0.0,
        y: 0.0,
        z: 2.2,
        lookAt: new THREE.Vector3(0.0, 0.0, 0.0)
    };

    // camera
    private _camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
        this.CAMERA_PARAMETER.fovy,
        this.CAMERA_PARAMETER.aspect,
        this.CAMERA_PARAMETER.near,
        this.CAMERA_PARAMETER.far
    );


    // light
    private _ambientColor: string = "#ffffff";
    private _ambientIntensity: number = 0.8;
    private _ambient: THREE.AmbientLight = new THREE.AmbientLight(this.changeRgbToHexNumberColor(this._ambientColor), this._ambientIntensity);

    private _directionalColor: string = "#ffffff";
    private _directional: THREE.DirectionalLight = new THREE.DirectionalLight(this.changeRgbToHexNumberColor(this._directionalColor));

    // object
    private _scene: THREE.Scene = new THREE.Scene();
    private _particle: Particle = new Particle();
    private _glitch: Glitch;
    private _rtSwitch: boolean = true;
    private _isGlitchStop: boolean = false;
    private _clock: Clock = new Clock();

    // mouse
    private _mouseX: number = 0;
    private _mouseY: number = 0;
    private _prevMouseX: number = 0;
    private _prevMouseY: number = 0;
    private _isStartMouseMove: boolean = false;

    private _dpr: number = window.devicePixelRatio;

    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        super();
        this._render = this._render.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onResize = this._onResize.bind(this);

        if (this._dpr > 2) {
            this._dpr = 2;
        }

        this._glitch = new Glitch(this._dpr);

        this._world.addEventListener(MouseEvent.MOUSE_MOVE, this._onMouseMove);
        this._resizeManager.addEventListener(this._onResize);

        this._initStats();
        this._initGui();


        this._initCamera();
        this._initLight();
        this._initRenderer();
        this._initScene();
        this._render();
    }

    /**
     * マウス座標を取得
     * @param e
     * @private
     */
    private _onMouseMove(e: any): void {
        this._mouseX = e.clientX;
        this._mouseY = e.clientY;
        this._isStartMouseMove = true;
    }


    /**
     * リサイズ処理
     * @param e
     * @private
     */
    private _onResize(e: any): void {
        this._w = Core.getInnerW();
        this._h = Core.getInnerH();
        this._renderer.setSize(this._w, this._h);
        this._camera.aspect = this._w / this._h;
        this._camera.updateProjectionMatrix();
        this._glitch.resize();
    }


    /**
     * statsを生成する
     * @private
     */
    private _initStats(): void {
        // 0: fps, 1: ms, 2: mb, 3+: custom
        this._stats = new Stats();
        this._stats.showPanel(0);
        document.body.appendChild(this._stats.dom);
    }


    /**
     * dat.guiの設定
     * @private
     */
    private _initGui(): void {
        this._controls = {
            run: this._isRun,
            aColor: this._ambientColor,
            aIntensity: this._ambientIntensity,
            dColor: this._directionalColor,
            x: this.CAMERA_PARAMETER.x,
            y: this.CAMERA_PARAMETER.y,
            z: this.CAMERA_PARAMETER.z,
            clockColor: ClockConstants.SHORT_HAND_PARAMETER.color,
            strength: GlitchConstants.GLITCH_STRENGTH,
            stop: this._isGlitchStop
        };

        const ambient = this._gui.addFolder("ambient");
        ambient.addColor(this._controls, "aColor").onChange((e): void => {
            this._ambientColor = e;
            this._ambient.color = new THREE.Color(e);
        });

        ambient.add(this._controls, "aIntensity", 0.1, 1.0).onChange((e): void => {
            this._ambientIntensity = e;
            this._ambient.intensity = this._ambientIntensity;
        });

        const directional = this._gui.addFolder("directional");
        directional.addColor(this._controls, "dColor").onChange((e): void => {
            this._directionalColor = e;
            this._directional.color = new THREE.Color(e);
        });


        // カメラ
        const camera = this._gui.addFolder("camera");
        camera.add(this._controls, "x", 0.0, 5.0).onChange((e): void => {
            this._camera.position.x = this.CAMERA_PARAMETER.x = e;
        });
        camera.add(this._controls, "y", 0.0, 5.0).onChange((e): void => {
            this._camera.position.y = this.CAMERA_PARAMETER.y = e;
        });
        camera.add(this._controls, "z", 0.0, 5.0).onChange((e): void => {
            this._camera.position.z = this.CAMERA_PARAMETER.z = e;
        });

        // クロック
        const clock = this._gui.addFolder("clock");
        clock.addColor(this._controls, "clockColor", ClockConstants.SHORT_HAND_PARAMETER.color).onChange((e): void => {
            this._clock.updateColor(e);
        });


        // ノイズ
        const noise = this._gui.addFolder("noise");
        noise.add(this._controls, "strength", 0.01, 0.1).onChange((e): void => {
            this._glitch.updateSetting({strength: e});
        });

        noise.add(this._controls, "stop").onChange((e): void => {
            this._isGlitchStop = e;
        });


        this._gui.add(this._controls, "run").onChange((e): void => {
            this._isRun = e;
            if (this._isRun) {
                this._render();
            }
        });

    }

    /**
     * カメラの初期設定
     * @private
     */
    private _initCamera(): void {
        const {x, y, z, lookAt}:ICameraParameter = this.CAMERA_PARAMETER;
        this._camera.position.x = x;
        this._camera.position.y = y;
        this._camera.position.z = z;
        this._camera.lookAt(lookAt);
    }

    /**
     * レンダラーの初期設定
     * @private
     */
    private _initRenderer(): void {
        const {w, h}:IRendererParameter = this.RENDERER_PARAMETER;

        this._renderer.setClearColor(new THREE.Color(this.RENDERER_PARAMETER.clearColor));
        this._renderer.setSize(w, h);
        this._renderer.setPixelRatio(this._dpr || 1);
        this._world.appendChild(this._renderer.domElement);
    }

    /**
     * シーンの初期設定
     * @private
     */
    private _initScene(): void {
        this._scene.add(this._clock.getClock());
        this._scene.add(this._glitch.getMesh());
        this._scene.add(this._particle.getParticle());
    }

    /**
     * ライトの初期設定
     * @private
     */
    private _initLight(): void {
        // const directionalLightHelper = new THREE.DirectionalLightHelper(this._directional);
        // this._scene.add(directionalLightHelper);
        // this._directional.position.set(0.0, 1.0, 0.0);
        // this._directional.intensity = 0.0;

        this._scene.add(this._ambient);
        this._scene.add(this._directional);
    }

    /**
     * レンダリング
     * @private
     */
    private _render(): void {
        if (this._isRun) {
            this._stats.update();
            // シーンの回転処理
            this._rotateScene();

            // 時計の処理
            this._clock.render();

            // パーティクルの処理
            if (this._clock._prevLineColor !== this._clock._lineColor) {
                this._particle.render(this._clock._lineColor);
            } else {
                this._particle.render();
            }

            // glitchの処理
            const delta: number = this._threeClock.getDelta();
            const rt: THREE.WebGLRenderTarget = this._glitch._renderTarget;
            if (!this._isGlitchStop) {
                this._glitch.render({time: delta, rad: 0, target: rt});
            }
            this._renderer.render(this._scene, this._camera, rt);
            this._renderer.render(this._scene, this._camera);
            this._rtSwitch = !this._rtSwitch;
            requestAnimationFrame(this._render);
        }
    }

    /**
     * シーンを回転させる
     * @private
     */
    private _rotateScene(): void {
        if (!this._isStartMouseMove) {
            return;
        }
        const ratio: number = 0.0005;
        const rotationMaxValueX: number = 0.4;
        const rotationMaxValueY: number = 0.4;
        const verticalRateX: number = this._w * ratio * 0.5;
        const verticalRateY: number = this._h * ratio * 0.5;
        let rotationX: number = this._mouseY * ratio - verticalRateY;
        let rotationY: number = this._mouseX * ratio - verticalRateX;

        if (rotationX > rotationMaxValueX || rotationX < -rotationMaxValueX) {
            rotationX = (rotationX > rotationMaxValueX) ? rotationMaxValueX : -rotationMaxValueX;
        }

        if (rotationY > rotationMaxValueY || rotationY < -rotationMaxValueY) {
            rotationY = (rotationY > rotationMaxValueY) ? rotationMaxValueY : -rotationMaxValueY;
        }

        this._scene.rotation.x = rotationX;
        this._scene.rotation.y = rotationY;
        this._prevMouseX = this._mouseX;
        this._prevMouseY = this._mouseY;
    }
}