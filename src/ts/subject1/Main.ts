import * as WebFont from "webfontloader";
import Dom from "../libs/views/Dom";
import MainScene from "./components/MainScene";

export default class Main {
    private _mainScene: MainScene;

    /**
     * コンストラクタ
     * @constructor
     */
    constructor() {
        this._documentReady = this._documentReady.bind(this);
        this._startClock = this._startClock.bind(this);
        Dom.documentReady(this._documentReady);
    }

    /**
     * DOMの準備ができた時に呼ばれる処理
     * @private
     */
    private _documentReady(): void {
        WebFont.load({
            timeout: 3000,
            google: {
                families: ["Playfair+Display"]
            },
            fontactive: (familyName, fvd) => {
            },
            active: () => {
                this._startClock();
            },
            inactive: () => {
                this._startClock();
            }
        });
    }

    /**
     * 時計を表示させる
     * @private
     */
    private _startClock(): void {
        if (!this._mainScene) {
            this._mainScene = new MainScene();
        }
    }
}

new Main();
