namespace GlitchConstants {
    export const GLITCH_STRENGTH: number = 0.01;
}

export default GlitchConstants;