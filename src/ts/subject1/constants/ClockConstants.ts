export interface ILineParameter {
    id: string;
    w: number;
    h: number;
    radius: number;
    color: string;
    length: number;
}

export interface IHandParameter {
    w: number;
    h: number;
    color: string;
    id: string;
    pos: {
        x: number; y: number; z: number;
    }
}

export interface ITextParameter {
    id: string;
    w: number;
    h: number;
    radius: number;
    color: string;
    length: number;
    size: number;
}

export interface ITime {
    h: number;
    m: number;
    s: number;
    ms: number;
}

namespace ClockConstants {
    export const PI: number = Math.PI;
    export const RF: number = PI / 180.0;
    export const TIME_COLOR: string = "#ffffff";
    export const SHORT_HAND_COLOR: string = "#f1f2ed";
    export const MINUTE_HAND_COLOR: string = "#f1f2ed";
    export const SECOND_HAND_COLOR: string = "#f1f2ed";

    export const SHORT_HAND_PARAMETER: IHandParameter = {
        w: 0.06, h: 0.5,
        color: SHORT_HAND_COLOR,
        id: "short-hand",
        pos: {x: 0.0, y: 0.25, z: 0.0}
    };

    export const MINUTE_HAND_PARAMETER: IHandParameter = {
        w: 0.03, h: 0.9,
        color: MINUTE_HAND_COLOR,
        id: "minute-hand",
        pos: {x: 0.0, y: 0.5, z: 0.0}
    };

    export const SECOND_HAND_PARAMETER: IHandParameter = {
        w: 0.015, h:  0.9,
        color: SECOND_HAND_COLOR,
        id: "second-hand",
        pos: {x: 0.0, y: 0.5, z: 0.0}
    };

    const LINE_RADIUS: number = 1.0;

    export const MS_LINE_PARAMETER: ILineParameter = {
        id: "ms",
        w: 0.0075,
        h: 0.025,
        length: 300,
        radius: LINE_RADIUS + 0.035,
        color: TIME_COLOR
    };

    export const S_LINE_PARAMETER: ILineParameter = {
        id: "s",
        w: 0.01,
        h: 0.05,
        length: 60,
        radius: LINE_RADIUS + 0.025,
        color: TIME_COLOR
    };

    export const H_LINE_PARAMETER: ILineParameter = {
        id: "h",
        w: 0.015,
        h: 0.1,
        length: 12,
        radius: LINE_RADIUS,
        color: TIME_COLOR
    };

    export const CURRENT_LINE_DISTANCE: number = 0.75;
    export const FONT_FAMILY: string = "Playfair Display";
    export const TEXT_PARAMETER: ITextParameter = {
        id: "h",
        w: 0.15,
        h: 0.15,
        length: 12,
        radius: 0.88,
        color: TIME_COLOR,
        size: 8
    };

}


export default ClockConstants;

