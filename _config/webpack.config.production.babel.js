import webpack from "webpack";
import path from "path";
import failPlugin from "webpack-fail-plugin";

import {env, paths, uglifyOpt} from "./variables";
import BaseConfig from "./webpack.config.babel";

const _config = Object.create(BaseConfig);
const _env = process.env.NODE_ENV || env.localhost;

_config.entry = {
    "assets/js/bundle": "./" + paths.src + paths.typescript + "/Main.ts"
};

_config.output.path = path.resolve(__dirname, `../${paths.app}`);
_config.plugins.push(
    new webpack.DefinePlugin({
        APP_ENV: JSON.stringify(_env),
        PROJECT_CONFIG: {
            isProduction: true
        },
        "process.env": {
            NODE_ENV: JSON.stringify(_env)
        }
    })
);

_config.plugins.push(new webpack.optimize.UglifyJsPlugin(uglifyOpt));
_config.plugins.push(failPlugin);


export default _config;