module.exports = {
    notify: false,
    files: "./app/**/*",
    server: {
        baseDir: "./app/",
        middleware: function (req, res, next) {
            // var timestamp = "[" + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "] ";
            // console.log(timestamp + req.method + " " + req.originalUrl + " - " + req.connection.remoteAddress + " - " + req.headers['user-agent']);
            next();
        }
    }
};