import webpack from "webpack";
import path from "path";

import {env, logColor} from "./variables";

// "localhost", "development", "staging", "production"
const _env = process.env.NODE_ENV || env.localhost;
console.log(logColor.yellow + "【NODE_ENV : " + _env + "】" + logColor.reset);

const MODULES_PATH = path.join(__dirname, "../node_modules/");
const OrbitControls = path.join(MODULES_PATH, "three/examples/js/controls/OrbitControls.js");
const OBJLoader = path.join(MODULES_PATH, "three/examples/js/loaders/OBJLoader.js");


export default {
    cache: true,
    entry: {},
    output: {
        path: "",
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /OrbitControls\.js/,
                use: ["expose-loader?OrbitControls"]
            },
            {
                test: /OBJLoader\.js/,
                use: ["expose-loader?OBJLoader"]
            },
            {
                test: /\.ts(x?)$/,
                loader: "awesome-typescript-loader",
                exclude: /node_modules/,
                options: {
                    configFileName: "tsconfig.json"
                }
            }, {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            }, {
                test: /\.css$/,
                use: [
                    {loader: "style-loader", options: {minimize: true}},
                    {loader: "css-loader"}
                ],

            }, {
                test: /\.scss$/,
                use: [
                    {loader: "style-loader"},
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true,
                            modules: true,
                            importLoaders: 1,
                            localIdentName: "[name]__[local]___[hash:base64:5]"
                        }
                    },
                    {
                        loader: "postcss-loader", options: {
                        config: {
                            path: "_config/postcss.config.js"
                        }
                    }
                    },
                    {loader: "sass-loader", options: {compress: true}}
                ]
            }, {
                test: /\.module\.scss$/,
                use: [
                    {loader: "style-loader"},
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true,
                            modules: true,
                            importLoaders: 1,
                            localIdentName: "[name]__[local]___[hash:base64:5]"
                        }
                    },
                    {loader: "sass-loader", options: {compress: true}}
                ]
            }, {
                test: /\.(vert|frag|glsl)$/,
                use: [
                    {loader: "raw-loader"},
                    {loader: "glslify-loader"}
                ],
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ["*", ".tsx", ".ts", ".js", "jsx"],
        alias: {
            "OrbitControls": OrbitControls,
            "OBJLoader": OBJLoader,
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            THREE: "three"
        })
    ],
    externals: {
        "babel-polyfill": "babel-polyfill"
    }
};

