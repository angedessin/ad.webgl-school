import gulpLoadPlugins from "gulp-load-plugins";
import BaseConfig from "./project.config";
import del from "del";
import glob from "glob";
import uglifySaveLicense from "uglify-save-license";

const fs = require("fs");
const json = JSON.parse(fs.readFileSync("./package.json"));

export const env = BaseConfig.env;
export const logColor = BaseConfig.logColor;
export const paths = {
    assets: "assets",
    src: "src/",
    app: "app/",
    js: "assets/js",
    libs: "assets/libs",
    css: "assets/css",
    images: "assets/images",
    sass: "sass",
    sprites: "sprites",
    svg: "svg",
    typescript: "ts",
    bower: "bower_components/",
    node: "node_modules/",
    pc: "pc/",
    sp: "sp/"
};

const _isLicenseComment = (function () {
    const licenseRegexp = /^\!|^@preserve|^@cc_on|\bMIT\b|\bMPL\b|\bGPL\b|\(c\)|License|Copyright/mi;
    let prevCommentLine = 0;
    return function (node, comment) {
        if (licenseRegexp.test(comment.value) || comment.line === 1 || comment.line === prevCommentLine + 1) {
            prevCommentLine = comment.line;
            return true;
        }
        prevCommentLine = 0;
        return false;
    };
}());


let plugins = gulpLoadPlugins({
    pattern: ["gulp-*", "gulp.*"],
    replaceString: /\bgulp[\-.]/
});

plugins.del = del;
plugins.glob = glob;

export const $ = plugins;
export const sprite = [
    {
        src: `sample/*.png`,
        imgName: `sprite_sample.png`,
        cssName: `_sprite_sample.scss`,
        imgPath: `../images/common/sprite_sample.png`,
        destImg: `${paths.app}${paths.images}/common/`,
        destCss: `${paths.src}${paths.sass}/modules/sprite/`,
        padding: 0, algorithm: "top-down"
    }
];

export const vendorsFiles = [
    `${paths.node}lodash/lodash.min.js`,
    `${paths.node}gsap/src/minified/TweenMax.min.js`,
    `${paths.node}axios/dist/axios.min.js`
];


export const banner = "/*!\n" +
    " * author : " + json.author.name + " (" + json.author.url + ")\n" +
    " * version : " + json.version + "\n" +
    " * Copyright " + new Date() + "\n" +
    " */\n";

export const uglifyOpt = {
    compress: true,
    mangle: true,
    output: {
        comments: uglifySaveLicense
    }
};

export const scriptMinFiles = ["bundle.js"];

export const command = {
    build: "build",
    uglify: "uglify",
    svgmin: "svgmin",
    concatVendors: "concat:vendors",
    sprite: "sprite"
};


