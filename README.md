# スクールのサンプルについて
---------------------------------------
| タイトル | 概要 |
|:-----------|------------|
| URL| [http://staff.maskman.co.jp/k_otsuka/webgl_school/](http://staff.maskman.co.jp/k_otsuka/webgl_school/) | 
| ID | webgl |
| PW | school |

※URLのあとに`?number=1&index=1`などのパラメータを入力しないと適切に表示されません。

## 概要
---------------------------------------
全てのサンプルは、TypeScriptで書いています。webpack経由でコンパイルして、`bundle.js`として書き出しています。

今回、使用しているライブラリは下記のとおりです。

+ [glcubic.js](https://github.com/doxas/glcubic.js/tree/master)
+ [three.js](https://threejs.org/)
+ [dat.GUI](https://workshop.chromeexperiments.com/examples/gui/#1--Basic-Usage)
+ [stats.js](https://github.com/mrdoob/stats.js/)

シェーダの読み込みに関しては、[glslify](https://github.com/stackgl/glslify)を用いてwebpack経由でJSに読み込んでいます。詳しくは、[こちら](http://qiita.com/yuichiroharai/items/ecbfd2d7729c7384fb3a)をご確認ください。


## ファイルの場所について
---------------------------------------
+ [ファイルのアップ先](https://bitbucket.org/angedessin/ad.webgl-school)
+ [ソースファイル](https://bitbucket.org/angedessin/ad.webgl-school/src/)

ドキュメントはソースファイル内の`_docs`ファイルにあります。TSファイルは、`src/ts/sample`にあります。

##　サンプルファイル
---------------------------------------
### 1. 3D 開発や WebGL・シェーダについて知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=6)
+ [サンプル07](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=1&index=7)


### 2. 3D 開発に必要な最低限の数学知識を知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=2&index=6)

### 3. レンダリングの流れとシェーダの役割を知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=3&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=3&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=3&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=3&index=4)


### 4. WebGL の骨格と 3D API の雰囲気を知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=6)
+ [サンプル07](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=4&index=7)

### 5. テクスチャやブレンドなどのテクニックを知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=6)
+ [サンプル07](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=7)
+ [サンプル08](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=8)
+ [サンプル09](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=9)
+ [サンプル10](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=10)
+ [課題](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=5&index=11)

### 6. シェーダをより深く理解し操る技術を知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=6)
+ [サンプル07](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=6&index=7)

### 7. より高度な表現のためのビルトインテクニックを知ろう

+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=7&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=7&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=7&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=7&index=4)

### 8. より高度な表現のためのビルトインテクニックを知ろう
+ [サンプル01](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=1)
+ [サンプル02](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=2)
+ [サンプル03](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=3)
+ [サンプル04](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=4)
+ [サンプル05](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=5)
+ [サンプル06](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=6)
+ [サンプル07](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=7)
+ [サンプル08](http://staff.maskman.co.jp/k_otsuka/webgl_school/?number=8&index=8)



# 開発環境
---------------------------------------
**Macのみの開発環境になります。Windowsでは動きません。**

タスクが複雑になった場合は、必要に応じて`gulp`を使います。コマンドの実行はgulp経由で行わず、タスクの実行は全て`npm-scripts`で行います。

## 使用ツール
---------------------------------------
以下の環境での動作確認しています。

| パッケージ | バージョン | インストール |説明 |
|:-----------|------------|------------|------------|
| [Node.js](https://nodejs.org/en/) | v7.10.0 | [nodebrew](https://github.com/hokaccha/nodebrew) | |
| [yarn](https://yarnpkg.com/) | v0.23.4 | [Homebrew](https://github.com/Homebrew/brew)  | |
| [gulp](http://gulpjs.com/)| v3.9.1 | npm | |
| [Sass](http://sass-lang.com/)| v3.4.22 | Ruby gem | |
| [TypeScript](http://www.typescriptlang.org/)| v2.4.2 | npm | |

### gulp
ES6形式でgulpファイルを記述しています。

## タスク
---------------------------------------
npm-scriptsでのタスクの実行についての説明です。タスクが長いものは、`shell`で実行しています。シェルファイルは、`_shフォルダ`にまとめています。

`NODE_ENV`の値に応じて、処理を変えています。必要に応じて、値を渡してください。

	$ NODE_ENV=production yarn task
	$ NODE_ENV=staging yarn task
	$ NODE_ENV=development yarn task

### サーバを起動させる
	$ yarn server

[`Browsersync`](https://www.browsersync.io/)を使って、ローカルサーバーを立ち上げます。	
	
### webpack
	$ yarn webpack
	
ローカルサーバーは`webpack-dev-server`ではなく、`Browsersync`を使用しています。

`NODE_ENV`の値に応じて、`config`ファイルを変えています。

| 値 | ファイル名 |
|:-----------|------------|
| production  | webpack.config.production.babel.js |
| production以外 | webpack.config.develop.babel.js |

### pug

	$ yarn pug
	
テンプレートエンジンの[`pug`](https://www.npmjs.com/package/pug)のコンパイル処理です。タスクを実行する上で、[`pug-cli`](https://github.com/pugjs/pug-cli)が必要です。

### CSS
	$ yarn css

コマンドを実行すると、順番にタスクが実行されます。

1. Sassをコンパイル
2. メディアクエリを1つにまとめる
3. `autoprefixer`を実行
4. `csscomb`を実行
	
### Sass

	$ yarn css:sass

初期設定では、`app.scss`を`app.css`にコンパイルしています。コンパイル先を変更したい場合、`npm-scripts`を変更してください。