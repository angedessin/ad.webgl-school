import gulp from "gulp";
import {command, vendorsFiles, $, uglifyOpt} from "../_config/variables";


/**
 * ライブラリファイルを結合するためのタスク
 * @param dirPath
 * @param destPath
 * @returns {gulp}
 */
const concatVendorFiles = (dirPath, destPath, fileName) => {
    return gulp.src(dirPath)
        .pipe($.concat(fileName))
        .pipe($.uglify({
            compress: true,
            mangle: false,
            output: {
                comments: /^!/
            }
        }))
        .pipe(gulp.dest(destPath));
};

/**
 * 選択された全てのライブラリファイルをconcat
 */
gulp.task(command.concatVendors, () => {
    concatVendorFiles(vendorsFiles, "./app/assets/js/", "vendors.js");
});

