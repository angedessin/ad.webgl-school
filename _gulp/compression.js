import gulp from "gulp";
import {paths, scriptMinFiles, uglifyOpt, command, banner, $} from "../_config/variables";


/**
 * JSを圧縮するためのタスク
 * @param dirPath
 * @param destPath
 * @param minFiles
 */
const uglifyTask = (dirPath, destPath, minFiles) => {
    for (let i = 0; i < minFiles.length; i++) {
        gulp.src(dirPath + minFiles[i])
            .pipe($.uglify(uglifyOpt))
            .pipe($.header(banner))
            .pipe(gulp.dest(destPath));
    }
};


/**
 * 指定したJSファイルを全て圧縮するタスク
 */
gulp.task(command.uglify, () => {
    const dirPath = paths.app + paths.js + "/";
    const destPath = paths.app + paths.js + "/";
    uglifyTask(dirPath, destPath, scriptMinFiles);

    // const dirPathPc = config.paths.app + config.paths.pc + config.paths.js + "/";
    // const destPathPc = config.paths.app + config.paths.pc + config.paths.js + "/";
    // const dirPathSp = config.paths.app + config.paths.sp + config.paths.js + "/";
    // const destPathSp = config.paths.app + config.paths.sp + config.paths.js + "/";
    // uglifyAllTask(dirPathPc, destPathPc, config.jsMinFiles);
    // uglifyAllTask(dirPathSp, destPathSp, config.jsMinFiles);
});


/**
 * SVGファイルを圧縮するタスク
 * @param dirPath
 * @param destPath
 * @returns {gulp}
 */
export const svgMinTask = (dirPath, destPath) => {
    return gulp.src(dirPath + "**/*.svg")
        .pipe($.svgmin())
        .pipe(gulp.dest(destPath));
};

/**
 * svgの圧縮
 */
gulp.task(command.svgmin, () => {
    const dirPath = paths.app + paths.images + "/";
    const destPath = paths.app + paths.images + "/";
    svgMinTask(dirPath, destPath);
});

