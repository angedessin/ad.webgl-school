import gulp from "gulp";
import {command, paths, sprite, $} from "../_config/variables";

/**
 * spriteを生成するタスク
 */
gulp.task(command.sprite, () => {
    const spriteObj = sprite;
    const spritePath = paths.src + paths.sprites + "/";
    let spriteData;

    for (let i = 0; i < spriteObj.length; i++) {
        const src = spritePath + spriteObj[i].src;
        spriteData = gulp.src(src)
            .pipe($.spritesmith({
                imgName: spriteObj[i].imgName,
                cssName: spriteObj[i].cssName,
                imgPath: spriteObj[i].imgPath,
                cssFormat: "scss",
                cssOpts: {functions: false},
                padding: spriteObj[i].padding,
                algorithm: spriteObj[i].algorithm,
            }));
        spriteData.img
            .pipe(gulp.dest(spriteObj[i].destImg));

        spriteData.css
            .pipe(gulp.dest(spriteObj[i].destCss));
    }
});